-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 10, 2018 at 02:24 PM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mysqlrem_bosslaser`
--

-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_banners`
--

DROP TABLE IF EXISTS `alexis_banners_banners`;
CREATE TABLE `alexis_banners_banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `looks` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `banner_code` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show` int(11) NOT NULL DEFAULT '0',
  `dummy` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `percents` int(11) NOT NULL DEFAULT '100',
  `config_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `dummy_type` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `alexis_banners_banners`
--

TRUNCATE TABLE `alexis_banners_banners`;
-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_configs`
--

DROP TABLE IF EXISTS `alexis_banners_configs`;
CREATE TABLE `alexis_banners_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `alexis_banners_configs`
--

TRUNCATE TABLE `alexis_banners_configs`;
-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_queues`
--

DROP TABLE IF EXISTS `alexis_banners_queues`;
CREATE TABLE `alexis_banners_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `config_id` int(11) NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `alexis_banners_queues`
--

TRUNCATE TABLE `alexis_banners_queues`;
-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

DROP TABLE IF EXISTS `backend_access_log`;
CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_access_log`
--

TRUNCATE TABLE `backend_access_log`;
--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-04-06 12:40:33', '2018-04-06 12:40:33'),
(2, 1, '::1', '2018-04-06 18:41:06', '2018-04-06 18:41:06'),
(3, 1, '::1', '2018-06-21 13:02:57', '2018-06-21 13:02:57'),
(4, 1, '41.38.109.32', '2018-06-25 09:30:20', '2018-06-25 09:30:20'),
(5, 1, '41.38.109.32', '2018-07-03 09:53:14', '2018-07-03 09:53:14');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

DROP TABLE IF EXISTS `backend_users`;
CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_users`
--

TRUNCATE TABLE `backend_users`;
--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
(1, 'Admin', 'mohamed', 'admin', 'admin@bosslaser.com', '$2y$10$yuF29I4YU2U.PoRIsKT0u.7s7YY3xk8I0xx49EIC6uqwAIJu1ns.2', NULL, '$2y$10$1DExCnLL/lQ8O2KCLJIY2u9JYxvcv/LMYNNsHoiZ3IT1U/IcWigX2', NULL, '', 1, 2, NULL, '2018-07-03 09:53:12', '2018-04-06 12:39:21', '2018-07-03 09:53:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

DROP TABLE IF EXISTS `backend_users_groups`;
CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_users_groups`
--

TRUNCATE TABLE `backend_users_groups`;
--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

DROP TABLE IF EXISTS `backend_user_groups`;
CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_user_groups`
--

TRUNCATE TABLE `backend_user_groups`;
--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2018-04-06 12:39:20', '2018-04-06 12:39:20', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

DROP TABLE IF EXISTS `backend_user_preferences`;
CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_user_preferences`
--

TRUNCATE TABLE `backend_user_preferences`;
-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

DROP TABLE IF EXISTS `backend_user_roles`;
CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_user_roles`
--

TRUNCATE TABLE `backend_user_roles`;
--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2018-04-06 12:39:20', '2018-04-06 12:39:20'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2018-04-06 12:39:20', '2018-04-06 12:39:20');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

DROP TABLE IF EXISTS `backend_user_throttle`;
CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `backend_user_throttle`
--

TRUNCATE TABLE `backend_user_throttle`;
--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL),
(2, 1, '41.38.109.32', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `cache`
--

TRUNCATE TABLE `cache`;
-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

DROP TABLE IF EXISTS `cms_theme_data`;
CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `cms_theme_data`
--

TRUNCATE TABLE `cms_theme_data`;
-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

DROP TABLE IF EXISTS `cms_theme_logs`;
CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `cms_theme_logs`
--

TRUNCATE TABLE `cms_theme_logs`;
-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

DROP TABLE IF EXISTS `deferred_bindings`;
CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `deferred_bindings`
--

TRUNCATE TABLE `deferred_bindings`;
-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `failed_jobs`
--

TRUNCATE TABLE `failed_jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_categories`
--

DROP TABLE IF EXISTS `ideas_categories`;
CREATE TABLE `ideas_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(10) UNSIGNED DEFAULT NULL,
  `nest_right` int(10) UNSIGNED DEFAULT NULL,
  `nest_depth` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `seo_title` text COLLATE utf8mb4_unicode_ci,
  `seo_keyword` text COLLATE utf8mb4_unicode_ci,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `featured_image` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `cat_order` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_categories`
--

TRUNCATE TABLE `ideas_categories`;
--
-- Dumping data for table `ideas_categories`
--

INSERT INTO `ideas_categories` (`id`, `name`, `slug`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`, `status`, `seo_title`, `seo_keyword`, `seo_description`, `featured_image`, `description`, `cat_order`, `created_at`, `updated_at`) VALUES
(1, 'Uncategory', 'uncategory', NULL, 1, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Laptop', 'laptop', NULL, 3, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_city`
--

DROP TABLE IF EXISTS `ideas_city`;
CREATE TABLE `ideas_city` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_zone_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_city`
--

TRUNCATE TABLE `ideas_city`;
--
-- Dumping data for table `ideas_city`
--

INSERT INTO `ideas_city` (`id`, `name`, `geo_zone_id`) VALUES
(1, 'city 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_config`
--

DROP TABLE IF EXISTS `ideas_config`;
CREATE TABLE `ideas_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `is_default` tinyint(4) NOT NULL COMMENT '1: default, 2: plus'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_config`
--

TRUNCATE TABLE `ideas_config`;
--
-- Dumping data for table `ideas_config`
--

INSERT INTO `ideas_config` (`id`, `name`, `slug`, `value`, `is_default`) VALUES
(1, 'Coupon prefix', 'coupon_prefix', 'shop', 1),
(2, 'Coupon length random', 'coupon_length_random', '10', 1),
(3, 'Is cache enable', 'is_cache', '0', 1),
(4, 'Currency default', 'currency_default', '1', 1),
(5, 'weight base default', 'weight_based_default', '1', 1),
(6, 'Paypal id', 'paypal_id', '', 1),
(7, 'Paypal mode', 'paypal_mode', '0', 1),
(8, 'Stripe publish key', 'stripe_publish', '', 1),
(9, 'Stripe secret key', 'stripe_secret', '', 1),
(10, 'Does delete downloadable product link after one time download', 'download_once', '1', 1),
(11, 'Can customer download right after paying when checkout', 'download_right_after_checkout', '1', 1),
(12, 'Number minutes to cache', 'cache_minute', '1', 1),
(13, 'Send mail by function', 'mail_method', '0', 1),
(14, 'Paypal', 'paypal', '0', 1),
(15, 'Stripe', 'stripe', '0', 1),
(16, 'Convert Currency', 'convert_currency', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_coupon`
--

DROP TABLE IF EXISTS `ideas_coupon`;
CREATE TABLE `ideas_coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: percentage, 2: fixed amount',
  `logged` tinyint(4) NOT NULL COMMENT 'need customer logged in ? 0: no, 1: yes',
  `discount` decimal(15,2) DEFAULT NULL,
  `total` decimal(15,2) DEFAULT NULL COMMENT 'total amount that has to be reached before the coupon is valid',
  `start_date` timestamp NULL DEFAULT NULL COMMENT 'coupon is valid from this date',
  `end_date` timestamp NULL DEFAULT NULL COMMENT 'coupon is valid to this date',
  `num_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'number times this coupon can be used',
  `num_per_customer` int(11) DEFAULT NULL COMMENT 'number times single customers can use for this coupon',
  `status` tinyint(4) NOT NULL,
  `is_for_all` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:not for all, 1: for all',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_coupon`
--

TRUNCATE TABLE `ideas_coupon`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_coupon_history`
--

DROP TABLE IF EXISTS `ideas_coupon_history`;
CREATE TABLE `ideas_coupon_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `total` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_coupon_history`
--

TRUNCATE TABLE `ideas_coupon_history`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_coupon_to_category`
--

DROP TABLE IF EXISTS `ideas_coupon_to_category`;
CREATE TABLE `ideas_coupon_to_category` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_coupon_to_category`
--

TRUNCATE TABLE `ideas_coupon_to_category`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_coupon_to_product`
--

DROP TABLE IF EXISTS `ideas_coupon_to_product`;
CREATE TABLE `ideas_coupon_to_product` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_coupon_to_product`
--

TRUNCATE TABLE `ideas_coupon_to_product`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_currency`
--

DROP TABLE IF EXISTS `ideas_currency`;
CREATE TABLE `ideas_currency` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol_position` tinyint(4) NOT NULL COMMENT '0: before, 1: after',
  `value` double(15,8) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_currency`
--

TRUNCATE TABLE `ideas_currency`;
--
-- Dumping data for table `ideas_currency`
--

INSERT INTO `ideas_currency` (`id`, `name`, `code`, `symbol`, `symbol_position`, `value`, `date_modified`) VALUES
(1, 'Us Dollar', 'USD', '$', 0, 1.00000000, '2018-06-21 01:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_document`
--

DROP TABLE IF EXISTS `ideas_document`;
CREATE TABLE `ideas_document` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_document`
--

TRUNCATE TABLE `ideas_document`;
--
-- Dumping data for table `ideas_document`
--

INSERT INTO `ideas_document` (`id`, `name`, `description`) VALUES
(1, 'Installation', '<p>- First, you install this plugin:</p><p><a data-cke-saved-href=\"https://octobercms.com/plugin/rainlab-user\" href=\"https://octobercms.com/plugin/rainlab-user\" target=\"_blank\">Rainlab User plugin</a></p><p>- Then, you install ideas shop plugin:</p><p>Ideas shop plugin</p><p>- Then you can install demo template of ideas shop</p><p>Ideas shop theme</p><p>- Config mail : Settings (of octoberCMS) -&gt; Mail configuration -&gt; config your mail method (Send mail, SMTP, ....)</p><p>- Config mail templage:&nbsp;Settings (of octoberCMS) -&gt; Mail Templates -&gt; config mail of ideas shop (prefix ideas.shop::*) to layout \'Ideas Shop Mail Layout\'</p><h2>Product</h2><p>- To create product, you have to create tax class first (default is nontaxable class)</p><p>- in field \'Quantity\' : left 0 if you not manage stock</p><p>- If you create configurable product, you have to create filter and filter option first</p><h2>Order</h2><p>- Click to button \'not paid\' to change payment status to \'paid\'</p><p>- In the right top corner, there is button \'return order\'. When you click this button, you will use function \'return order\'</p><p>+ You can return each product individually with checkbox \'Check\'</p><p>+ If you don\'t want to reverse quantity of this product, you can un-check checkbox in column \'Qty reverse\'</p><h2>Config Default</h2><p>Go to Settings -&gt; Config Default</p><p>You can config something here:</p><p>- When you generate coupon code, you can config prefix of coupon code and length of code</p><p>- You can choose&nbsp;mail method between \'send\' and \'queue\', default is \'send\'. If you can access to server, you should choose \'queue\'</p><p>- You can enable \'paypal\' and \'stripe\' payment method in tab \'payment\'</p><p>- In tab \'Downloable Product\' &nbsp;you can choose:</p><p>+ Download once : if this config is enable, so your customer just can download your product 1 time. Then link will be expired.</p><p>If this config is disable. Your customer can download your product in 24 hours</p><p>+ Download right after checkout: if this funcion is enable, your customer can be receive email download immediately after pay</p><h2>Config plus</h2><p>Go to Settings -&gt; Config Plus</p><p>You can config something else if you want</p><h2>Theme manager</h2><p>Go to Settings -&gt; Theme manager</p><p>field \'type\' has two value \'text\' and \'image\'.</p><p>With type \'image\' , you can add 1 image or many images (gallery)&nbsp;</p><h2>Tax class</h2><p>Go to Settings -&gt; Tax Class manager</p><p>Before create a tax class, you have to create tax rate (Settings -&gt; Tax Rate manager)</p><h2>Coupon</h2><p>Go to Settings -&gt; Coupon Manager</p><p>Create coupon:</p><p>- field \'type\' : percentage or fixed amount</p><p>- field \'Number can be used\' : number times coupon can use, leave 0 if unlimited</p><p>- field \'Number times used by a customer\' : number times one customer can use, leave 0 if unlimited</p><p>- field \'category\' : category is applied&nbsp;by this coupon, leave empty if apply for all</p><p>- field \'product\' : product is applied by this coupon, leave emtpy if apply for all</p><p>- field \'Is Logged\' : does this coupn need logged in user ?</p><h2>Mail layout</h2><p>Settings (Of october cms) -&gt; Mail template -&gt; Layouts -&gt;&nbsp;Ideas Shop Mail Layout</p><p>=&gt; you can change html and css in tab \'HTML\' and \'CSS\'</p>'),
(2, 'Customize and extends', '<p>If you want to change something in Ideas Shop functions (backend). You can create a plugins then override (to add more field, save data to another table, ....)</p>\r\n\r\n<p><strong>1. extends by add columns</strong></p>\r\n\r\n<p>- For example, you like to add more field&nbsp;in table \'ideas_product\'</p>\r\n\r\n<p>- Create a&nbsp;plugin, in this plugin, create more (2) fields in table \'ideas_product\', for example : \'something1\', \'something2\'</p>\r\n\r\n<p>- in boot() of this plugin, add this code to extends form of product create page</p><pre><code>//extend form\r\nEvent::listen(\r\n&nbsp; &nbsp; \'backend.form.extendFields\',\r\n&nbsp; &nbsp; function ($widget) {\r\n&nbsp; &nbsp; &nbsp; &nbsp; // Only for the Product model\r\n&nbsp; &nbsp; &nbsp; &nbsp; if (!$widget-&gt;model instanceof \\Ideas\\Shop\\Models\\Products) {\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; return;\r\n&nbsp; &nbsp; &nbsp; &nbsp; }\r\n&nbsp; &nbsp; &nbsp; &nbsp; $widget-&gt;addTabFields([\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'something1\' =&gt; [\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'tab\' =&gt; \'other_tab\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'label\' =&gt; \'something1\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'span\' =&gt; \'left\'\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ],\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'something2\' =&gt; [\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'tab\' =&gt; \'other_tab\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'label\' =&gt; \'something2\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'span\' =&gt; \'left\'\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]\r\n&nbsp; &nbsp; &nbsp; &nbsp; ]);\r\n&nbsp; &nbsp; &nbsp; &nbsp; // Remove some fields\r\n&nbsp; &nbsp; &nbsp; &nbsp; //$widget-&gt;removeField(\'qty\');\r\n&nbsp; &nbsp; &nbsp; &nbsp; //$widget-&gt;removeField(\'price_promotion\');\r\n&nbsp; &nbsp; }\r\n);</code></pre>\r\n\r\n<p><strong>2. extends by use event of ideas shop</strong></p>\r\n\r\n<p>for example,we can add more field in create page, but name of field has prefix \'_\' &nbsp;: \'_something1\', \'_something2\'</p><pre><code>....\r\n&nbsp;$widget-&gt;addTabFields([\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'_something1\' =&gt; [\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'tab\' =&gt; \'other_tab\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'label\' =&gt; \'something1\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'span\' =&gt; \'left\'\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ],\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'_something2\' =&gt; [\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'tab\' =&gt; \'other_tab\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'label\' =&gt; \'something2\',\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; \'span\' =&gt; \'left\'\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ]\r\n&nbsp; &nbsp; &nbsp; &nbsp; ]);</code></pre>\r\n\r\n<p>Then, in&nbsp;boot() function:</p><pre><code>Event::listen(\'ideas.shop.save_product\', function ($productSaved, $post) {\r\n&nbsp; &nbsp; var_dump($productSaved);\r\n&nbsp; &nbsp; echo \'&lt;br/&gt;\';\r\n&nbsp; &nbsp; var_dump($post);\r\n&nbsp; &nbsp; die;\r\n});</code></pre>\r\n\r\n<p>=&gt; we use event \'ideas.shop.save_product\' to save \'_something1\' and \'_something2\' field data to another table</p>'),
(3, 'Override component views', '<p>Ideas Shop has some components for frontend. If you like to change your theme. You can override component of Ideas Shop.</p><p>For example:</p><p>If you like to override product list. You can create a file name \'product_list.htm\' in your_theme/partials/product.</p><p>You can copy code from \'/plugins/ideas/shop/components/product/product_list.htm\' then customize to whatever you want</p>'),
(4, 'Add payment method', '<p>- Override checkout/checkout.htm</p>\r\n\r\n<p>Create file \'checkout.htm\' in your_theme/partials/partials/ with code of file checkout.htm&nbsp;in component Checkout of IdeasShop, then change this code</p><pre><code>&lt;div class=\"checkbox\"&gt;\r\n&nbsp; &nbsp; &lt;label&gt;&lt;input type=\"radio\" name=\"payment_method\" value=\"1\"&gt;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=\"text-grey\"&gt;Cash on delivery&lt;/span&gt;&lt;/label&gt;&lt;br/&gt;\r\n\r\n&nbsp; &nbsp; {% if paypal == enable %}\r\n&nbsp; &nbsp; &lt;label&gt;&lt;input type=\"radio\" name=\"payment_method\" value=\"2\"&gt;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=\"text-grey\"&gt;Paypal&lt;/span&gt;&lt;/label&gt;&lt;br/&gt;\r\n&nbsp; &nbsp; {% endif %}\r\n\r\n&nbsp; &nbsp; {% if stripe == enable %}\r\n&nbsp; &nbsp; &lt;label&gt;&lt;input type=\"radio\" name=\"payment_method\" value=\"3\"&gt;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=\"text-grey\"&gt;Stripe&lt;/span&gt;&lt;/label&gt;&lt;br/&gt;\r\n&nbsp; &nbsp; {% endif %}\r\n\r\n&nbsp; &nbsp; &lt;label&gt;&lt;input type=\"radio\" name=\"payment_method\" value=\"4\"&gt;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=\"text-grey\"&gt;Other&lt;/span&gt;&lt;/label&gt;&lt;br/&gt;\r\n&lt;/div&gt;</code></pre>\r\n\r\n<p>=&gt; There are 3 payment method in IdeasShop : CoD, paypal, stripe with value:&nbsp;1, 2, 3</p>\r\n\r\n<p>=&gt; If you add more method, you can set from 4</p>\r\n\r\n<p>- You create your Component, then add component after component \'Checkout\' of IdeasShop:</p><pre><code>[Ideas\\Shop\\Components\\Checkout Checkout]\r\n[Ideas\\Shop\\Components\\AnotherPayment AnotherPayment]\r\n</code></pre>\r\n\r\n<p>- in your component, for example : \'AnotherPayment\', you add a file js:</p><pre><code>&nbsp; public function onRun()\r\n&nbsp; &nbsp; {\r\n&nbsp; &nbsp; &nbsp; &nbsp; $this-&gt;addJs(\'/plugins/ideas/shop/assets/components/js/another_payment.js\');\r\n&nbsp; &nbsp; }</code></pre>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- file another_payment.js:</p><pre><code>$(document).ready(function() {\r\n&nbsp; &nbsp; //call event anotherPaymentMethod\r\n&nbsp; &nbsp; $(document).on(\'anotherPaymentMethod\', function(e, params) {\r\n&nbsp; &nbsp; &nbsp; &nbsp; console.log(params);\r\n&nbsp; &nbsp; &nbsp; &nbsp; console.log(params.params);\r\n&nbsp; &nbsp; &nbsp; &nbsp; console.log(params.orderId);\r\n&nbsp; &nbsp; &nbsp; &nbsp; //do something\r\n&nbsp; &nbsp; &nbsp; &nbsp; //delete cart\r\n&nbsp; &nbsp; &nbsp; &nbsp; //sessionStorage.setItem(\'cart\', \'\');\r\n&nbsp; &nbsp; });\r\n});\r\n</code></pre>\r\n\r\n<p>=&gt; this file will run event : \'anotherPaymentMethod\' of IdeasShop</p>'),
(5, 'Event list', '<p>- ideas.shop.save_product</p><p>-&nbsp;ideas.shop.save_category</p><p>-&nbsp;ideas.shop.save_filter</p><p>-&nbsp;ideas.shop.save_filter_option</p><p>-&nbsp;ideas.shop.save_city</p><p>-&nbsp;ideas.shop.save_config</p><p>-&nbsp;ideas.shop.save_coupon</p><p>-&nbsp;ideas.shop.save_currency</p><p>-&nbsp;ideas.shop.save_geo</p><p>-&nbsp;ideas.shop.save_ship</p><p>-&nbsp;ideas.shop.save_tax_rate</p><p>-&nbsp;ideas.shop.save_tax_class</p><p>- ideas.shop.save_theme</p>'),
(6, 'Weight Based Shipping', '<ul>  <li>Check your store\'s default weight class and currency settings (in &nbsp;Settings &gt; Config default). Make sure they\'re set to what you want.</li> <li>Check your store\'s weight class values (in &nbsp;Settings &gt; &nbsp;Weight Management). Make sure your default class is set to 1.0000, and the others are set to appropriate conversion values. For example, if your default weight class is kg, then you would set its value to 1.0000, and set the value for grams to 1000.</li>  <li>Check your store\'s default currency value (in Settings &nbsp;&gt; Config default). Make sure your default currency value is set to 1.0000.</li>  <li>Since your default weight class is used in Weight Based Shipping, make sure your rates are using the appropriate values based on this weight class. So if you\'re using the rates 1:5.00, 2:7.00, etc. with a default weight class of kg, that means, \"Up to 1 kg charge 5.00, up to 2 kg charge 7.00, etc.\"</li> <li>Check your product weights and weight classes, and make sure they\'re set to what you want.</li></ul>'),
(7, 'Ideas shop mail layout', '<p>- Go to settings (of Octobercms) -&gt; Mail Template -&gt; choose mails that have prefix \'ideas.shop\' to set layout \'Ideas Shop Mail Layout\'</p><p>- Change css: Settings (of Octobercms) -&gt; Mail Template -&gt; tab \'LAYOUTS\' -&gt; choose \'Ideas Shop Mail Layout\' -&gt; choose tab \'CSS\' to change css of mail template.&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_filter`
--

DROP TABLE IF EXISTS `ideas_filter`;
CREATE TABLE `ideas_filter` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filter_order` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_filter`
--

TRUNCATE TABLE `ideas_filter`;
--
-- Dumping data for table `ideas_filter`
--

INSERT INTO `ideas_filter` (`id`, `name`, `slug`, `filter_order`) VALUES
(1, 'color', 'color', 0),
(2, 'size', 'size', 0),
(3, 'brand', 'brand', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_filter_option`
--

DROP TABLE IF EXISTS `ideas_filter_option`;
CREATE TABLE `ideas_filter_option` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filter_type` tinyint(4) NOT NULL COMMENT '1: color, 2: text, 3: image',
  `filter_id` int(10) UNSIGNED NOT NULL,
  `option_order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_filter_option`
--

TRUNCATE TABLE `ideas_filter_option`;
--
-- Dumping data for table `ideas_filter_option`
--

INSERT INTO `ideas_filter_option` (`id`, `name`, `slug`, `option_value`, `filter_type`, `filter_id`, `option_order`) VALUES
(1, 'White', 'white', '#ffffff', 1, 1, 1),
(2, 'Black', 'black', '#0f0f0f', 1, 1, 2),
(3, 'S', 's', 'S', 2, 2, 1),
(4, 'M', 'm', 'M', 2, 2, 2),
(5, 'L', 'l', 'L', 2, 2, 3),
(6, 'XL', 'xl', 'XL', 2, 2, 4),
(7, 'Sony', 'sony', 'Sony', 2, 3, 1),
(8, 'LG', 'lg', 'LG', 2, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_geo_zone`
--

DROP TABLE IF EXISTS `ideas_geo_zone`;
CREATE TABLE `ideas_geo_zone` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_geo_zone`
--

TRUNCATE TABLE `ideas_geo_zone`;
--
-- Dumping data for table `ideas_geo_zone`
--

INSERT INTO `ideas_geo_zone` (`id`, `name`, `description`) VALUES
(1, 'Area 1', '');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_length`
--

DROP TABLE IF EXISTS `ideas_length`;
CREATE TABLE `ideas_length` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_length`
--

TRUNCATE TABLE `ideas_length`;
--
-- Dumping data for table `ideas_length`
--

INSERT INTO `ideas_length` (`id`, `name`, `unit`, `value`) VALUES
(1, 'Centimeter', 'cm', '1.00'),
(2, 'Inch', 'in', '0.39');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_order`
--

DROP TABLE IF EXISTS `ideas_order`;
CREATE TABLE `ideas_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `billing_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_city_id` int(11) DEFAULT NULL,
  `billing_email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_post_code` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_city_id` int(11) DEFAULT NULL,
  `shipping_email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_post_code` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_rule_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL COMMENT '1: Cod; 2:paypal, 3:Stripe',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `shipping_cost` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `payment_status` tinyint(4) NOT NULL COMMENT '0: not paid; 1: paid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order`
--

TRUNCATE TABLE `ideas_order`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_order_product`
--

DROP TABLE IF EXISTS `ideas_order_product`;
CREATE TABLE `ideas_order_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL,
  `price_after_tax` decimal(15,2) NOT NULL,
  `total` decimal(15,2) NOT NULL,
  `weight` decimal(15,2) NOT NULL,
  `weight_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order_product`
--

TRUNCATE TABLE `ideas_order_product`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_order_return`
--

DROP TABLE IF EXISTS `ideas_order_return`;
CREATE TABLE `ideas_order_return` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `billing_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason_id` int(10) UNSIGNED NOT NULL,
  `qty_order` int(10) UNSIGNED NOT NULL,
  `is_reverse_order_qty` tinyint(4) DEFAULT '1' COMMENT '1: reverse order quantity, 0: not reverse',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order_return`
--

TRUNCATE TABLE `ideas_order_return`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_order_return_reason`
--

DROP TABLE IF EXISTS `ideas_order_return_reason`;
CREATE TABLE `ideas_order_return_reason` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order_return_reason`
--

TRUNCATE TABLE `ideas_order_return_reason`;
--
-- Dumping data for table `ideas_order_return_reason`
--

INSERT INTO `ideas_order_return_reason` (`id`, `name`) VALUES
(1, 'Order error');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_order_status`
--

DROP TABLE IF EXISTS `ideas_order_status`;
CREATE TABLE `ideas_order_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order_status`
--

TRUNCATE TABLE `ideas_order_status`;
--
-- Dumping data for table `ideas_order_status`
--

INSERT INTO `ideas_order_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Shipped'),
(3, 'Processing'),
(4, 'Complete'),
(5, 'Denied'),
(6, 'Failed'),
(7, 'Refunded'),
(8, 'Canceled'),
(9, 'On hold');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_order_status_change`
--

DROP TABLE IF EXISTS `ideas_order_status_change`;
CREATE TABLE `ideas_order_status_change` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_order_status_change`
--

TRUNCATE TABLE `ideas_order_status_change`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product`
--

DROP TABLE IF EXISTS `ideas_product`;
CREATE TABLE `ideas_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(15,2) DEFAULT '0.00',
  `price_promotion` decimal(15,2) DEFAULT '0.00',
  `qty` int(11) DEFAULT '0',
  `qty_order` int(11) DEFAULT '0',
  `featured_image` text COLLATE utf8mb4_unicode_ci,
  `gallery` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `product_order` int(10) UNSIGNED DEFAULT NULL,
  `product_type` tinyint(4) NOT NULL DEFAULT '1',
  `tax_class_id` int(11) NOT NULL,
  `review_count` int(11) DEFAULT '0',
  `related_product` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product`
--

TRUNCATE TABLE `ideas_product`;
--
-- Dumping data for table `ideas_product`
--

INSERT INTO `ideas_product` (`id`, `name`, `slug`, `sku`, `price`, `price_promotion`, `qty`, `qty_order`, `featured_image`, `gallery`, `status`, `product_order`, `product_type`, `tax_class_id`, `review_count`, `related_product`, `created_at`, `updated_at`) VALUES
(1, 'Product 1', 'product-1', '', '53.00', '53.00', 883, 0, '/product/9.jpg', '/product/2.jpg;/product/1.jpg;/product/5.jpg;/product/9.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(2, 'Product 2', 'product-2', '', '48.00', '48.00', 666, 0, '/product/3.jpg', '/product/1.jpg;/product/1.jpg;/product/6.jpg;/product/9.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(3, 'Product 3', 'product-3', '', '75.00', '75.00', 326, 0, '/product/5.jpg', '/product/1.jpg;/product/5.jpg;/product/2.jpg;/product/2.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(4, 'Product 4', 'product-4', '', '48.00', '48.00', 832, 0, '/product/3.jpg', '/product/2.jpg;/product/3.jpg;/product/3.jpg;/product/5.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(5, 'Product 5', 'product-5', '', '14.00', '14.00', 233, 0, '/product/1.jpg', '/product/4.jpg;/product/2.jpg;/product/1.jpg;/product/10.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(6, 'Product 6', 'product-6', '', '39.00', '39.00', 891, 0, '/product/2.jpg', '/product/6.jpg;/product/2.jpg;/product/10.jpg;/product/4.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(7, 'Product 7', 'product-7', '', '14.00', '14.00', 650, 0, '/product/7.jpg', '/product/4.jpg;/product/6.jpg;/product/3.jpg;/product/6.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(8, 'Product 8', 'product-8', '', '95.00', '95.00', 685, 0, '/product/9.jpg', '/product/6.jpg;/product/5.jpg;/product/2.jpg;/product/5.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(9, 'Product 9', 'product-9', '', '35.00', '35.00', 339, 0, '/product/9.jpg', '/product/2.jpg;/product/9.jpg;/product/2.jpg;/product/1.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(10, 'Product 10', 'product-10', '', '85.00', '85.00', 508, 0, '/product/4.jpg', '/product/4.jpg;/product/2.jpg;/product/6.jpg;/product/3.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50'),
(11, 'Product 11', 'product-11', '', '99.00', '99.00', 724, 0, '/product/3.jpg', '/product/2.jpg;/product/6.jpg;/product/2.jpg;/product/1.jpg', 1, 0, 1, 1, 0, NULL, '2018-06-21 13:02:50', '2018-06-21 13:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_attribute`
--

DROP TABLE IF EXISTS `ideas_product_attribute`;
CREATE TABLE `ideas_product_attribute` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `short_intro` text COLLATE utf8mb4_unicode_ci,
  `full_intro` text COLLATE utf8mb4_unicode_ci,
  `seo_title` text COLLATE utf8mb4_unicode_ci,
  `seo_keyword` text COLLATE utf8mb4_unicode_ci,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `is_homepage` tinyint(4) NOT NULL,
  `is_featured_product` tinyint(4) NOT NULL,
  `is_new` tinyint(4) NOT NULL,
  `is_bestseller` tinyint(4) NOT NULL,
  `weight` decimal(15,2) NOT NULL,
  `weight_id` int(10) UNSIGNED NOT NULL,
  `length` decimal(15,2) DEFAULT NULL,
  `width` decimal(15,2) DEFAULT NULL,
  `height` decimal(15,2) DEFAULT NULL,
  `length_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_attribute`
--

TRUNCATE TABLE `ideas_product_attribute`;
--
-- Dumping data for table `ideas_product_attribute`
--

INSERT INTO `ideas_product_attribute` (`id`, `product_id`, `short_intro`, `full_intro`, `seo_title`, `seo_keyword`, `seo_description`, `is_homepage`, `is_featured_product`, `is_new`, `is_bestseller`, `weight`, `weight_id`, `length`, `width`, `height`, `length_id`) VALUES
(1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 1', 'product 1', 'product 1', 1, 1, 1, 1, '8.00', 1, '1.00', '1.00', '1.00', 1),
(2, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 2', 'product 2', 'product 2', 1, 1, 1, 1, '7.00', 1, '1.00', '1.00', '1.00', 1),
(3, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 3', 'product 3', 'product 3', 1, 1, 1, 1, '9.00', 1, '1.00', '1.00', '1.00', 1),
(4, 4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 4', 'product 4', 'product 4', 1, 1, 1, 1, '2.00', 1, '1.00', '1.00', '1.00', 1),
(5, 5, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 5', 'product 5', 'product 5', 1, 1, 1, 1, '5.00', 1, '1.00', '1.00', '1.00', 1),
(6, 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 6', 'product 6', 'product 6', 1, 1, 1, 1, '4.00', 1, '1.00', '1.00', '1.00', 1),
(7, 7, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 7', 'product 7', 'product 7', 1, 1, 1, 1, '1.00', 1, '1.00', '1.00', '1.00', 1),
(8, 8, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 8', 'product 8', 'product 8', 1, 1, 1, 1, '3.00', 1, '1.00', '1.00', '1.00', 1),
(9, 9, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 9', 'product 9', 'product 9', 1, 1, 1, 1, '10.00', 1, '1.00', '1.00', '1.00', 1),
(10, 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 10', 'product 10', 'product 10', 1, 1, 1, 1, '7.00', 1, '1.00', '1.00', '1.00', 1),
(11, 11, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident', 'product 11', 'product 11', 'product 11', 1, 1, 1, 1, '6.00', 1, '1.00', '1.00', '1.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_child_to_category`
--

DROP TABLE IF EXISTS `ideas_product_child_to_category`;
CREATE TABLE `ideas_product_child_to_category` (
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'id of product child of configurable product',
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_child_to_category`
--

TRUNCATE TABLE `ideas_product_child_to_category`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_configurable`
--

DROP TABLE IF EXISTS `ideas_product_configurable`;
CREATE TABLE `ideas_product_configurable` (
  `id` int(10) UNSIGNED NOT NULL,
  `str_filter_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `str_filter_option_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `str_filter_option_label` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pc_product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'id of product config child',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'id of product config parent'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_configurable`
--

TRUNCATE TABLE `ideas_product_configurable`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_downloadable`
--

DROP TABLE IF EXISTS `ideas_product_downloadable`;
CREATE TABLE `ideas_product_downloadable` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `link` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_downloadable`
--

TRUNCATE TABLE `ideas_product_downloadable`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_downloadable_link`
--

DROP TABLE IF EXISTS `ideas_product_downloadable_link`;
CREATE TABLE `ideas_product_downloadable_link` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `code` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_downloadable_link`
--

TRUNCATE TABLE `ideas_product_downloadable_link`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_reviews`
--

DROP TABLE IF EXISTS `ideas_product_reviews`;
CREATE TABLE `ideas_product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT '0',
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` tinyint(4) DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_reviews`
--

TRUNCATE TABLE `ideas_product_reviews`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_to_category`
--

DROP TABLE IF EXISTS `ideas_product_to_category`;
CREATE TABLE `ideas_product_to_category` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_to_category`
--

TRUNCATE TABLE `ideas_product_to_category`;
--
-- Dumping data for table `ideas_product_to_category`
--

INSERT INTO `ideas_product_to_category` (`product_id`, `category_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_product_to_filter_option`
--

DROP TABLE IF EXISTS `ideas_product_to_filter_option`;
CREATE TABLE `ideas_product_to_filter_option` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `filter_option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_product_to_filter_option`
--

TRUNCATE TABLE `ideas_product_to_filter_option`;
--
-- Dumping data for table `ideas_product_to_filter_option`
--

INSERT INTO `ideas_product_to_filter_option` (`product_id`, `filter_option_id`) VALUES
(1, 4),
(2, 7),
(3, 4),
(4, 1),
(5, 2),
(6, 3),
(7, 2),
(8, 2),
(9, 2),
(10, 4),
(11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_routes`
--

DROP TABLE IF EXISTS `ideas_routes`;
CREATE TABLE `ideas_routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: product, 2: category'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_routes`
--

TRUNCATE TABLE `ideas_routes`;
--
-- Dumping data for table `ideas_routes`
--

INSERT INTO `ideas_routes` (`id`, `slug`, `entity_id`, `type`) VALUES
(1, 'uncategory', 1, 2),
(2, 'laptop', 2, 2),
(3, 'product-1', 1, 1),
(4, 'product-2', 2, 1),
(5, 'product-3', 3, 1),
(6, 'product-4', 4, 1),
(7, 'product-5', 5, 1),
(8, 'product-6', 6, 1),
(9, 'product-7', 7, 1),
(10, 'product-8', 8, 1),
(11, 'product-9', 9, 1),
(12, 'product-10', 10, 1),
(13, 'product-11', 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_ship_rule`
--

DROP TABLE IF EXISTS `ideas_ship_rule`;
CREATE TABLE `ideas_ship_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `above_price` decimal(15,2) DEFAULT NULL,
  `geo_zone_id` int(10) UNSIGNED DEFAULT NULL,
  `weight_type` tinyint(4) DEFAULT NULL COMMENT '1:fixed, 2:rate',
  `weight_based` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` decimal(15,2) DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: price, 2: geo, 3: weight based, 4: per item, 5: geo weight based',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_ship_rule`
--

TRUNCATE TABLE `ideas_ship_rule`;
--
-- Dumping data for table `ideas_ship_rule`
--

INSERT INTO `ideas_ship_rule` (`id`, `name`, `above_price`, `geo_zone_id`, `weight_type`, `weight_based`, `cost`, `type`, `status`) VALUES
(1, 'Pick up from store', '0.00', NULL, NULL, '0', '0.00', 1, 1),
(2, 'Flat price', '50.00', NULL, NULL, '0', '10.00', 1, 1),
(3, 'weight based 1', '0.00', NULL, 1, '1:5', '0.00', 3, 1),
(4, 'weight based rate 1', '0.00', NULL, 2, '6:13.50,9:14.00,12:14.50,17:15.00,22:15.50,24:16.00,27:16.50,32:17.00,35:17.50,38:18.00,42:18.50,47:19.00,51:19.50,60:20.00,63:20.50,66:21.00,70:21.50,72:22.00,73:24.50', '0.00', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_tax_class`
--

DROP TABLE IF EXISTS `ideas_tax_class`;
CREATE TABLE `ideas_tax_class` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_tax_class`
--

TRUNCATE TABLE `ideas_tax_class`;
--
-- Dumping data for table `ideas_tax_class`
--

INSERT INTO `ideas_tax_class` (`id`, `name`, `description`) VALUES
(1, 'Nontaxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_tax_rate`
--

DROP TABLE IF EXISTS `ideas_tax_rate`;
CREATE TABLE `ideas_tax_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: percentage, 2: fixed amount',
  `geo_zone_id` int(10) UNSIGNED NOT NULL,
  `rate` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_tax_rate`
--

TRUNCATE TABLE `ideas_tax_rate`;
--
-- Dumping data for table `ideas_tax_rate`
--

INSERT INTO `ideas_tax_rate` (`id`, `name`, `type`, `geo_zone_id`, `rate`) VALUES
(1, 'Nontaxable Rate', 0, 1, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `ideas_tax_rule`
--

DROP TABLE IF EXISTS `ideas_tax_rule`;
CREATE TABLE `ideas_tax_rule` (
  `tax_class_id` int(10) UNSIGNED NOT NULL,
  `tax_rate_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_tax_rule`
--

TRUNCATE TABLE `ideas_tax_rule`;
--
-- Dumping data for table `ideas_tax_rule`
--

INSERT INTO `ideas_tax_rule` (`tax_class_id`, `tax_rate_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_theme_config`
--

DROP TABLE IF EXISTS `ideas_theme_config`;
CREATE TABLE `ideas_theme_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1:textarea, 2:image'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_theme_config`
--

TRUNCATE TABLE `ideas_theme_config`;
--
-- Dumping data for table `ideas_theme_config`
--

INSERT INTO `ideas_theme_config` (`id`, `name`, `slug`, `type`) VALUES
(1, 'Homepage Gallery', 'homepage_gallery', 2),
(2, 'Welcome to our shop', 'welcome_to_our_shop', 2),
(3, 'Testimonials', 'testimonials', 2),
(4, 'Ship slide', 'ship_slide', 2),
(5, 'Image header bottom', 'image_header_bottom', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_theme_config_image_detail`
--

DROP TABLE IF EXISTS `ideas_theme_config_image_detail`;
CREATE TABLE `ideas_theme_config_image_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_order` int(10) UNSIGNED NOT NULL,
  `theme_config_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_theme_config_image_detail`
--

TRUNCATE TABLE `ideas_theme_config_image_detail`;
--
-- Dumping data for table `ideas_theme_config_image_detail`
--

INSERT INTO `ideas_theme_config_image_detail` (`id`, `name`, `url`, `link`, `title`, `alt`, `description`, `image_order`, `theme_config_id`) VALUES
(1, 'image 1', '/theme/slider1.jpg', NULL, NULL, NULL, NULL, 1, 1),
(2, 'image 2', '/theme/slider1.jpg', NULL, NULL, NULL, NULL, 2, 1),
(3, 'image 3', '/theme/slider1.jpg', NULL, NULL, NULL, NULL, 3, 1),
(4, 'welcome 1', '/product/3.jpg', NULL, NULL, NULL, NULL, 1, 2),
(5, 'welcome 2', '/product/5.jpg', NULL, NULL, NULL, NULL, 2, 2),
(6, 'welcome 3', '/product/7.jpg', NULL, NULL, NULL, NULL, 3, 2),
(7, 'welcome 4', '/product/5.jpg', NULL, NULL, NULL, NULL, 4, 2),
(8, 'welcome 5', '/product/5.jpg', NULL, NULL, NULL, NULL, 5, 2),
(9, 'testimonial 1', '/theme/user-1.jpg', NULL, 'John Doe 1', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 1, 3),
(10, 'testimonial 2', '/theme/user-2.jpg', NULL, 'John Doe 2', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 2, 3),
(11, 'testimonial 3', '/theme/user-3.jpg', NULL, 'John Doe 3', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 3, 3),
(12, 'testimonial 4', '/theme/user-4.jpg', NULL, 'John Doe 4', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 4, 3),
(13, 'ship 1', '/theme/ship-1.jpg', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 1, 4),
(14, 'ship 2', '/theme/ship-2.jpg', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 2, 4),
(15, 'ship 3', '/theme/ship-3.jpg', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 3, 4),
(16, 'Image header bottom', '/theme/slider1.jpg', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ideas_theme_config_text_value`
--

DROP TABLE IF EXISTS `ideas_theme_config_text_value`;
CREATE TABLE `ideas_theme_config_text_value` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `theme_config_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_theme_config_text_value`
--

TRUNCATE TABLE `ideas_theme_config_text_value`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_users_extends`
--

DROP TABLE IF EXISTS `ideas_users_extends`;
CREATE TABLE `ideas_users_extends` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_users_extends`
--

TRUNCATE TABLE `ideas_users_extends`;
-- --------------------------------------------------------

--
-- Table structure for table `ideas_weight`
--

DROP TABLE IF EXISTS `ideas_weight`;
CREATE TABLE `ideas_weight` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `ideas_weight`
--

TRUNCATE TABLE `ideas_weight`;
--
-- Dumping data for table `ideas_weight`
--

INSERT INTO `ideas_weight` (`id`, `name`, `unit`, `value`) VALUES
(1, 'Kilograms', 'Kg', '1.00'),
(2, 'Gram', 'g', '1000.00'),
(3, 'Pound', 'lb', '2.20');

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_brands`
--

DROP TABLE IF EXISTS `jiri_jkshop_brands`;
CREATE TABLE `jiri_jkshop_brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_brands`
--

TRUNCATE TABLE `jiri_jkshop_brands`;
--
-- Dumping data for table `jiri_jkshop_brands`
--

INSERT INTO `jiri_jkshop_brands` (`id`, `created_at`, `updated_at`, `title`, `slug`, `description`, `active`, `show_in_menu`, `meta_title`, `meta_keywords`, `meta_description`, `short_description`) VALUES
(1, '2018-06-28 18:41:53', '2018-06-28 18:41:53', 'HP', 'hp', '', 1, 1, '', '', '', '<p>HP\r\n	<br>\r\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_carriers`
--

DROP TABLE IF EXISTS `jiri_jkshop_carriers`;
CREATE TABLE `jiri_jkshop_carriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `transit_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speed_grade` int(11) DEFAULT NULL,
  `tracking_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_shipping` tinyint(1) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `billing` int(11) DEFAULT NULL,
  `billing_total_price` text COLLATE utf8mb4_unicode_ci,
  `billing_weight` text COLLATE utf8mb4_unicode_ci,
  `maximum_package_width` double DEFAULT NULL,
  `maximum_package_height` double DEFAULT NULL,
  `maximum_package_depth` double DEFAULT NULL,
  `maximum_package_weight` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_carriers`
--

TRUNCATE TABLE `jiri_jkshop_carriers`;
--
-- Dumping data for table `jiri_jkshop_carriers`
--

INSERT INTO `jiri_jkshop_carriers` (`id`, `created_at`, `updated_at`, `title`, `active`, `transit_time`, `speed_grade`, `tracking_url`, `free_shipping`, `tax_id`, `billing`, `billing_total_price`, `billing_weight`, `maximum_package_width`, `maximum_package_height`, `maximum_package_depth`, `maximum_package_weight`) VALUES
(1, NULL, NULL, 'Carrier', 1, '2 days', 0, 'http://exampl.com/track.php?num=@', 0, 1, 2, '{\\\"1\\\":{\\\"from\\\":\\\"0\\\",\\\"to\\\":\\\"999999999999\\\",\\\"price\\\":\\\"11\\\"}}', '{\\\"1\\\":{\\\"from\\\":\\\"0\\\",\\\"to\\\":\\\"9999\\\",\\\"price\\\":\\\"3.99\\\"}}', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_categories`;
CREATE TABLE `jiri_jkshop_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) NOT NULL DEFAULT '0',
  `nest_right` int(11) NOT NULL DEFAULT '0',
  `nest_depth` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_categories`
--

TRUNCATE TABLE `jiri_jkshop_categories`;
--
-- Dumping data for table `jiri_jkshop_categories`
--

INSERT INTO `jiri_jkshop_categories` (`id`, `created_at`, `updated_at`, `title`, `slug`, `description`, `active`, `show_in_menu`, `meta_title`, `meta_keywords`, `meta_description`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`) VALUES
(1, '2018-06-28 18:35:05', '2018-06-28 18:36:01', 'HP Series', 'hp-series', '', 1, 1, '', '', '', 2, 2, 3, 1),
(2, '2018-06-28 18:35:36', '2018-06-28 18:35:58', 'Laser Machines', 'laser-machines', '', 1, 1, '', '', '', NULL, 1, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_categories_users_sale`
--

DROP TABLE IF EXISTS `jiri_jkshop_categories_users_sale`;
CREATE TABLE `jiri_jkshop_categories_users_sale` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sale` double UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_categories_users_sale`
--

TRUNCATE TABLE `jiri_jkshop_categories_users_sale`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons`;
CREATE TABLE `jiri_jkshop_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_from` timestamp NULL DEFAULT NULL,
  `valid_to` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `used_count` int(11) DEFAULT NULL,
  `minimum_value_basket` double DEFAULT NULL,
  `type_value` int(11) DEFAULT NULL,
  `value` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_coupons`
--

TRUNCATE TABLE `jiri_jkshop_coupons`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_categories`;
CREATE TABLE `jiri_jkshop_coupons_categories` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_coupons_categories`
--

TRUNCATE TABLE `jiri_jkshop_coupons_categories`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_products`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_products`;
CREATE TABLE `jiri_jkshop_coupons_products` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_coupons_products`
--

TRUNCATE TABLE `jiri_jkshop_coupons_products`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_users`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_users`;
CREATE TABLE `jiri_jkshop_coupons_users` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_coupons_users`
--

TRUNCATE TABLE `jiri_jkshop_coupons_users`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_orders`
--

DROP TABLE IF EXISTS `jiri_jkshop_orders`;
CREATE TABLE `jiri_jkshop_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `orderstatus_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ds_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_id` int(10) UNSIGNED DEFAULT NULL,
  `products_json` text COLLATE utf8mb4_unicode_ci,
  `total_price_without_tax` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `shipping_price_without_tax` double DEFAULT NULL,
  `shipping_tax` double DEFAULT NULL,
  `shipping_price` double DEFAULT NULL,
  `paid_date` timestamp NULL DEFAULT NULL,
  `paid_detail` text COLLATE utf8mb4_unicode_ci,
  `payment_method` int(10) UNSIGNED DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `ds_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_global_discount` double DEFAULT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_gateway_id` int(10) UNSIGNED DEFAULT NULL,
  `security_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_orders`
--

TRUNCATE TABLE `jiri_jkshop_orders`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_order_statuses`
--

DROP TABLE IF EXISTS `jiri_jkshop_order_statuses`;
CREATE TABLE `jiri_jkshop_order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `send_email_to_customer` tinyint(1) DEFAULT NULL,
  `attach_invoice_pdf_to_email` tinyint(1) DEFAULT NULL,
  `mail_template_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_increase_back` tinyint(1) NOT NULL DEFAULT '0',
  `qty_decrease` tinyint(1) NOT NULL DEFAULT '0',
  `disallow_for_gateway` tinyint(1) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_order_statuses`
--

TRUNCATE TABLE `jiri_jkshop_order_statuses`;
--
-- Dumping data for table `jiri_jkshop_order_statuses`
--

INSERT INTO `jiri_jkshop_order_statuses` (`id`, `created_at`, `updated_at`, `title`, `color`, `active`, `send_email_to_customer`, `attach_invoice_pdf_to_email`, `mail_template_id`, `qty_increase_back`, `qty_decrease`, `disallow_for_gateway`, `paid`) VALUES
(1, NULL, NULL, 'New Order - Cash on Delivery', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(2, NULL, NULL, 'New Order - PayPal', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(3, NULL, NULL, 'New Order - Bank transfer', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(4, NULL, NULL, 'Payment Received', '#9b59b6', 1, 1, 0, NULL, 0, 0, 0, 0),
(5, NULL, NULL, 'Cancel Order', '#c0392b', 1, 1, 0, NULL, 0, 0, 0, 0),
(6, NULL, NULL, 'Expedited Order', '#2ecc71', 1, 1, 1, NULL, 0, 0, 0, 0),
(7, NULL, NULL, 'Expedited Order - Cash on Delivery', '#27ae60', 1, 1, 1, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_payment_gateways`
--

DROP TABLE IF EXISTS `jiri_jkshop_payment_gateways`;
CREATE TABLE `jiri_jkshop_payment_gateways` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_status_before_id` int(10) UNSIGNED DEFAULT NULL,
  `order_status_after_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `gateway` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_currency` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_page` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_payment_gateways`
--

TRUNCATE TABLE `jiri_jkshop_payment_gateways`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products`
--

DROP TABLE IF EXISTS `jiri_jkshop_products`;
CREATE TABLE `jiri_jkshop_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ean_13` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `on_sale` tinyint(1) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT NULL,
  `available_for_order` tinyint(1) DEFAULT NULL,
  `show_price` tinyint(1) DEFAULT NULL,
  `condition` int(11) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `brand_id` int(11) DEFAULT NULL,
  `pre_tax_wholesale_price` double DEFAULT NULL,
  `pre_tax_retail_price` double DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `retail_price_with_tax` double DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `default_category_id` int(11) DEFAULT NULL,
  `package_width` double DEFAULT NULL,
  `package_height` double DEFAULT NULL,
  `package_depth` double DEFAULT NULL,
  `package_weight` double DEFAULT NULL,
  `additional_shipping_fees` double DEFAULT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `when_out_of_stock` int(11) DEFAULT NULL,
  `minimum_quantity` int(10) UNSIGNED NOT NULL,
  `availability_date` timestamp NULL DEFAULT NULL,
  `customization` text COLLATE utf8mb4_unicode_ci,
  `virtual_product` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products`
--

TRUNCATE TABLE `jiri_jkshop_products`;
--
-- Dumping data for table `jiri_jkshop_products`
--

INSERT INTO `jiri_jkshop_products` (`id`, `created_at`, `updated_at`, `title`, `slug`, `ean_13`, `barcode`, `active`, `on_sale`, `visibility`, `available_for_order`, `show_price`, `condition`, `short_description`, `description`, `brand_id`, `pre_tax_wholesale_price`, `pre_tax_retail_price`, `tax_id`, `retail_price_with_tax`, `meta_title`, `meta_keywords`, `meta_description`, `default_category_id`, `package_width`, `package_height`, `package_depth`, `package_weight`, `additional_shipping_fees`, `quantity`, `when_out_of_stock`, `minimum_quantity`, `availability_date`, `customization`, `virtual_product`) VALUES
(1, '2018-06-28 18:42:03', '2018-06-28 18:42:38', 'BOSS HP-2436', 'boss-hp-2436', '', '', 1, 0, 1, 1, 1, 0, '<p>High Power 150W – 24×36″ laser cutter delivers the ultimate in material flexibility. <strong>Vector cut or raster engrave metal or non metal materials</strong>. &nbsp;Multi-material pass thru options (4) for greater material handling. New Flex™ and VectorDC™ auto focus laser heads…</p>', '<ul>\r\n	<li><strong>Call to schedule a LIVE demo here at our location in Orlando, FL!</strong></li>\r\n	<li><strong>2017 designed and engineered by BossLaser</strong></li>\r\n	<li>Vector cut and/or raster engrave both metal and non-metal materials</li>\r\n	<li>Capable of <strong>cleanly cutting</strong> 18 ga steel (1.02mm)</li>\r\n	<li>Includes both the <strong>Flex™ and the new VectorDC™&nbsp;</strong>reciprocating auto focus laser heads for clean raster and deep cutting</li>\r\n	<li>4-way Material Pass Thru points for processing over sized materials</li>\r\n	<li><strong>NEW!</strong>Operator friendly LaserCAD™ premium laser software designed for both metal &amp; non-metal precision processing</li>\r\n	<li><strong>Wi-Fi or USB</strong> connection to PC Option</li>\r\n	<li>Inline Beam Combiner accurately illuminates a red dot on the &nbsp;material which is helps start up precision – Superior to red dot &nbsp;pointer.</li>\r\n	<li><strong>U.S. optics from <em>American Photonics</em> improves beam transmission by 14% vs. imported optics.</strong></li>\r\n	<li>Push-button Z-axis for accommodating <strong>7″ tall objects.</strong></li>\r\n	<li><strong>Includes Industrial Water Chiller w/ pump&nbsp;</strong>(CW-5000) keeps water flowing thru CO2 Tube at ambient temperature.</li>\r\n	<li>Includes Max output <strong>Air Compressor w/ exhaust fan &amp; tubing</strong> for air assistance at the laser head.</li>\r\n	<li>Convenient vacuum table and sliding tray captures finished product</li>\r\n	<li>Knife blade table included for laser processing a broad array of materials</li>\r\n	<li>Industrial CNC machined replacement parts in stock for long term service</li>\r\n	<li>Ball bearing gantry promotes smooth, precision moving laser head</li>\r\n	<li>Optional Chuck or Roller rotary attachment for cylindrical objects</li>\r\n	<li>Works with Windows 10, 8, 7 (64 or 32 bit), XP – NO MAC</li>\r\n	<li>USA tech support available to serve you Monday-Saturday</li>\r\n	<li><strong>FREE Lifetime Tech Support</strong>. We are a tech support driven company</li>\r\n	<li>Pick-up available at our Orlando/Sanford, FL facility</li>\r\n	<li><strong>1 year Warranty</strong>covers CO2 Laser machine and laser tube.</li>\r\n	<li>Available <strong>30 Day&nbsp;</strong>Return Policy</li>\r\n	<li>LTL shipping with lift gate for off loading (not included in initial cost)</li>\r\n	<li><strong>Have questions? Please call us at 407.878.0880 for help!</strong></li>\r\n</ul>', 1, 14, 200, 1, 242, '', '', '', 1, 24, 36, 38, 10, NULL, 20, 0, 1, '2018-07-21 18:39:29', '[]', 0),
(2, '2018-06-28 18:42:55', '2018-06-28 18:42:55', 'BOSS HP-2436-duplicate-1', 'boss-hp-2436-duplicate-1', '', '', 1, 0, 1, 1, 1, 0, '<p>High Power 150W – 24×36″ laser cutter delivers the ultimate in material flexibility. <strong>Vector cut or raster engrave metal or non metal materials</strong>. &nbsp;Multi-material pass thru options (4) for greater material handling. New Flex™ and VectorDC™ auto focus laser heads…</p>', '<ul>\r\n	<li><strong>Call to schedule a LIVE demo here at our location in Orlando, FL!</strong></li>\r\n	<li><strong>2017 designed and engineered by BossLaser</strong></li>\r\n	<li>Vector cut and/or raster engrave both metal and non-metal materials</li>\r\n	<li>Capable of <strong>cleanly cutting</strong> 18 ga steel (1.02mm)</li>\r\n	<li>Includes both the <strong>Flex™ and the new VectorDC™&nbsp;</strong>reciprocating auto focus laser heads for clean raster and deep cutting</li>\r\n	<li>4-way Material Pass Thru points for processing over sized materials</li>\r\n	<li><strong>NEW!</strong>Operator friendly LaserCAD™ premium laser software designed for both metal &amp; non-metal precision processing</li>\r\n	<li><strong>Wi-Fi or USB</strong> connection to PC Option</li>\r\n	<li>Inline Beam Combiner accurately illuminates a red dot on the &nbsp;material which is helps start up precision – Superior to red dot &nbsp;pointer.</li>\r\n	<li><strong>U.S. optics from <em>American Photonics</em> improves beam transmission by 14% vs. imported optics.</strong></li>\r\n	<li>Push-button Z-axis for accommodating <strong>7″ tall objects.</strong></li>\r\n	<li><strong>Includes Industrial Water Chiller w/ pump&nbsp;</strong>(CW-5000) keeps water flowing thru CO2 Tube at ambient temperature.</li>\r\n	<li>Includes Max output <strong>Air Compressor w/ exhaust fan &amp; tubing</strong> for air assistance at the laser head.</li>\r\n	<li>Convenient vacuum table and sliding tray captures finished product</li>\r\n	<li>Knife blade table included for laser processing a broad array of materials</li>\r\n	<li>Industrial CNC machined replacement parts in stock for long term service</li>\r\n	<li>Ball bearing gantry promotes smooth, precision moving laser head</li>\r\n	<li>Optional Chuck or Roller rotary attachment for cylindrical objects</li>\r\n	<li>Works with Windows 10, 8, 7 (64 or 32 bit), XP – NO MAC</li>\r\n	<li>USA tech support available to serve you Monday-Saturday</li>\r\n	<li><strong>FREE Lifetime Tech Support</strong>. We are a tech support driven company</li>\r\n	<li>Pick-up available at our Orlando/Sanford, FL facility</li>\r\n	<li><strong>1 year Warranty</strong>covers CO2 Laser machine and laser tube.</li>\r\n	<li>Available <strong>30 Day&nbsp;</strong>Return Policy</li>\r\n	<li>LTL shipping with lift gate for off loading (not included in initial cost)</li>\r\n	<li><strong>Have questions? Please call us at 407.878.0880 for help!</strong></li>\r\n</ul>', 1, 14, 200, 1, 242, '', '', '', 1, 24, 36, 38, 10, NULL, 20, 0, 1, '2018-07-21 18:39:29', '[]', 0),
(3, '2018-06-28 18:43:08', '2018-06-28 18:43:08', 'BOSS HP-2436-duplicate-1-duplicate-1', 'boss-hp-2436-duplicate-1-duplicate-1', '', '', 1, 0, 1, 1, 1, 0, '<p>High Power 150W – 24×36″ laser cutter delivers the ultimate in material flexibility. <strong>Vector cut or raster engrave metal or non metal materials</strong>. &nbsp;Multi-material pass thru options (4) for greater material handling. New Flex™ and VectorDC™ auto focus laser heads…</p>', '<ul>\r\n	<li><strong>Call to schedule a LIVE demo here at our location in Orlando, FL!</strong></li>\r\n	<li><strong>2017 designed and engineered by BossLaser</strong></li>\r\n	<li>Vector cut and/or raster engrave both metal and non-metal materials</li>\r\n	<li>Capable of <strong>cleanly cutting</strong> 18 ga steel (1.02mm)</li>\r\n	<li>Includes both the <strong>Flex™ and the new VectorDC™&nbsp;</strong>reciprocating auto focus laser heads for clean raster and deep cutting</li>\r\n	<li>4-way Material Pass Thru points for processing over sized materials</li>\r\n	<li><strong>NEW!</strong>Operator friendly LaserCAD™ premium laser software designed for both metal &amp; non-metal precision processing</li>\r\n	<li><strong>Wi-Fi or USB</strong> connection to PC Option</li>\r\n	<li>Inline Beam Combiner accurately illuminates a red dot on the &nbsp;material which is helps start up precision – Superior to red dot &nbsp;pointer.</li>\r\n	<li><strong>U.S. optics from <em>American Photonics</em> improves beam transmission by 14% vs. imported optics.</strong></li>\r\n	<li>Push-button Z-axis for accommodating <strong>7″ tall objects.</strong></li>\r\n	<li><strong>Includes Industrial Water Chiller w/ pump&nbsp;</strong>(CW-5000) keeps water flowing thru CO2 Tube at ambient temperature.</li>\r\n	<li>Includes Max output <strong>Air Compressor w/ exhaust fan &amp; tubing</strong> for air assistance at the laser head.</li>\r\n	<li>Convenient vacuum table and sliding tray captures finished product</li>\r\n	<li>Knife blade table included for laser processing a broad array of materials</li>\r\n	<li>Industrial CNC machined replacement parts in stock for long term service</li>\r\n	<li>Ball bearing gantry promotes smooth, precision moving laser head</li>\r\n	<li>Optional Chuck or Roller rotary attachment for cylindrical objects</li>\r\n	<li>Works with Windows 10, 8, 7 (64 or 32 bit), XP – NO MAC</li>\r\n	<li>USA tech support available to serve you Monday-Saturday</li>\r\n	<li><strong>FREE Lifetime Tech Support</strong>. We are a tech support driven company</li>\r\n	<li>Pick-up available at our Orlando/Sanford, FL facility</li>\r\n	<li><strong>1 year Warranty</strong>covers CO2 Laser machine and laser tube.</li>\r\n	<li>Available <strong>30 Day&nbsp;</strong>Return Policy</li>\r\n	<li>LTL shipping with lift gate for off loading (not included in initial cost)</li>\r\n	<li><strong>Have questions? Please call us at 407.878.0880 for help!</strong></li>\r\n</ul>', 1, 14, 200, 1, 242, '', '', '', 1, 24, 36, 38, 10, NULL, 20, 0, 1, '2018-07-21 18:39:29', '[]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_accessories`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_accessories`;
CREATE TABLE `jiri_jkshop_products_accessories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `accessory_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_accessories`
--

TRUNCATE TABLE `jiri_jkshop_products_accessories`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_carriers_no`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_carriers_no`;
CREATE TABLE `jiri_jkshop_products_carriers_no` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `carrier_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_carriers_no`
--

TRUNCATE TABLE `jiri_jkshop_products_carriers_no`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_categories`;
CREATE TABLE `jiri_jkshop_products_categories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_categories`
--

TRUNCATE TABLE `jiri_jkshop_products_categories`;
--
-- Dumping data for table `jiri_jkshop_products_categories`
--

INSERT INTO `jiri_jkshop_products_categories` (`product_id`, `category_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_featured`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_featured`;
CREATE TABLE `jiri_jkshop_products_featured` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `featured_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_featured`
--

TRUNCATE TABLE `jiri_jkshop_products_featured`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_options`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_options`;
CREATE TABLE `jiri_jkshop_products_options` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price_difference_with_tax` double DEFAULT NULL,
  `extended_quantity` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_options`
--

TRUNCATE TABLE `jiri_jkshop_products_options`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_properties`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_properties`;
CREATE TABLE `jiri_jkshop_products_properties` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_properties`
--

TRUNCATE TABLE `jiri_jkshop_products_properties`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_users_price`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_users_price`;
CREATE TABLE `jiri_jkshop_products_users_price` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `price` double UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_products_users_price`
--

TRUNCATE TABLE `jiri_jkshop_products_users_price`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_properties`
--

DROP TABLE IF EXISTS `jiri_jkshop_properties`;
CREATE TABLE `jiri_jkshop_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_properties`
--

TRUNCATE TABLE `jiri_jkshop_properties`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_property_options`
--

DROP TABLE IF EXISTS `jiri_jkshop_property_options`;
CREATE TABLE `jiri_jkshop_property_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_index` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_property_options`
--

TRUNCATE TABLE `jiri_jkshop_property_options`;
-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_taxes`
--

DROP TABLE IF EXISTS `jiri_jkshop_taxes`;
CREATE TABLE `jiri_jkshop_taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `percent` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jiri_jkshop_taxes`
--

TRUNCATE TABLE `jiri_jkshop_taxes`;
--
-- Dumping data for table `jiri_jkshop_taxes`
--

INSERT INTO `jiri_jkshop_taxes` (`id`, `created_at`, `updated_at`, `name`, `active`, `percent`) VALUES
(1, NULL, NULL, 'Default DPH', 1, 21),
(2, NULL, NULL, 'Reduced DPH', 1, 15);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `jobs`
--

TRUNCATE TABLE `jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_action`
--

DROP TABLE IF EXISTS `marcelhaupt_email_action`;
CREATE TABLE `marcelhaupt_email_action` (
  `id` int(10) UNSIGNED NOT NULL,
  `redirect_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_action`
--

TRUNCATE TABLE `marcelhaupt_email_action`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_action_log`
--

DROP TABLE IF EXISTS `marcelhaupt_email_action_log`;
CREATE TABLE `marcelhaupt_email_action_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_triggered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_action_log`
--

TRUNCATE TABLE `marcelhaupt_email_action_log`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_campaign`
--

DROP TABLE IF EXISTS `marcelhaupt_email_campaign`;
CREATE TABLE `marcelhaupt_email_campaign` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `emails_sent` int(11) NOT NULL DEFAULT '0',
  `emails_opened` int(11) NOT NULL DEFAULT '0',
  `emails_clicked` int(11) NOT NULL DEFAULT '0',
  `incl_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `excl_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'any',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_unsubscribed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_campaign`
--

TRUNCATE TABLE `marcelhaupt_email_campaign`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_condition`
--

DROP TABLE IF EXISTS `marcelhaupt_email_condition`;
CREATE TABLE `marcelhaupt_email_condition` (
  `id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `property` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_condition`
--

TRUNCATE TABLE `marcelhaupt_email_condition`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_send_log`
--

DROP TABLE IF EXISTS `marcelhaupt_email_send_log`;
CREATE TABLE `marcelhaupt_email_send_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `pixel_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_opened` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_send_log`
--

TRUNCATE TABLE `marcelhaupt_email_send_log`;
-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_user_settings`
--

DROP TABLE IF EXISTS `marcelhaupt_email_user_settings`;
CREATE TABLE `marcelhaupt_email_user_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT '1',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `marcelhaupt_email_user_settings`
--

TRUNCATE TABLE `marcelhaupt_email_user_settings`;
-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2013_10_01_000001_Db_Backend_Users', 2),
(26, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(27, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(28, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(29, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(30, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(31, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(32, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(33, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(34, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(35, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(36, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(37, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(38, '2018_05_14_192455_create_products_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attrib_set_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `visibility` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `websites` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `video` text COLLATE utf8mb4_unicode_ci,
  `weight` double DEFAULT NULL,
  `set_product_as_new_from_date1` date DEFAULT NULL,
  `set_product_as_new_from_date2` date DEFAULT NULL,
  `show_materials_tab` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_of_manufacture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_cost` double DEFAULT NULL,
  `bottom_seo_block` text COLLATE utf8mb4_unicode_ci,
  `display_fume_extractor_tab` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `call_for_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `products`
--

TRUNCATE TABLE `products`;
-- --------------------------------------------------------

--
-- Table structure for table `rainlab_sitemap_definitions`
--

DROP TABLE IF EXISTS `rainlab_sitemap_definitions`;
CREATE TABLE `rainlab_sitemap_definitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `rainlab_sitemap_definitions`
--

TRUNCATE TABLE `rainlab_sitemap_definitions`;
-- --------------------------------------------------------

--
-- Table structure for table `rainlab_user_mail_blockers`
--

DROP TABLE IF EXISTS `rainlab_user_mail_blockers`;
CREATE TABLE `rainlab_user_mail_blockers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `rainlab_user_mail_blockers`
--

TRUNCATE TABLE `rainlab_user_mail_blockers`;
-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_fields`
--

DROP TABLE IF EXISTS `renatio_formbuilder_fields`;
CREATE TABLE `renatio_formbuilder_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `field_type_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation_messages` text COLLATE utf8mb4_unicode_ci,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_attributes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_formbuilder_fields`
--

TRUNCATE TABLE `renatio_formbuilder_fields`;
--
-- Dumping data for table `renatio_formbuilder_fields`
--

INSERT INTO `renatio_formbuilder_fields` (`id`, `form_id`, `section_id`, `field_type_id`, `label`, `name`, `default`, `validation`, `validation_messages`, `comment`, `class`, `wrapper_class`, `placeholder`, `custom_attributes`, `options`, `is_visible`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 1, 'Name', 'name', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 2, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(2, 1, NULL, 1, 'Subject', 'subject', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 3, 4, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(3, 1, NULL, 1, 'E-mail', 'email', NULL, 'required|email', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 6, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(4, 1, NULL, 1, 'Phone', 'phone', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 7, 8, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(5, 1, NULL, 2, 'Message', 'content_message', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, 10, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(6, 1, NULL, 7, 'reCaptcha', 'g-recaptcha-response', NULL, 'required|recaptcha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 11, 12, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(7, 1, NULL, 8, 'Send', 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 13, 14, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(8, 2, NULL, 1, 'Text', 'text', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 15, 16, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(9, 2, NULL, 3, 'Dropdown', 'dropdown', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 17, 18, 0, '2018-04-06 19:01:51', '2018-04-06 19:02:05'),
(10, 2, NULL, 4, 'Checkbox', 'checkbox', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 19, 20, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(11, 2, NULL, 5, 'Checkbox list', 'checkbox_list', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 21, 22, 0, '2018-04-06 19:01:51', '2018-04-06 19:02:05'),
(12, 2, NULL, 6, 'Radio list', 'radio_list', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 23, 24, 0, '2018-04-06 19:01:51', '2018-04-06 19:02:05'),
(13, 2, NULL, 10, 'Country select', 'country', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, NULL, 1, NULL, 25, 26, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(14, 2, NULL, 11, 'State select', 'state', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, NULL, 1, NULL, 27, 28, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(15, 2, NULL, 2, 'Textarea', 'textarea', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 29, 30, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(16, 2, NULL, 9, 'File uploader', 'files', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 31, 32, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(17, 2, NULL, 7, 'reCaptcha', 'g-recaptcha-response', NULL, 'required|recaptcha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 33, 34, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(18, 2, NULL, 8, 'Send', 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 35, 36, 0, '2018-04-06 19:01:51', '2018-04-06 19:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_field_types`
--

DROP TABLE IF EXISTS `renatio_formbuilder_field_types`;
CREATE TABLE `renatio_formbuilder_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `markup` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_formbuilder_field_types`
--

TRUNCATE TABLE `renatio_formbuilder_field_types`;
--
-- Dumping data for table `renatio_formbuilder_field_types`
--

INSERT INTO `renatio_formbuilder_field_types` (`id`, `name`, `description`, `code`, `markup`, `created_at`, `updated_at`) VALUES
(1, 'Text', 'Renders a single line text box.', 'text', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <input type=\"text\"\n           name=\"{{ name }}\"\n           value=\"{{ default }}\"\n           class=\"{{ class }} form-control\"\n           placeholder=\"{{ placeholder }}\"\n           {{ custom_attributes|raw }}\n    >\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:49', '2018-04-06 19:01:49'),
(2, 'Textarea', 'Renders a multiline text box.', 'textarea', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <textarea name=\"{{ name }}\"\n              class=\"{{ class }} form-control\"\n              placeholder=\"{{ placeholder }}\"\n              {{ custom_attributes|raw }}\n    >{{ default }}</textarea>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:49', '2018-04-06 19:01:49'),
(3, 'Dropdown', 'Renders a dropdown with specified options.', 'dropdown', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <select class=\"{{ class }} form-control\" name=\"{{ name }}\" {{ custom_attributes|raw }}>\n\n        {% if placeholder %}\n\n            <option value=\"\">{{ placeholder }}</option>\n\n        {% endif %}\n\n        {% for option in options %}\n\n            <option value=\"{{ option.o_key }}\" {{ option.o_key == default ? \'selected\' : \'\' }}>{{ option.o_label }}</option>\n\n        {% endfor %}\n\n    </select>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:49', '2018-04-06 19:02:05'),
(4, 'Checkbox', 'Renders a single checkbox.', 'checkbox', '<div class=\"{{ wrapper_class }} checkbox\">\n\n    <label>\n        <input {{ default ? \'checked\' : \'\' }}\n            name=\"{{ name }}\"\n            class=\"{{ class }}\"\n            value=\"1\"\n            type=\"checkbox\"\n            {{ custom_attributes|raw }}\n        >\n        {{ label }}\n    </label>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:49', '2018-04-06 19:01:49'),
(5, 'Checkbox List', 'Renders a list of checkboxes.', 'checkbox_list', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label class=\"btn-block\" for=\"{{ name }}\">{{ label }}</label>\n\n    {% set values = default|split(\'|\') %}\n    {% for option in options %}\n\n        <label class=\"checkbox-inline\">\n            <input {{ values[loop.index0] == option.o_key ? \'checked\' : \'\' }}\n                type=\"checkbox\"\n                name=\"{{ name }}[]\"\n                class=\"{{ class }}\"\n                id=\"{{ option.o_label }}\"\n                value=\"{{ option.o_key }}\"\n                {{ custom_attributes|raw }}\n            >\n            {{ option.o_label }}\n        </label>\n\n    {% endfor %}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:50', '2018-04-06 19:02:05'),
(6, 'Radio List', 'Renders a list of radio options, where only one item can be selected at a time.', 'radio_list', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label class=\"btn-block\" for=\"{{ name }}\">{{ label }}</label>\n\n    {% for option in options %}\n\n        <label class=\"radio-inline\">\n            <input {{ default == option.o_key ? \'checked\' : \'\' }}\n                type=\"radio\"\n                name=\"{{ name }}\"\n                class=\"{{ class }}\"\n                id=\"{{ option.o_label }}\"\n                value=\"{{ option.o_key }}\"\n                {{ custom_attributes|raw }}\n            >\n            {{ option.o_label }}\n        </label>\n\n    {% endfor %}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:50', '2018-04-06 19:02:05'),
(7, 'ReCaptcha', 'Renders a reCaptcha box.', 'recaptcha', '<div class=\"{{ wrapper_class }} form-group\">\n    <div class=\"g-recaptcha\" data-sitekey=\"{{ settings.site_key }}\" data-theme=\"{{ settings.theme }}\"></div>\n    <script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api.js?hl={{ settings.lang }}\"></script>\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n</div>', '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(8, 'Submit', 'Renders a submit button.', 'submit', '<span class=\"{{ wrapper_class }}\">\n    <button type=\"submit\" class=\"{{ class }} btn btn-primary\" {{ custom_attributes|raw }}>{{ label }}</button>\n</span>', '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(9, 'File uploader', 'Renders a file uploader for regular files.', 'file_uploader', '<div class=\"control-multi-file-uploader form-group\"\n     data-handler=\"{{ __SELF__ ~ \'::onUpdateFile\' }}\"\n     data-control=\"multi-file-uploader\"\n     data-max-size=\"{{ fileConfig.maxSize }}\"\n     data-file-types=\"{{ fileConfig.fileTypes|join(\', \') }}\">\n\n    <div class=\"clickable\"></div>\n    <div class=\"content\">\n        <p class=\"placeholder\">{{ fileConfig.placeholder }}</p>\n    </div>\n\n    <div class=\"template\" style=\"display: none\">\n        <div class=\"dz-preview dz-file-preview\">\n            <img class=\"thumbnail\" src=\"{{ \'plugins/renatio/formbuilder/assets/images/upload.png\'|app }}\"\n                 data-dz-thumbnail/>\n\n            <div class=\"dz-details\">\n                <div class=\"dz-filename\"><span data-dz-name></span></div>\n                <div class=\"dz-size\" data-dz-size></div>\n                <div class=\"activity\">\n                    <div class=\"loading\"></div>\n                </div>\n            </div>\n            <div class=\"action-panel\">\n                <a href=\"javascript:;\" class=\"delete\">&times;</a>\n            </div>\n            <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n\n            <div class=\"dz-error-message\">\n                <div class=\"dz-error-mark\">&cross;</div>\n                <span data-dz-errormessage></span>\n            </div>\n        </div>\n    </div>\n\n</div>', '2018-04-06 19:01:51', '2018-04-06 19:01:51'),
(10, 'Country select', 'Renders a dropdown with country options.', 'country_select', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    {{ form_select_country(name, null,\n    {\n        class: class ~ \' form-control\',\n        emptyOption: placeholder,\n        \'data-request\': \'onChangeCountry\',\n        \'data-request-success\': \'updateStateDropdown(data)\'\n    }) }}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:55', '2018-04-06 19:01:55'),
(11, 'State select', 'Renders a dropdown with state options.', 'state_select', '<div id=\"country-state\" class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    {{ form_select_state(name, null, null,\n    {\n        class: class ~ \' form-control\',\n        emptyOption: placeholder\n    }) }}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-06 19:01:55', '2018-04-06 19:01:55'),
(12, 'Section', 'Renders a section with assigned fields.', 'section', '{{ wrapper_begin|raw }}\n\n    <fieldset class=\"{{ class }}\" id=\"{{ name }}\">\n        {% if label %}<legend>{{ label }}</legend>{% endif %}\n        {{ fields|raw }}\n    </fieldset>\n\n{{ wrapper_end|raw }}', '2018-04-06 19:02:00', '2018-04-06 19:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_forms`
--

DROP TABLE IF EXISTS `renatio_formbuilder_forms`;
CREATE TABLE `renatio_formbuilder_forms` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `from_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8mb4_unicode_ci,
  `error_message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_email_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_formbuilder_forms`
--

TRUNCATE TABLE `renatio_formbuilder_forms`;
--
-- Dumping data for table `renatio_formbuilder_forms`
--

INSERT INTO `renatio_formbuilder_forms` (`id`, `template_id`, `name`, `code`, `description`, `from_email`, `from_name`, `to_email`, `to_name`, `bcc_email`, `bcc_name`, `success_message`, `error_message`, `created_at`, `updated_at`, `css_class`, `reply_email`, `reply_name`, `response_email_field`) VALUES
(1, 1, 'Contact Form', 'renatio.formbuilder::contact', 'Renders contact form.', NULL, NULL, 'admin@bosslaser.com', 'Admin mohamed', NULL, NULL, 'Message was sent successfully!', 'There was an error. Please try again!', '2018-04-06 19:01:50', '2018-04-06 19:01:50', NULL, NULL, NULL, NULL),
(2, 2, 'Default Form', 'renatio.formbuilder::default', 'Renders default form with all available system fields.', NULL, NULL, 'admin@bosslaser.com', 'Admin mohamed', NULL, NULL, 'Message was sent successfully!', 'There was an error. Please try again!', '2018-04-06 19:01:51', '2018-04-06 19:01:51', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_form_logs`
--

DROP TABLE IF EXISTS `renatio_formbuilder_form_logs`;
CREATE TABLE `renatio_formbuilder_form_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `form_data` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_formbuilder_form_logs`
--

TRUNCATE TABLE `renatio_formbuilder_form_logs`;
-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_sections`
--

DROP TABLE IF EXISTS `renatio_formbuilder_sections`;
CREATE TABLE `renatio_formbuilder_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `sort_order` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_begin` text COLLATE utf8mb4_unicode_ci,
  `wrapper_end` text COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_formbuilder_sections`
--

TRUNCATE TABLE `renatio_formbuilder_sections`;
-- --------------------------------------------------------

--
-- Table structure for table `renatio_seomanager_seo_tags`
--

DROP TABLE IF EXISTS `renatio_seomanager_seo_tags`;
CREATE TABLE `renatio_seomanager_seo_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `seo_tag_id` int(10) UNSIGNED DEFAULT NULL,
  `seo_tag_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robot_index` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'index',
  `robot_follow` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'follow',
  `robot_advanced` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `renatio_seomanager_seo_tags`
--

TRUNCATE TABLE `renatio_seomanager_seo_tags`;
-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;
-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

DROP TABLE IF EXISTS `system_event_logs`;
CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_event_logs`
--

TRUNCATE TABLE `system_event_logs`;
--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'RainLab\\User\\Models\\User\' not found in C:\\wamp64\\www\\bosslaser\\plugins\\marcelhaupt\\email\\Plugin.php:12\nStack trace:\n#0 C:\\wamp64\\www\\bosslaser\\modules\\system\\classes\\PluginManager.php(279): MarcelHaupt\\Email\\Plugin->boot()\n#1 C:\\wamp64\\www\\bosslaser\\modules\\system\\classes\\PluginManager.php(261): System\\Classes\\PluginManager->bootPlugin(Object(MarcelHaupt\\Email\\Plugin))\n#2 C:\\wamp64\\www\\bosslaser\\modules\\system\\ServiceProvider.php(96): System\\Classes\\PluginManager->bootAll()\n#3 [internal function]: System\\ServiceProvider->boot()\n#4 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#5 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#6 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#7 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#8 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Application.php(792): Illuminate\\Container\\Container->call(Array)\n#9 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Application.php(775): Illuminate\\Foundation\\Application->bootProvider(Object(System\\ServiceProvider))\n#10 [internal function]: Illuminate\\Foundation\\Application->Illuminate\\Foundation\\{closure}(Object(System\\ServiceProvider), 22)\n#11 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Application.php(776): array_walk(Array, Object(Closure))\n#12 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Bootstrap\\BootProviders.php(17): Illuminate\\Foundation\\Application->boot()\n#13 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Application.php(213): Illuminate\\Foundation\\Bootstrap\\BootProviders->bootstrap(Object(October\\Rain\\Foundation\\Application))\n#14 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(162): Illuminate\\Foundation\\Application->bootstrapWith(Array)\n#15 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(146): Illuminate\\Foundation\\Http\\Kernel->bootstrap()\n#16 C:\\wamp64\\www\\bosslaser\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#17 C:\\wamp64\\www\\bosslaser\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#18 {main}', NULL, '2018-04-06 19:11:26', '2018-04-06 19:11:26'),
(2, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'Backend\\Models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CodeParser.php(293) : eval()\'d code:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b43770477a2d385561555_58a37d4d5428c8d23d7fd6dc814e6430Class->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 14:53:56', '2018-07-09 14:53:56'),
(3, 'error', 'ErrorException: file_put_contents(/y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/framework/classes.php): failed to open stream: Permission denied in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:122\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'file_put_conten...\', \'/y/apache2/htdo...\', 122, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(122): file_put_contents(\'/y/apache2/htdo...\', \'<?php return ar...\', 0)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Filesystem/Filesystem.php(220): Illuminate\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\', false)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(287): October\\Rain\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(164): October\\Rain\\Support\\ClassLoader->write(Array)\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Bootstrap/RegisterClassLoader.php(33): October\\Rain\\Support\\ClassLoader->build()\n#6 [internal function]: October\\Rain\\Foundation\\Bootstrap\\RegisterClassLoader->October\\Rain\\Foundation\\Bootstrap\\{closure}(Object(Illuminate\\Http\\Request), Object(Illuminate\\Http\\Response))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(233): call_user_func_array(Object(Closure), Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(197): October\\Rain\\Events\\Dispatcher->dispatch(\'router.after\', Array, false)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(22): October\\Rain\\Events\\Dispatcher->fire(\'router.after\', Array)\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#22 {main}', NULL, '2018-07-09 14:53:57', '2018-07-09 14:53:57'),
(4, 'error', 'ErrorException: file_put_contents(/y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/framework/classes.php): failed to open stream: Permission denied in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:122\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'file_put_conten...\', \'/y/apache2/htdo...\', 122, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(122): file_put_contents(\'/y/apache2/htdo...\', \'<?php return ar...\', 0)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Filesystem/Filesystem.php(220): Illuminate\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\', false)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(287): October\\Rain\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(164): October\\Rain\\Support\\ClassLoader->write(Array)\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Bootstrap/RegisterClassLoader.php(33): October\\Rain\\Support\\ClassLoader->build()\n#6 [internal function]: October\\Rain\\Foundation\\Bootstrap\\RegisterClassLoader->October\\Rain\\Foundation\\Bootstrap\\{closure}(Object(Illuminate\\Http\\Request), Object(Illuminate\\Http\\Response))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(233): call_user_func_array(Object(Closure), Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(197): October\\Rain\\Events\\Dispatcher->dispatch(\'router.after\', Array, false)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(22): October\\Rain\\Events\\Dispatcher->fire(\'router.after\', Array)\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#22 {main}', NULL, '2018-07-09 14:53:59', '2018-07-09 14:53:59'),
(5, 'error', 'ErrorException: file_put_contents(/y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/framework/classes.php): failed to open stream: Permission denied in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:122\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'file_put_conten...\', \'/y/apache2/htdo...\', 122, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(122): file_put_contents(\'/y/apache2/htdo...\', \'<?php return ar...\', 0)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Filesystem/Filesystem.php(220): Illuminate\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\', false)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(287): October\\Rain\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(164): October\\Rain\\Support\\ClassLoader->write(Array)\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Bootstrap/RegisterClassLoader.php(33): October\\Rain\\Support\\ClassLoader->build()\n#6 [internal function]: October\\Rain\\Foundation\\Bootstrap\\RegisterClassLoader->October\\Rain\\Foundation\\Bootstrap\\{closure}(Object(Illuminate\\Http\\Request), Object(Illuminate\\Http\\Response))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(233): call_user_func_array(Object(Closure), Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(197): October\\Rain\\Events\\Dispatcher->dispatch(\'router.after\', Array, false)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(22): October\\Rain\\Events\\Dispatcher->fire(\'router.after\', Array)\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#22 {main}', NULL, '2018-07-09 14:53:59', '2018-07-09 14:53:59'),
(6, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'Backend\\Models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b43770477a2d385561555_58a37d4d5428c8d23d7fd6dc814e6430Class->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:01:37', '2018-07-09 15:01:37'),
(7, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'Backend\\Models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b43770477a2d385561555_58a37d4d5428c8d23d7fd6dc814e6430Class->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:13:11', '2018-07-09 15:13:11');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(8, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CodeParser.php(293) : eval()\'d code:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b437bf7ba007149200487_1d0450088510bc5e492207395cc232fcClass->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:15:03', '2018-07-09 15:15:03'),
(9, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b437bf7ba007149200487_1d0450088510bc5e492207395cc232fcClass->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:17:20', '2018-07-09 15:17:20'),
(10, 'error', 'ErrorException: Declaration of RainLab\\User\\Classes\\AuthManager::register(array $credentials, $activate = false) should be compatible with October\\Rain\\Auth\\Manager::register(array $credentials, $activate = false, $autoLogin = true) in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/classes/AuthManager.php:7\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/classes/AuthManager.php(7): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'Declaration of ...\', \'/y/apache2/htdo...\', 7, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(130): require_once(\'/y/apache2/htdo...\')\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(99): October\\Rain\\Support\\ClassLoader->includeClass(\'RainLab\\\\User\\\\Cl...\', \'plugins/rainlab...\')\n#3 [internal function]: October\\Rain\\Support\\ClassLoader->load(\'RainLab\\\\User\\\\Cl...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/Plugin.php(37): spl_autoload_call(\'RainLab\\\\User\\\\Cl...\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(749): RainLab\\User\\Plugin->RainLab\\User\\{closure}(Object(October\\Rain\\Foundation\\Application), Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(631): Illuminate\\Container\\Container->build(Object(Closure))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(586): Illuminate\\Container\\Container->resolve(\'user.auth\', Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Application.php(732): Illuminate\\Container\\Container->make(\'user.auth\', Array)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Application.php(162): Illuminate\\Foundation\\Application->make(\'user.auth\')\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(1195): October\\Rain\\Foundation\\Application->make(\'user.auth\')\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(159): Illuminate\\Container\\Container->offsetGet(\'user.auth\')\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/Facade.php(28): Illuminate\\Support\\Facades\\Facade::resolveFacadeInstance(\'user.auth\')\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(128): October\\Rain\\Support\\Facade::resolveFacadeInstance(\'user.auth\')\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(215): Illuminate\\Support\\Facades\\Facade::getFacadeRoot()\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/jiri/jkshop/models/Product.php(311): Illuminate\\Support\\Facades\\Facade::__callStatic(\'getUser\', Array)\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Extension/Core.php(1605): Jiri\\JKShop\\Models\\Product->getFinalPrice()\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/twig/54/54bf2e48ebfc0af13cf080783653b201f1c8f2503ec878ff6fe23c2705602ce9.php(82): twig_get_attribute(Object(Twig_Environment), Object(Twig_Source), Object(Jiri\\JKShop\\Models\\Product), \'getFinalPrice\', Array)\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(390): __TwigTemplate_69d8b875d9456d0f838cd8d8e728851429cb1772cf3689e3854fc9b3a4a0b25a->doDisplay(Array, Array)\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(375): Twig_Template->display(Array)\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(365): Twig_Template->render(Array)\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'products1\')\n#24 [internal function]: Cms\\Classes\\CmsController->run(\'products1\')\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#48 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#49 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#51 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#52 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template (\"Declaration of RainLab\\User\\Classes\\AuthManager::register(array $credentials, $activate = false) should be compatible with October\\Rain\\Auth\\Manager::register(array $credentials, $activate = false, $autoLogin = true)\") in \"/y/apache2/htdocs/bosslaser/htdocs/bosslaser/themes/bosslaser/pages/products.htm\" at line 20. in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php:405\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(375): Twig_Template->display(Array)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(365): Twig_Template->render(Array)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'products1\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'products1\')\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}', NULL, '2018-07-09 15:17:51', '2018-07-09 15:17:51');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(11, 'error', 'ErrorException: Declaration of RainLab\\User\\Classes\\AuthManager::register(array $credentials, $activate = false) should be compatible with October\\Rain\\Auth\\Manager::register(array $credentials, $activate = false, $autoLogin = true) in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/classes/AuthManager.php:7\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/classes/AuthManager.php(7): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'Declaration of ...\', \'/y/apache2/htdo...\', 7, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(130): require_once(\'/y/apache2/htdo...\')\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(99): October\\Rain\\Support\\ClassLoader->includeClass(\'RainLab\\\\User\\\\Cl...\', \'plugins/rainlab...\')\n#3 [internal function]: October\\Rain\\Support\\ClassLoader->load(\'RainLab\\\\User\\\\Cl...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/rainlab/user/Plugin.php(37): spl_autoload_call(\'RainLab\\\\User\\\\Cl...\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(749): RainLab\\User\\Plugin->RainLab\\User\\{closure}(Object(October\\Rain\\Foundation\\Application), Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(631): Illuminate\\Container\\Container->build(Object(Closure))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(586): Illuminate\\Container\\Container->resolve(\'user.auth\', Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Application.php(732): Illuminate\\Container\\Container->make(\'user.auth\', Array)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Application.php(162): Illuminate\\Foundation\\Application->make(\'user.auth\')\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Container/Container.php(1195): October\\Rain\\Foundation\\Application->make(\'user.auth\')\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(159): Illuminate\\Container\\Container->offsetGet(\'user.auth\')\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/Facade.php(28): Illuminate\\Support\\Facades\\Facade::resolveFacadeInstance(\'user.auth\')\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(128): October\\Rain\\Support\\Facade::resolveFacadeInstance(\'user.auth\')\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Support/Facades/Facade.php(215): Illuminate\\Support\\Facades\\Facade::getFacadeRoot()\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/plugins/jiri/jkshop/models/Product.php(311): Illuminate\\Support\\Facades\\Facade::__callStatic(\'getUser\', Array)\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Extension/Core.php(1605): Jiri\\JKShop\\Models\\Product->getFinalPrice()\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/twig/54/54bf2e48ebfc0af13cf080783653b201f1c8f2503ec878ff6fe23c2705602ce9.php(82): twig_get_attribute(Object(Twig_Environment), Object(Twig_Source), Object(Jiri\\JKShop\\Models\\Product), \'getFinalPrice\', Array)\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(390): __TwigTemplate_69d8b875d9456d0f838cd8d8e728851429cb1772cf3689e3854fc9b3a4a0b25a->doDisplay(Array, Array)\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(375): Twig_Template->display(Array)\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(365): Twig_Template->render(Array)\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'products1\')\n#24 [internal function]: Cms\\Classes\\CmsController->run(\'products1\')\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#48 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#49 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#51 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#52 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#53 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template (\"Declaration of RainLab\\User\\Classes\\AuthManager::register(array $credentials, $activate = false) should be compatible with October\\Rain\\Auth\\Manager::register(array $credentials, $activate = false, $autoLogin = true)\") in \"/y/apache2/htdocs/bosslaser/htdocs/bosslaser/themes/bosslaser/pages/products.htm\" at line 20. in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php:405\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(367): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/twig/twig/lib/Twig/Template.php(375): Twig_Template->display(Array)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(365): Twig_Template->render(Array)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'products1\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'products1\')\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 {main}', NULL, '2018-07-09 15:19:03', '2018-07-09 15:19:03'),
(12, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b437bf7ba007149200487_1d0450088510bc5e492207395cc232fcClass->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:19:30', '2018-07-09 15:19:30'),
(13, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b437bf7ba007149200487_1d0450088510bc5e492207395cc232fcClass->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:31:13', '2018-07-09 15:31:13'),
(14, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b437bf7ba007149200487_1d0450088510bc5e492207395cc232fcClass->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:33:01', '2018-07-09 15:33:01');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(15, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CodeParser.php(293) : eval()\'d code:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b438055039e0523710424_483c7ef84a3dc99ac568ed06386e6345Class->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:33:41', '2018-07-09 15:33:41'),
(16, 'error', 'Symfony\\Component\\Debug\\Exception\\FatalThrowableError: Class \'backend\\models\\Product\' not found in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/cms/cache/39/8b/filter.htm.php:6\nStack trace:\n#0 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(426): Cms5b438055039e0523710424_483c7ef84a3dc99ac568ed06386e6345Class->onStart()\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(348): Cms\\Classes\\Controller->execPageCycle()\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/Controller.php(211): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/modules/cms/classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#6 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#19 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#22 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#25 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#30 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#31 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#32 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/barryvdh/laravel-debugbar/src/Middleware/InjectDebugbar.php(65): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Barryvdh\\Debugbar\\Middleware\\InjectDebugbar->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#42 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#43 {main}', NULL, '2018-07-09 15:35:18', '2018-07-09 15:35:18'),
(17, 'error', 'ErrorException: file_put_contents(/y/apache2/htdocs/bosslaser/htdocs/bosslaser/storage/framework/classes.php): failed to open stream: Permission denied in /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php:122\nStack trace:\n#0 [internal function]: Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(2, \'file_put_conten...\', \'/y/apache2/htdo...\', 122, Array)\n#1 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Filesystem/Filesystem.php(122): file_put_contents(\'/y/apache2/htdo...\', \'<?php return ar...\', 0)\n#2 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Filesystem/Filesystem.php(220): Illuminate\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\', false)\n#3 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(287): October\\Rain\\Filesystem\\Filesystem->put(\'/y/apache2/htdo...\', \'<?php return ar...\')\n#4 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Support/ClassLoader.php(164): October\\Rain\\Support\\ClassLoader->write(Array)\n#5 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Foundation/Bootstrap/RegisterClassLoader.php(33): October\\Rain\\Support\\ClassLoader->build()\n#6 [internal function]: October\\Rain\\Foundation\\Bootstrap\\RegisterClassLoader->October\\Rain\\Foundation\\Bootstrap\\{closure}(Object(Illuminate\\Http\\Request), Object(Illuminate\\Http\\Response))\n#7 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(233): call_user_func_array(Object(Closure), Array)\n#8 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Events/Dispatcher.php(197): October\\Rain\\Events\\Dispatcher->dispatch(\'router.after\', Array, false)\n#9 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/october/rain/src/Router/CoreRouter.php(22): October\\Rain\\Events\\Dispatcher->fire(\'router.after\', Array)\n#10 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#11 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#14 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#17 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#18 /y/apache2/htdocs/bosslaser/htdocs/bosslaser/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#19 {main}', NULL, '2018-07-09 15:36:54', '2018-07-09 15:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

DROP TABLE IF EXISTS `system_files`;
CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_files`
--

TRUNCATE TABLE `system_files`;
--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, '5b352b838795a184074938.jpg', 'product1.jpg', 33852, 'image/jpeg', NULL, NULL, 'images', '1', 'Jiri\\JKShop\\Models\\Product', 1, 1, '2018-06-28 18:40:03', '2018-06-28 18:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

DROP TABLE IF EXISTS `system_mail_layouts`;
CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_mail_layouts`
--

TRUNCATE TABLE `system_mail_layouts`;
--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-04-06 12:39:20', '2018-04-06 12:39:20'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-04-06 12:39:20', '2018-04-06 12:39:20'),
(3, 'Form Builder Default Layout', 'form_builder', '<!doctype html>\n<html>\n<head>\n    <meta name=\"viewport\" content=\"width=device-width\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <title>Renatio FormBuilder</title>\n    <style>\n        {{ css|raw }}\n    </style>\n</head>\n<body class=\"\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">\n    <tr>\n        <td>&nbsp;</td>\n        <td class=\"container\">\n            <div class=\"content\">\n\n                {{ content|raw }}\n\n                <!-- START FOOTER -->\n                <div class=\"footer\">\n                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                        <tr>\n                            <td class=\"content-block powered-by\">\n                                Powered by <a href=\"https://renatio.com\">Renatio</a>.\n                            </td>\n                        </tr>\n                    </table>\n                </div>\n\n                <!-- END FOOTER -->\n\n                <!-- END CENTERED WHITE CONTAINER --></div>\n        </td>\n        <td>&nbsp;</td>\n    </tr>\n</table>\n</body>\n</html>', '{{ content }}', '/* -------------------------------------\n    GLOBAL RESETS\n------------------------------------- */\nimg {\n    border: none;\n    -ms-interpolation-mode: bicubic;\n    max-width: 100%;\n}\n\nbody {\n    background-color: #f6f6f6;\n    font-family: sans-serif;\n    -webkit-font-smoothing: antialiased;\n    font-size: 14px;\n    line-height: 1.4;\n    margin: 0;\n    padding: 0;\n    -ms-text-size-adjust: 100%;\n    -webkit-text-size-adjust: 100%;\n}\n\ntable {\n    border-collapse: separate;\n    mso-table-lspace: 0pt;\n    mso-table-rspace: 0pt;\n    width: 100%;\n}\n\ntable td {\n    font-family: sans-serif;\n    font-size: 14px;\n    vertical-align: top;\n}\n\n/* -------------------------------------\n    BODY & CONTAINER\n------------------------------------- */\n\n.body {\n    background-color: #f6f6f6;\n    width: 100%;\n}\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n    display: block;\n    Margin: 0 auto !important;\n    /* makes it centered */\n    max-width: 580px;\n    padding: 10px;\n    width: auto !important;\n    width: 580px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n    box-sizing: border-box;\n    display: block;\n    Margin: 0 auto;\n    max-width: 580px;\n    padding: 10px;\n}\n\n/* -------------------------------------\n    HEADER, FOOTER, MAIN\n------------------------------------- */\n.main {\n    background: #fff;\n    border-radius: 3px;\n    width: 100%;\n}\n\n.wrapper {\n    box-sizing: border-box;\n    padding: 20px;\n}\n\n.contact {\n    border-collapse: collapse;\n    padding-bottom: 100px;\n}\n\n.contact td {\n    padding: 0.25rem;\n    text-align: left;\n}\n\n.contact tr {\n    border-bottom: 1px solid #eee;\n}\n\n.footer {\n    clear: both;\n    padding-top: 10px;\n    text-align: center;\n    width: 100%;\n}\n\n.footer td,\n.footer p,\n.footer span,\n.footer a {\n    color: #999999;\n    font-size: 12px;\n    text-align: center;\n}\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1,\nh2,\nh3,\nh4 {\n    color: #000000;\n    font-family: sans-serif;\n    font-weight: 400;\n    line-height: 1.4;\n    margin: 0;\n    Margin-bottom: 30px;\n}\n\nh1 {\n    font-size: 35px;\n    font-weight: 300;\n    text-align: center;\n    text-transform: capitalize;\n}\n\np,\nul,\nol {\n    font-family: sans-serif;\n    font-size: 14px;\n    font-weight: normal;\n    margin: 0;\n    Margin-bottom: 15px;\n}\n\np li,\nul li,\nol li {\n    list-style-position: inside;\n    margin-left: 5px;\n}\n\na {\n    color: #3498db;\n    text-decoration: underline;\n}\n\n/* -------------------------------------\n    BUTTONS\n------------------------------------- */\n.btn {\n    box-sizing: border-box;\n    width: 100%;\n}\n\n.btn > tbody > tr > td {\n    padding-bottom: 15px;\n}\n\n.btn table {\n    width: auto;\n}\n\n.btn table td {\n    background-color: #ffffff;\n    border-radius: 5px;\n    text-align: center;\n}\n\n.btn a {\n    background-color: #ffffff;\n    border: solid 1px #3498db;\n    border-radius: 5px;\n    box-sizing: border-box;\n    color: #3498db;\n    cursor: pointer;\n    display: inline-block;\n    font-size: 14px;\n    font-weight: bold;\n    margin: 0;\n    padding: 12px 25px;\n    text-decoration: none;\n    text-transform: capitalize;\n}\n\n.btn-primary table td {\n    background-color: #3498db;\n}\n\n.btn-primary a {\n    background-color: #3498db;\n    border-color: #3498db;\n    color: #ffffff;\n}\n\n/* -------------------------------------\n    OTHER STYLES THAT MIGHT BE USEFUL\n------------------------------------- */\n.last {\n    margin-bottom: 0;\n}\n\n.first {\n    margin-top: 0;\n}\n\n.align-center {\n    text-align: center;\n}\n\n.align-right {\n    text-align: right;\n}\n\n.align-left {\n    text-align: left;\n}\n\n.clear {\n    clear: both;\n}\n\n.mt0 {\n    margin-top: 0;\n}\n\n.mb0 {\n    margin-bottom: 0;\n}\n\n.preheader {\n    color: transparent;\n    display: none;\n    height: 0;\n    max-height: 0;\n    max-width: 0;\n    opacity: 0;\n    overflow: hidden;\n    mso-hide: all;\n    visibility: hidden;\n    width: 0;\n}\n\n.powered-by a {\n    text-decoration: none;\n}\n\nhr {\n    border: 0;\n    border-bottom: 1px solid #f6f6f6;\n    Margin: 20px 0;\n}\n\n/* -------------------------------------\n    RESPONSIVE AND MOBILE FRIENDLY STYLES\n------------------------------------- */\n@media only screen and (max-width: 620px) {\n    table[class=body] h1 {\n        font-size: 28px !important;\n        margin-bottom: 10px !important;\n    }\n\n    table[class=body] p,\n    table[class=body] ul,\n    table[class=body] ol,\n    table[class=body] td,\n    table[class=body] span,\n    table[class=body] a {\n        font-size: 16px !important;\n    }\n\n    table[class=body] .wrapper,\n    table[class=body] .article {\n        padding: 10px !important;\n    }\n\n    table[class=body] .content {\n        padding: 0 !important;\n    }\n\n    table[class=body] .container {\n        padding: 0 !important;\n        width: 100% !important;\n    }\n\n    table[class=body] .main {\n        border-left-width: 0 !important;\n        border-radius: 0 !important;\n        border-right-width: 0 !important;\n    }\n\n    table[class=body] .btn table {\n        width: 100% !important;\n    }\n\n    table[class=body] .btn a {\n        width: 100% !important;\n    }\n\n    table[class=body] .img-responsive {\n        height: auto !important;\n        max-width: 100% !important;\n        width: auto !important;\n    }\n}\n\n/* -------------------------------------\n    PRESERVE THESE STYLES IN THE HEAD\n------------------------------------- */\n@media all {\n    .ExternalClass {\n        width: 100%;\n    }\n\n    .ExternalClass,\n    .ExternalClass p,\n    .ExternalClass span,\n    .ExternalClass font,\n    .ExternalClass td,\n    .ExternalClass div {\n        line-height: 100%;\n    }\n\n    .apple-link a {\n        color: inherit !important;\n        font-family: inherit !important;\n        font-size: inherit !important;\n        font-weight: inherit !important;\n        line-height: inherit !important;\n        text-decoration: none !important;\n    }\n\n    .btn-primary table td:hover {\n        background-color: #34495e !important;\n    }\n\n    .btn-primary a:hover {\n        background-color: #34495e !important;\n        border-color: #34495e !important;\n    }\n}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(4, 'Ideas Shop Mail Layout', 'ideas_shop_mail_layout', '<html>\r\n            <head>\r\n                <style type=\"text/css\" media=\"screen\">\r\n                    {{ css|raw }}\r\n                </style>\r\n            </head>\r\n            <body>\r\n                {{ content|raw }}\r\n            </body>\r\n        </html>', '{{ content|raw }}', 'body {\r\n           background-color: #ffffff;\r\n        }\r\n        #mail-table {\r\n            font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\r\n            border-collapse: collapse;\r\n            width: 80%;\r\n        }\r\n        #mail-table td, #mail-table th {\r\n            border: 1px solid #ddd;\r\n            padding: 8px;\r\n        }\r\n        #mail-table tr:nth-child(even){background-color: #f2f2f2;}\r\n        #mail-table tr:hover {background-color: #ddd;}\r\n        #mail-table th {\r\n            padding-top: 12px;\r\n            padding-bottom: 12px;\r\n            text-align: left;\r\n            background-color: #4CAF50;\r\n            color: white;\r\n        }', 1, '2018-06-21 13:02:53', '2018-06-21 13:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

DROP TABLE IF EXISTS `system_mail_partials`;
CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_mail_partials`
--

TRUNCATE TABLE `system_mail_partials`;
--
-- Dumping data for table `system_mail_partials`
--

INSERT INTO `system_mail_partials` (`id`, `name`, `code`, `content_html`, `content_text`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(4, 'Panel', 'panel', '<table class=\"panel\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(7, 'Promotion', 'promotion', '<table class=\"promotion\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

DROP TABLE IF EXISTS `system_mail_templates`;
CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_mail_templates`
--

TRUNCATE TABLE `system_mail_templates`;
--
-- Dumping data for table `system_mail_templates`
--

INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'renatio.formbuilder::mail.contact', NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(2, 'renatio.formbuilder::mail.default', NULL, NULL, NULL, NULL, NULL, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(3, 'marcelhaupt.email::mail.sample', NULL, 'This is a sample newsletter', NULL, NULL, 1, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(4, 'backend::mail.invite', NULL, 'Invite new admin to the site', NULL, NULL, 2, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50'),
(5, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2018-04-06 19:01:50', '2018-04-06 19:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

DROP TABLE IF EXISTS `system_parameters`;
CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_parameters`
--

TRUNCATE TABLE `system_parameters`;
--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '3'),
(2, 'system', 'core', 'hash', '\"eb0dccd7698b8d533b4758b812ea10ad\"'),
(3, 'system', 'core', 'build', '437'),
(4, 'system', 'update', 'retry', '1531235940'),
(5, 'system', 'project', 'id', '\"1ZGx2BQtgAwx1BGxgMGp0AwHlAwSvMGDjMTHlA2AyZwVmZwH1MQuzZGEzZwR\"'),
(6, 'system', 'project', 'name', '\"Bosslaser\"'),
(7, 'system', 'project', 'owner', '\"Ahmed Ibrahim\"'),
(8, 'cms', 'theme', 'active', '\"bosslaser\"');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

DROP TABLE IF EXISTS `system_plugin_history`;
CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_plugin_history`
--

TRUNCATE TABLE `system_plugin_history`;
--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2018-04-06 12:39:19'),
(2, 'Alexis.Banners', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-06 19:01:04'),
(3, 'Alexis.Banners', 'script', '1.0.2', 'builder_table_create_alexis_banners_configs.php', '2018-04-06 19:01:05'),
(4, 'Alexis.Banners', 'comment', '1.0.2', 'Created table alexis_banners_configs', '2018-04-06 19:01:05'),
(5, 'Alexis.Banners', 'script', '1.0.3', 'builder_table_create_alexis_banners_banners.php', '2018-04-06 19:01:06'),
(6, 'Alexis.Banners', 'comment', '1.0.3', 'Created table alexis_banners_banners', '2018-04-06 19:01:06'),
(7, 'Alexis.Banners', 'script', '1.0.4', 'builder_table_update_alexis_banners_banners.php', '2018-04-06 19:01:08'),
(8, 'Alexis.Banners', 'comment', '1.0.4', 'Updated table alexis_banners_banners', '2018-04-06 19:01:08'),
(9, 'Alexis.Banners', 'script', '1.0.5', 'builder_table_update_alexis_banners_banners_2.php', '2018-04-06 19:01:10'),
(10, 'Alexis.Banners', 'comment', '1.0.5', 'Updated table alexis_banners_banners', '2018-04-06 19:01:10'),
(11, 'Alexis.Banners', 'script', '1.0.6', 'builder_table_update_alexis_banners_banners_3.php', '2018-04-06 19:01:10'),
(12, 'Alexis.Banners', 'comment', '1.0.6', 'Updated table alexis_banners_banners', '2018-04-06 19:01:10'),
(13, 'Alexis.Banners', 'script', '1.0.7', 'builder_table_update_alexis_banners_configs.php', '2018-04-06 19:01:11'),
(14, 'Alexis.Banners', 'comment', '1.0.7', 'Updated table alexis_banners_configs', '2018-04-06 19:01:11'),
(15, 'Alexis.Banners', 'script', '1.0.8', 'builder_table_update_alexis_banners_banners_4.php', '2018-04-06 19:01:12'),
(16, 'Alexis.Banners', 'comment', '1.0.8', 'Updated table alexis_banners_banners', '2018-04-06 19:01:13'),
(17, 'Alexis.Banners', 'script', '1.0.9', 'builder_table_update_alexis_banners_banners_5.php', '2018-04-06 19:01:13'),
(18, 'Alexis.Banners', 'comment', '1.0.9', 'Updated table alexis_banners_banners', '2018-04-06 19:01:13'),
(19, 'Alexis.Banners', 'script', '1.0.10', 'builder_table_update_alexis_banners_banners_6.php', '2018-04-06 19:01:14'),
(20, 'Alexis.Banners', 'comment', '1.0.10', 'Updated table alexis_banners_banners', '2018-04-06 19:01:14'),
(21, 'Alexis.Banners', 'script', '1.0.11', 'builder_table_create_alexis_banners_queues.php', '2018-04-06 19:01:14'),
(22, 'Alexis.Banners', 'comment', '1.0.11', 'Created table alexis_banners_queues', '2018-04-06 19:01:14'),
(23, 'MarcelHaupt.Email', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-06 19:01:14'),
(24, 'MarcelHaupt.Email', 'script', '1.0.2', 'builder_table_create_marcelhaupt_email_user_settings.php', '2018-04-06 19:01:15'),
(25, 'MarcelHaupt.Email', 'comment', '1.0.2', 'Created table marcelhaupt_email_user_settings', '2018-04-06 19:01:15'),
(26, 'MarcelHaupt.Email', 'script', '1.0.3', 'builder_table_create_marcelhaupt_email_campaigns.php', '2018-04-06 19:01:16'),
(27, 'MarcelHaupt.Email', 'comment', '1.0.3', 'Created table marcelhaupt_email_campaigns', '2018-04-06 19:01:16'),
(28, 'MarcelHaupt.Email', 'script', '1.0.4', 'builder_table_create_marcelhaupt_email_actions.php', '2018-04-06 19:01:16'),
(29, 'MarcelHaupt.Email', 'comment', '1.0.4', 'Created table marcelhaupt_email_actions', '2018-04-06 19:01:16'),
(30, 'MarcelHaupt.Email', 'script', '1.0.5', 'builder_table_create_marcelhaupt_email_conditions.php', '2018-04-06 19:01:17'),
(31, 'MarcelHaupt.Email', 'comment', '1.0.5', 'Created table marcelhaupt_email_conditions', '2018-04-06 19:01:17'),
(32, 'MarcelHaupt.Email', 'script', '1.0.6', 'builder_table_update_marcelhaupt_email_actions.php', '2018-04-06 19:01:17'),
(33, 'MarcelHaupt.Email', 'comment', '1.0.6', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:18'),
(34, 'MarcelHaupt.Email', 'script', '1.0.7', 'builder_table_update_marcelhaupt_email_user_settings.php', '2018-04-06 19:01:19'),
(35, 'MarcelHaupt.Email', 'comment', '1.0.7', 'Updated table marcelhaupt_email_user_settings', '2018-04-06 19:01:19'),
(36, 'MarcelHaupt.Email', 'script', '1.0.8', 'builder_table_update_marcelhaupt_email_actions_2.php', '2018-04-06 19:01:20'),
(37, 'MarcelHaupt.Email', 'comment', '1.0.8', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:20'),
(38, 'MarcelHaupt.Email', 'script', '1.0.9', 'builder_table_update_marcelhaupt_email_campaigns.php', '2018-04-06 19:01:22'),
(39, 'MarcelHaupt.Email', 'comment', '1.0.9', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:22'),
(40, 'MarcelHaupt.Email', 'script', '1.0.10', 'builder_table_update_marcelhaupt_email_campaigns_2.php', '2018-04-06 19:01:23'),
(41, 'MarcelHaupt.Email', 'comment', '1.0.10', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:24'),
(42, 'MarcelHaupt.Email', 'script', '1.0.11', 'builder_table_update_marcelhaupt_email_campaigns_3.php', '2018-04-06 19:01:26'),
(43, 'MarcelHaupt.Email', 'comment', '1.0.11', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:26'),
(44, 'MarcelHaupt.Email', 'script', '1.0.12', 'builder_table_update_marcelhaupt_email_conditions.php', '2018-04-06 19:01:26'),
(45, 'MarcelHaupt.Email', 'comment', '1.0.12', 'Updated table marcelhaupt_email_conditions', '2018-04-06 19:01:26'),
(46, 'MarcelHaupt.Email', 'script', '1.0.13', 'builder_table_update_marcelhaupt_email_campaigns_4.php', '2018-04-06 19:01:27'),
(47, 'MarcelHaupt.Email', 'comment', '1.0.13', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:27'),
(48, 'MarcelHaupt.Email', 'script', '1.0.14', 'builder_table_update_marcelhaupt_email_actions_3.php', '2018-04-06 19:01:31'),
(49, 'MarcelHaupt.Email', 'comment', '1.0.14', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:31'),
(50, 'MarcelHaupt.Email', 'script', '1.0.15', 'builder_table_update_marcelhaupt_email_actions_4.php', '2018-04-06 19:01:32'),
(51, 'MarcelHaupt.Email', 'comment', '1.0.15', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:32'),
(52, 'MarcelHaupt.Email', 'script', '1.0.16', 'builder_table_update_marcelhaupt_email_actions_5.php', '2018-04-06 19:01:34'),
(53, 'MarcelHaupt.Email', 'comment', '1.0.16', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:34'),
(54, 'MarcelHaupt.Email', 'script', '1.0.17', 'builder_table_update_marcelhaupt_email_actions_6.php', '2018-04-06 19:01:35'),
(55, 'MarcelHaupt.Email', 'comment', '1.0.17', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:35'),
(56, 'MarcelHaupt.Email', 'script', '1.0.18', 'builder_table_create_marcelhaupt_email_send_log.php', '2018-04-06 19:01:35'),
(57, 'MarcelHaupt.Email', 'comment', '1.0.18', 'Created table marcelhaupt_email_send_log', '2018-04-06 19:01:35'),
(58, 'MarcelHaupt.Email', 'script', '1.0.19', 'builder_table_create_marcelhaupt_email_action_log.php', '2018-04-06 19:01:36'),
(59, 'MarcelHaupt.Email', 'comment', '1.0.19', 'Created table marcelhaupt_email_action_log', '2018-04-06 19:01:36'),
(60, 'MarcelHaupt.Email', 'script', '1.0.20', 'builder_table_update_marcelhaupt_email_send_log.php', '2018-04-06 19:01:36'),
(61, 'MarcelHaupt.Email', 'comment', '1.0.20', 'Updated table marcelhaupt_email_send_log', '2018-04-06 19:01:36'),
(62, 'MarcelHaupt.Email', 'script', '1.0.21', 'builder_table_update_marcelhaupt_email_campaigns_5.php', '2018-04-06 19:01:37'),
(63, 'MarcelHaupt.Email', 'comment', '1.0.21', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:37'),
(64, 'MarcelHaupt.Email', 'script', '1.0.22', 'builder_table_update_marcelhaupt_email_campaigns_6.php', '2018-04-06 19:01:38'),
(65, 'MarcelHaupt.Email', 'comment', '1.0.22', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:38'),
(66, 'MarcelHaupt.Email', 'script', '1.0.23', 'builder_table_update_marcelhaupt_email_user_settings_2.php', '2018-04-06 19:01:39'),
(67, 'MarcelHaupt.Email', 'comment', '1.0.23', 'Updated table marcelhaupt_email_user_settings', '2018-04-06 19:01:39'),
(68, 'MarcelHaupt.Email', 'script', '1.0.24', 'builder_table_update_marcelhaupt_email_conditions_2.php', '2018-04-06 19:01:41'),
(69, 'MarcelHaupt.Email', 'comment', '1.0.24', 'Updated table marcelhaupt_email_conditions', '2018-04-06 19:01:41'),
(70, 'MarcelHaupt.Email', 'script', '1.0.25', 'builder_table_update_marcelhaupt_email_conditions_3.php', '2018-04-06 19:01:42'),
(71, 'MarcelHaupt.Email', 'comment', '1.0.25', 'Updated table marcelhaupt_email_conditions', '2018-04-06 19:01:42'),
(72, 'MarcelHaupt.Email', 'script', '1.0.26', 'builder_table_update_marcelhaupt_email_action_log.php', '2018-04-06 19:01:42'),
(73, 'MarcelHaupt.Email', 'comment', '1.0.26', 'Updated table marcelhaupt_email_action_log', '2018-04-06 19:01:43'),
(74, 'MarcelHaupt.Email', 'script', '1.0.27', 'builder_table_update_marcelhaupt_email_campaigns_7.php', '2018-04-06 19:01:43'),
(75, 'MarcelHaupt.Email', 'comment', '1.0.27', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:43'),
(76, 'MarcelHaupt.Email', 'script', '1.0.28', 'builder_table_update_marcelhaupt_email_send_log_2.php', '2018-04-06 19:01:43'),
(77, 'MarcelHaupt.Email', 'comment', '1.0.28', 'Updated table marcelhaupt_email_send_log', '2018-04-06 19:01:43'),
(78, 'MarcelHaupt.Email', 'script', '1.0.29', 'builder_table_update_marcelhaupt_email_user_settings_3.php', '2018-04-06 19:01:44'),
(79, 'MarcelHaupt.Email', 'comment', '1.0.29', 'Updated table marcelhaupt_email_user_settings', '2018-04-06 19:01:44'),
(80, 'MarcelHaupt.Email', 'script', '1.0.30', 'builder_table_update_marcelhaupt_email_action.php', '2018-04-06 19:01:44'),
(81, 'MarcelHaupt.Email', 'comment', '1.0.30', 'Updated table marcelhaupt_email_actions', '2018-04-06 19:01:44'),
(82, 'MarcelHaupt.Email', 'script', '1.0.31', 'builder_table_update_marcelhaupt_email_campaign.php', '2018-04-06 19:01:45'),
(83, 'MarcelHaupt.Email', 'comment', '1.0.31', 'Updated table marcelhaupt_email_campaigns', '2018-04-06 19:01:45'),
(84, 'MarcelHaupt.Email', 'script', '1.0.32', 'builder_table_update_marcelhaupt_email_condition.php', '2018-04-06 19:01:45'),
(85, 'MarcelHaupt.Email', 'comment', '1.0.32', 'Updated table marcelhaupt_email_conditions', '2018-04-06 19:01:45'),
(86, 'MarcelHaupt.Email', 'script', '1.0.33', 'builder_table_update_marcelhaupt_email_campaign_2.php', '2018-04-06 19:01:46'),
(87, 'MarcelHaupt.Email', 'comment', '1.0.33', 'Updated table marcelhaupt_email_campaign', '2018-04-06 19:01:46'),
(88, 'MarcelHaupt.Email', 'comment', '1.0.40', 'User Settings Hotfix', '2018-04-06 19:01:46'),
(89, 'MarcelHaupt.Email', 'comment', '1.0.41', 'Action Hotfix', '2018-04-06 19:01:46'),
(90, 'MarcelHaupt.Email', 'comment', '1.0.42', 'Download Emails button added', '2018-04-06 19:01:46'),
(91, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_forms_table.php', '2018-04-06 19:01:47'),
(92, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_fields_table.php', '2018-04-06 19:01:48'),
(93, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_field_types_table.php', '2018-04-06 19:01:49'),
(94, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_field_types_table.php', '2018-04-06 19:01:50'),
(95, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_contact_form.php', '2018-04-06 19:01:50'),
(96, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_default_form.php', '2018-04-06 19:01:51'),
(97, 'Renatio.FormBuilder', 'comment', '1.0.1', 'First version of Form Builder plugin.', '2018-04-06 19:01:51'),
(98, 'Renatio.FormBuilder', 'comment', '1.0.2', 'Reference __SELF__ in component partial.', '2018-04-06 19:01:51'),
(99, 'Renatio.FormBuilder', 'script', '1.0.3', 'seed_file_field_type.php', '2018-04-06 19:01:51'),
(100, 'Renatio.FormBuilder', 'comment', '1.0.3', 'Add file attachment field type.', '2018-04-06 19:01:51'),
(101, 'Renatio.FormBuilder', 'script', '1.0.4', 'create_form_logs_table.php', '2018-04-06 19:01:53'),
(102, 'Renatio.FormBuilder', 'comment', '1.0.4', 'Add simple logs.', '2018-04-06 19:01:53'),
(103, 'Renatio.FormBuilder', 'comment', '1.1.0', '!!! Important update', '2018-04-06 19:01:53'),
(104, 'Renatio.FormBuilder', 'script', '1.1.1', 'add_css_class_column_to_forms_table.php', '2018-04-06 19:01:54'),
(105, 'Renatio.FormBuilder', 'comment', '1.1.1', 'Use Twig::parse() facade. Only update for October build 300 and above!', '2018-04-06 19:01:55'),
(106, 'Renatio.FormBuilder', 'comment', '1.1.2', 'Allow for multiple forms on the same cms page. Fix required validation rule for fileuploader field type.', '2018-04-06 19:01:55'),
(107, 'Renatio.FormBuilder', 'comment', '1.1.3', 'Minor changes.', '2018-04-06 19:01:55'),
(108, 'Renatio.FormBuilder', 'script', '1.1.4', 'seed_location_field_types.php', '2018-04-06 19:01:55'),
(109, 'Renatio.FormBuilder', 'script', '1.1.4', 'add_css_wrapper_class_column_to_fields_table.php', '2018-04-06 19:01:56'),
(110, 'Renatio.FormBuilder', 'comment', '1.1.4', 'RainLab Location Plugin support.', '2018-04-06 19:01:56'),
(111, 'Renatio.FormBuilder', 'comment', '1.1.5', 'Add Event to extend default functionality.', '2018-04-06 19:01:57'),
(112, 'Renatio.FormBuilder', 'script', '1.1.6', 'add_reply_and_autoresponse_fields.php', '2018-04-06 19:01:58'),
(113, 'Renatio.FormBuilder', 'comment', '1.1.6', 'Add reply to and autoresponse functionality.', '2018-04-06 19:01:58'),
(114, 'Renatio.FormBuilder', 'script', '1.1.7', 'create_sections_table.php', '2018-04-06 19:02:00'),
(115, 'Renatio.FormBuilder', 'script', '1.1.7', 'seed_section_field_type.php', '2018-04-06 19:02:00'),
(116, 'Renatio.FormBuilder', 'comment', '1.1.7', 'Add sections feature.', '2018-04-06 19:02:00'),
(117, 'Renatio.FormBuilder', 'script', '1.1.8', 'add_content_html_column_to_form_logs_table.php', '2018-04-06 19:02:01'),
(118, 'Renatio.FormBuilder', 'comment', '1.1.8', 'Add HTML preview in logs.', '2018-04-06 19:02:01'),
(119, 'Renatio.FormBuilder', 'script', '1.1.9', 'add_comment_column_to_fields_table.php', '2018-04-06 19:02:03'),
(120, 'Renatio.FormBuilder', 'comment', '1.1.9', 'Add comment to fields.', '2018-04-06 19:02:03'),
(121, 'Renatio.FormBuilder', 'comment', '1.2.0', 'Dutch translation.', '2018-04-06 19:02:03'),
(122, 'Renatio.FormBuilder', 'script', '1.2.1', 'add_validation_messages_column_to_fields_table.php', '2018-04-06 19:02:04'),
(123, 'Renatio.FormBuilder', 'comment', '1.2.1', 'Allow for custom validation messages.', '2018-04-06 19:02:04'),
(124, 'Renatio.FormBuilder', 'comment', '1.2.2', 'Allow to manually specify url for redirect.', '2018-04-06 19:02:04'),
(125, 'Renatio.FormBuilder', 'comment', '1.2.3', 'German translation.', '2018-04-06 19:02:05'),
(126, 'Renatio.FormBuilder', 'comment', '1.2.4', 'Minor changes.', '2018-04-06 19:02:05'),
(127, 'Renatio.FormBuilder', 'comment', '1.2.5', 'Allow to duplicate a form.', '2018-04-06 19:02:05'),
(128, 'Renatio.FormBuilder', 'comment', '1.2.6', 'Export form logs to CSV file.', '2018-04-06 19:02:05'),
(129, 'Renatio.FormBuilder', 'comment', '1.2.7', 'Improve seeding mail templates. Add Contact Form and Default Form. Update Mail Layout.', '2018-04-06 19:02:05'),
(130, 'Renatio.FormBuilder', 'comment', '1.2.8', 'Fix sending email with file attachment for October build 401+', '2018-04-06 19:02:05'),
(131, 'Renatio.FormBuilder', 'script', '1.2.9', 'fix_rainlab_translate_issues.php', '2018-04-06 19:02:05'),
(132, 'Renatio.FormBuilder', 'comment', '1.2.9', 'Fix RainLab.Translate compatibility issues.', '2018-04-06 19:02:05'),
(133, 'Renatio.FormBuilder', 'comment', '1.3.0', 'Allow to use CMS filters and functions in field templates.', '2018-04-06 19:02:06'),
(134, 'Renatio.FormBuilder', 'comment', '1.3.1', 'Fix example forms seeding.', '2018-04-06 19:02:06'),
(135, 'Renatio.FormBuilder', 'comment', '1.4.0', '!!! Nicer form field validation.', '2018-04-06 19:02:06'),
(136, 'Renatio.FormBuilder', 'comment', '1.4.1', 'Set default reCaptcha keys for testing.', '2018-04-06 19:02:06'),
(137, 'Renatio.FormBuilder', 'comment', '1.4.2', 'Fix fields and sections translation.', '2018-04-06 19:02:07'),
(138, 'Renatio.SeoManager', 'script', '1.0.1', 'create_seo_tags_table.php', '2018-04-06 19:02:07'),
(139, 'Renatio.SeoManager', 'comment', '1.0.1', 'First version of Seo Manager.', '2018-04-06 19:02:08'),
(140, 'Renatio.SeoManager', 'script', '1.0.2', 'import_default_values.php', '2018-04-06 19:02:09'),
(141, 'Renatio.SeoManager', 'comment', '1.0.2', 'Import data.', '2018-04-06 19:02:09'),
(142, 'Renatio.SeoManager', 'comment', '1.0.3', 'Import SeoExtension Settings.', '2018-04-06 19:02:09'),
(143, 'Renatio.SeoManager', 'comment', '1.0.4', 'Fix bug for PHP version < 5.6.', '2018-04-06 19:02:09'),
(144, 'Renatio.SeoManager', 'comment', '1.0.5', 'Fix spaces on prefix/suffix title.', '2018-04-06 19:02:09'),
(145, 'Renatio.SeoManager', 'comment', '1.0.6', 'Fix repeater field error in RainLab.Pages plugin.', '2018-04-06 19:02:09'),
(146, 'Renatio.SeoManager', 'comment', '1.0.7', 'Fix showing Seo fields in repeater.', '2018-04-06 19:02:09'),
(147, 'Renatio.SeoManager', 'comment', '1.1.0', '!!! Update only for Laravel 5.5 LTS.', '2018-04-06 19:02:10'),
(148, 'Renatio.SeoManager', 'comment', '1.1.1', 'Fix RainLab.Blog installation issue.', '2018-04-06 19:02:10'),
(149, 'Renatio.SeoManager', 'comment', '1.1.2', 'Fix attaching SEO fields to model with empty tab fields.', '2018-04-06 19:02:10'),
(150, 'Renatio.SeoManager', 'comment', '1.1.3', 'Fix attaching morphOne relation to model.', '2018-04-06 19:02:10'),
(151, 'Renatio.SeoManager', 'comment', '1.1.4', 'Fix adding SEO fields for model with pivot.', '2018-04-06 19:02:10'),
(152, 'Renatio.SeoManager', 'script', '1.1.5', 'update_seo_fields_length.php', '2018-04-06 19:02:10'),
(153, 'Renatio.SeoManager', 'comment', '1.1.5', 'Increase meta title and description fields length.', '2018-04-06 19:02:10'),
(154, 'Renatio.SeoManager', 'script', '1.1.6', 'update_morph_fields_to_nullable.php', '2018-04-06 19:02:11'),
(155, 'Renatio.SeoManager', 'comment', '1.1.6', 'Make polymorphic fields nullable.', '2018-04-06 19:02:11'),
(156, 'Renatio.SeoManager', 'comment', '1.1.7', 'Fix adding SEO fields inside repeater fields.', '2018-04-06 19:02:11'),
(157, 'Renatio.SeoManager', 'comment', '1.1.8', 'Ensure OG image dimensions are set.', '2018-04-06 19:02:11'),
(158, 'Renatio.SeoManager', 'comment', '1.1.9', 'Fix adding SEO fields to pivot models (edge case).', '2018-04-06 19:02:11'),
(159, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2018-04-06 19:14:48'),
(160, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2018-04-06 19:14:49'),
(161, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-06 19:14:49'),
(162, 'RainLab.User', 'comment', '1.0.2', 'Seed tables.', '2018-04-06 19:14:49'),
(163, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2018-04-06 19:14:49'),
(164, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2018-04-06 19:14:49'),
(165, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2018-04-06 19:14:50'),
(166, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2018-04-06 19:14:51'),
(167, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2018-04-06 19:14:51'),
(168, 'RainLab.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2018-04-06 19:14:52'),
(169, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2018-04-06 19:14:52'),
(170, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2018-04-06 19:14:52'),
(171, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2018-04-06 19:14:52'),
(172, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2018-04-06 19:14:55'),
(173, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2018-04-06 19:14:55'),
(174, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2018-04-06 19:14:56'),
(175, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2018-04-06 19:14:56'),
(176, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2018-04-06 19:14:56'),
(177, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2018-04-06 19:14:56'),
(178, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2018-04-06 19:14:57'),
(179, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2018-04-06 19:14:57'),
(180, 'RainLab.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2018-04-06 19:14:58'),
(181, 'RainLab.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2018-04-06 19:14:58'),
(182, 'RainLab.User', 'script', '1.1.1', 'create_user_groups_table.php', '2018-04-06 19:15:00'),
(183, 'RainLab.User', 'script', '1.1.1', 'seed_user_groups_table.php', '2018-04-06 19:15:01'),
(184, 'RainLab.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2018-04-06 19:15:01'),
(185, 'RainLab.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2018-04-06 19:15:01'),
(186, 'RainLab.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2018-04-06 19:15:01'),
(187, 'RainLab.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2018-04-06 19:15:01'),
(188, 'RainLab.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2018-04-06 19:15:01'),
(189, 'RainLab.User', 'script', '1.2.0', 'users_add_deleted_at.php', '2018-04-06 19:15:02'),
(190, 'RainLab.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2018-04-06 19:15:02'),
(191, 'RainLab.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2018-04-06 19:15:03'),
(192, 'RainLab.User', 'comment', '1.2.2', 'Add bulk action button to user list.', '2018-04-06 19:15:03'),
(193, 'RainLab.User', 'comment', '1.2.3', 'Included some descriptive paragraphs in the Reset Password component markup.', '2018-04-06 19:15:03'),
(194, 'RainLab.User', 'comment', '1.2.4', 'Added a checkbox for blocking all mail sent to the user.', '2018-04-06 19:15:03'),
(195, 'RainLab.User', 'script', '1.2.5', 'update_timestamp_nullable.php', '2018-04-06 19:15:04'),
(196, 'RainLab.User', 'comment', '1.2.5', 'Database maintenance. Updated all timestamp columns to be nullable.', '2018-04-06 19:15:04'),
(197, 'RainLab.User', 'script', '1.2.6', 'users_add_last_seen.php', '2018-04-06 19:15:05'),
(198, 'RainLab.User', 'comment', '1.2.6', 'Add a dedicated last seen column for users.', '2018-04-06 19:15:05'),
(199, 'RainLab.User', 'comment', '1.2.7', 'Minor fix to user timestamp attributes.', '2018-04-06 19:15:05'),
(200, 'RainLab.User', 'comment', '1.2.8', 'Add date range filter to users list. Introduced a logout event.', '2018-04-06 19:15:05'),
(201, 'RainLab.User', 'comment', '1.2.9', 'Add invitation mail for new accounts created in the back-end.', '2018-04-06 19:15:05'),
(202, 'RainLab.User', 'script', '1.3.0', 'users_add_guest_flag.php', '2018-04-06 19:15:06'),
(203, 'RainLab.User', 'script', '1.3.0', 'users_add_superuser_flag.php', '2018-04-06 19:15:08'),
(204, 'RainLab.User', 'comment', '1.3.0', 'Introduced guest user accounts.', '2018-04-06 19:15:08'),
(205, 'RainLab.User', 'comment', '1.3.1', 'User notification variables can now be extended.', '2018-04-06 19:15:08'),
(206, 'RainLab.User', 'comment', '1.3.2', 'Minor fix to the Auth::register method.', '2018-04-06 19:15:08'),
(207, 'RainLab.User', 'comment', '1.3.3', 'Allow prevention of concurrent user sessions via the user settings.', '2018-04-06 19:15:08'),
(208, 'RainLab.User', 'comment', '1.3.4', 'Added force secure protocol property to the account component.', '2018-04-06 19:15:09'),
(209, 'RainLab.User', 'comment', '1.4.0', '!!! The Notifications tab in User settings has been removed.', '2018-04-06 19:15:09'),
(210, 'RainLab.User', 'comment', '1.4.1', 'Added support for user impersonation.', '2018-04-06 19:15:09'),
(211, 'RainLab.User', 'comment', '1.4.2', 'Fixes security bug in Password Reset component.', '2018-04-06 19:15:09'),
(212, 'RainLab.User', 'comment', '1.4.3', 'Fixes session handling for AJAX requests.', '2018-04-06 19:15:09'),
(213, 'RainLab.User', 'comment', '1.4.4', 'Fixes bug where impersonation touches the last seen timestamp.', '2018-04-06 19:15:10'),
(214, 'RainLab.User', 'comment', '1.4.5', 'Added token fallback process to Account / Reset Password components when parameter is missing.', '2018-04-06 19:15:10'),
(215, 'Jiri.JKShop', 'script', '1.0.1', 'create_categories_table.php', '2018-04-06 19:15:11'),
(216, 'Jiri.JKShop', 'comment', '1.0.1', 'First version of JK Shop', '2018-04-06 19:15:11'),
(217, 'Jiri.JKShop', 'script', '1.0.2', 'create_brands_table.php', '2018-04-06 19:15:12'),
(218, 'Jiri.JKShop', 'comment', '1.0.2', 'Add brands', '2018-04-06 19:15:12'),
(219, 'Jiri.JKShop', 'script', '1.0.3', 'create_taxes_table.php', '2018-04-06 19:15:13'),
(220, 'Jiri.JKShop', 'comment', '1.0.3', 'Add Taxes', '2018-04-06 19:15:13'),
(221, 'Jiri.JKShop', 'script', '1.0.4', 'create_carriers_table.php', '2018-04-06 19:15:14'),
(222, 'Jiri.JKShop', 'comment', '1.0.4', 'Add Carriers', '2018-04-06 19:15:14'),
(223, 'Jiri.JKShop', 'script', '1.0.5', 'create_order_statuses_table.php', '2018-04-06 19:15:14'),
(224, 'Jiri.JKShop', 'comment', '1.0.5', 'Add Order Statuses', '2018-04-06 19:15:14'),
(225, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_table.php', '2018-04-06 19:15:15'),
(226, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_categories.php', '2018-04-06 19:15:15'),
(227, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_carriers_disallowed.php', '2018-04-06 19:15:15'),
(228, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_products_featured.php', '2018-04-06 19:15:16'),
(229, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_products_accessories.php', '2018-04-06 19:15:16'),
(230, 'Jiri.JKShop', 'comment', '1.0.6', 'Add Products', '2018-04-06 19:15:16'),
(231, 'Jiri.JKShop', 'script', '1.0.7', 'create_orders_table.php', '2018-04-06 19:15:17'),
(232, 'Jiri.JKShop', 'comment', '1.0.7', 'Add orders', '2018-04-06 19:15:17'),
(233, 'Jiri.JKShop', 'script', '1.0.8', 'create_products_users_price.php', '2018-04-06 19:15:18'),
(234, 'Jiri.JKShop', 'script', '1.0.8', 'create_categories_users_sale.php', '2018-04-06 19:15:20'),
(235, 'Jiri.JKShop', 'comment', '1.0.8', 'Add support RainLab.User (sale for category, individual prices)', '2018-04-06 19:15:20'),
(236, 'Jiri.JKShop', 'script', '1.0.9', 'create_sample_data.php', '2018-04-06 19:15:20'),
(237, 'Jiri.JKShop', 'comment', '1.0.9', 'First Release', '2018-04-06 19:15:21'),
(238, 'Jiri.JKShop', 'script', '1.0.10', 'Component - My Orders - fix link on invoice', '2018-04-06 19:15:21'),
(239, 'Jiri.JKShop', 'comment', '1.0.10', 'Fixes', '2018-04-06 19:15:21'),
(240, 'Jiri.JKShop', 'comment', '1.0.11', 'Backend - language files [EN], Fixes: Links featured + accessories, Basket - work with product minimum quantity, Complete Order - check stock before order', '2018-04-06 19:15:21'),
(241, 'Jiri.JKShop', 'comment', '1.0.12', 'Automatic rounding into order total price - Paypal allow max 2 decimal places', '2018-04-06 19:15:21'),
(242, 'Jiri.JKShop', 'comment', '1.0.13', 'Orders - fix search, add hidden fields into order list (Email, Phone, First name, Address, Postcode, City, Country), all fields are searchable', '2018-04-06 19:15:21'),
(243, 'Jiri.JKShop', 'comment', '1.0.14', 'Fixes: Order detail -  names on list of customers, Orders list - little UI bug', '2018-04-06 19:15:22'),
(244, 'Jiri.JKShop', 'comment', '1.0.15', 'Fix: Carrier - free shipping; Add: Italian translation', '2018-04-06 19:15:22'),
(245, 'Jiri.JKShop', 'script', '1.1.0', 'create_properties_table.php', '2018-04-06 19:15:22'),
(246, 'Jiri.JKShop', 'script', '1.1.0', 'create_property_options_table.php', '2018-04-06 19:15:23'),
(247, 'Jiri.JKShop', 'script', '1.1.0', 'create_products_properties.php', '2018-04-06 19:15:24'),
(248, 'Jiri.JKShop', 'comment', '1.1.0', 'Properties - product properties: sizes, colors, etc.. (https://octobercms.com/plugin/jiri-jkshop#Properties)', '2018-04-06 19:15:24'),
(249, 'Jiri.JKShop', 'script', '1.1.1', 'update_111.php', '2018-04-06 19:15:25'),
(250, 'Jiri.JKShop', 'comment', '1.1.1', 'Add: Order - note, county; New page variables for components (Products By Category, Products By Brand)', '2018-04-06 19:15:25'),
(251, 'Jiri.JKShop', 'script', '1.1.2', 'create_products_options.php', '2018-04-06 19:15:25'),
(252, 'Jiri.JKShop', 'comment', '1.1.2', 'Add: Product - Advanced Properties ( You can add property options with pivot data: title, description, price difference, image ); Update Components: Basket, Product Detail', '2018-04-06 19:15:25'),
(253, 'Jiri.JKShop', 'comment', '1.1.3', 'Fix: Advanced Properties - Options - Order by', '2018-04-06 19:15:25'),
(254, 'Jiri.JKShop', 'script', '1.1.4', 'update_114.php', '2018-04-06 19:15:27'),
(255, 'Jiri.JKShop', 'comment', '1.1.4', 'Add: Order - tracking number for tracking package', '2018-04-06 19:15:27'),
(256, 'Jiri.JKShop', 'comment', '1.2.0', 'Add: Stripe payment gateway', '2018-04-06 19:15:27'),
(257, 'Jiri.JKShop', 'comment', '1.2.1', 'Add: Czech translation', '2018-04-06 19:15:27'),
(258, 'Jiri.JKShop', 'script', '1.2.2', 'create_rainlab_extend_users.php', '2018-04-06 19:15:29'),
(259, 'Jiri.JKShop', 'comment', '1.2.2', 'Add: Settings - default image; Rainlab.User: save billing and delivery address into user; Products Components - add or extend order by', '2018-04-06 19:15:29'),
(260, 'Jiri.JKShop', 'comment', '1.2.3', 'Small fixes and improvements: add some language texts, page 404 for non-existent slug - products, categories, brands', '2018-04-06 19:15:29'),
(261, 'Jiri.JKShop', 'comment', '1.2.4', 'Fix: order list - order by order status', '2018-04-06 19:15:29'),
(262, 'Jiri.JKShop', 'comment', '1.3.0', 'Added translation support', '2018-04-06 19:15:29'),
(263, 'Jiri.JKShop', 'comment', '1.3.1', 'Small Fixes', '2018-04-06 19:15:29'),
(264, 'Jiri.JKShop', 'comment', '1.3.2', 'Fix name of Users without surname', '2018-04-06 19:15:29'),
(265, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_table.php', '2018-04-06 19:15:30'),
(266, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_categories.php', '2018-04-06 19:15:30'),
(267, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_products.php', '2018-04-06 19:15:31'),
(268, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_users.php', '2018-04-06 19:15:32'),
(269, 'Jiri.JKShop', 'script', '1.4.0', 'udpate_orders_140.php', '2018-04-06 19:15:36'),
(270, 'Jiri.JKShop', 'comment', '1.4.0', 'Coupons - discount coupons (https://octobercms.com/plugin/jiri-jkshop#Coupons)', '2018-04-06 19:15:37'),
(271, 'Jiri.JKShop', 'comment', '1.4.1', 'Small Fixes in Coupons', '2018-04-06 19:15:37'),
(272, 'Jiri.JKShop', 'comment', '1.4.2', 'Increased character limits (name, title, etc.) to 255', '2018-04-06 19:15:37'),
(273, 'Jiri.JKShop', 'comment', '1.4.3', 'Add: German translation', '2018-04-06 19:15:37'),
(274, 'Jiri.JKShop', 'script', '1.4.4', 'update_144.php', '2018-04-06 19:15:38'),
(275, 'Jiri.JKShop', 'comment', '1.4.4', 'Add Components: BrandDetails, BrandsList. Thanks to Nick Gavanozov', '2018-04-06 19:15:38'),
(276, 'Jiri.JKShop', 'comment', '1.4.5', 'Small Fix in Coupons - datepicker issue', '2018-04-06 19:15:38'),
(277, 'Jiri.JKShop', 'comment', '1.4.6', 'Add Components: BreadcrumbsCategory, BreadcrumbsProduct', '2018-04-06 19:15:38'),
(278, 'Jiri.JKShop', 'comment', '1.4.7', 'Component BreadcrumbsProduct - fix URL', '2018-04-06 19:15:39'),
(279, 'Jiri.JKShop', 'script', '1.5.0', 'create_payment_gateways_table.php', '2018-04-06 19:15:42'),
(280, 'Jiri.JKShop', 'script', '1.5.0', 'update_orders_150.php', '2018-04-06 19:15:45'),
(281, 'Jiri.JKShop', 'comment', '1.5.0', 'Check upgrade guide before upgrading: Add payment gateways, remove previous payment system (https://octobercms.com/plugin/jiri-jkshop#upgrade)', '2018-04-06 19:15:45'),
(282, 'Jiri.JKShop', 'comment', '1.5.1', 'Basket component: moved htm files into basket folder, for easy overriding. Payment Gateways: gateway title is translatable.', '2018-04-06 19:15:45'),
(283, 'Jiri.JKShop', 'script', '1.5.2', 'update_152.php', '2018-04-06 19:15:50'),
(284, 'Jiri.JKShop', 'comment', '1.5.2', 'Small fixes: MyOrderDetail component, MyOrders component - if user or id order is bad, return 404, Add Foreign keys', '2018-04-06 19:15:50'),
(285, 'Jiri.JKShop', 'comment', '1.5.3', 'Basket Component: added method \'onGetSessionBasket\' - now you can easy call this method and get complete basket as JSON', '2018-04-06 19:15:50'),
(286, 'Jiri.JKShop', 'comment', '1.5.4', 'Upgrade: Omnipay payment gateways, Small Fixes in Components', '2018-04-06 19:15:50'),
(287, 'Jiri.JKShop', 'comment', '1.5.5', 'Add payment gateways: TwoCheckoutPlus, TwoCheckoutPlus_Token', '2018-04-06 19:15:50'),
(288, 'Jiri.JKShop', 'comment', '1.5.6', 'Fixed DB error on some MySQL databases', '2018-04-06 19:15:50'),
(289, 'Jiri.JKShop', 'comment', '1.5.7', 'Small fixes: Product Detail component fixed URL for accessories and featured products', '2018-04-06 19:15:50'),
(290, 'Jiri.JKShop', 'comment', '1.5.8', 'Add: French translation (thanks Théo Corée)', '2018-04-06 19:15:51'),
(291, 'Jiri.JKShop', 'comment', '1.5.9', 'Add security checks for payment components', '2018-04-06 19:15:51'),
(292, 'Jiri.JKShop', 'script', '1.6.0', 'update_statuses_160.php', '2018-04-06 19:15:54'),
(293, 'Jiri.JKShop', 'comment', '1.6.0', 'Add Extended Inventory Management. OrderStatus - add new fields: disallow for gateway, qty decrease, qty increase back', '2018-04-06 19:15:54'),
(294, 'Jiri.JKShop', 'comment', '1.6.1', 'Add some features into basket and product details components: allow to add product into basket with qty and etc..', '2018-04-06 19:15:54'),
(295, 'Jiri.JKShop', 'comment', '1.6.2', 'Update: Italian translation', '2018-04-06 19:15:54'),
(296, 'Jiri.JKShop', 'comment', '1.6.3', 'Fixed DB error - installation on PostgreSQL', '2018-04-06 19:15:54'),
(297, 'Jiri.JKShop', 'comment', '1.6.4', 'Backend - Products: Add checkboxes, Add multi delete, Add multi duplicate', '2018-04-06 19:15:54'),
(298, 'Jiri.JKShop', 'script', '1.6.5', 'update_orders_165.php', '2018-04-06 19:15:55'),
(299, 'Jiri.JKShop', 'comment', '1.6.5', 'Orders: add security token - which allow to generate unique URL for a payment pages', '2018-04-06 19:15:55'),
(300, 'Jiri.JKShop', 'comment', '1.6.6', 'Add: Dutch translation (thanks Jeroen)', '2018-04-06 19:15:55'),
(301, 'Jiri.JKShop', 'comment', '1.6.7', 'Add coupon - coupon_wrong_code update - error message about validation it shown only once', '2018-04-06 19:15:55'),
(302, 'Jiri.JKShop', 'comment', '1.6.8', 'Fixed new installation issue', '2018-04-06 19:15:56'),
(303, 'Jiri.JKShop', 'comment', '1.6.9', 'Fixed Payment Gateway component issue - https://octobercms.com/plugin/support/jiri-jkshop/error-onpaymentsubmit', '2018-04-06 19:15:56'),
(304, 'Jiri.JKShop', 'comment', '1.6.10', 'Update vendors (dompdf, omnipay, etc..), small fixes', '2018-04-06 19:15:56'),
(305, 'Jiri.JKShop', 'comment', '1.6.11', 'Small fixes, add search box for accessories and features', '2018-04-06 19:15:56'),
(306, 'Jiri.JKShop', 'comment', '1.6.12', 'Small fixes, add eager-loading for a images to a components: BrandList, ProdctsByBrand, ProductsByCategory, ProductsList - https://octobercms.com/plugin/support/jiri-jkshop/optimize-loading-product-images', '2018-04-06 19:15:56'),
(307, 'Jiri.JKShop', 'script', '1.7.0', 'update_170.php', '2018-04-06 19:15:56'),
(308, 'Jiri.JKShop', 'comment', '1.7.0', 'Extended stock inventory - every product property can have set extended quantity - if this is set main product quantity will be ignored. Example: size X-10pcs; S-5pcs. More info there - https://octobercms.com/plugin/support/jiri-jkshop/feature-request-adding-inventory-per-property?page=1', '2018-04-06 19:15:56'),
(309, 'Jiri.JKShop', 'comment', '1.7.1', 'Small fixes on components: custom payment stripe, product detail, basket - filtering available carrier by total weight', '2018-04-06 19:15:56'),
(310, 'Jiri.JKShop', 'comment', '1.7.2', 'Fix: previous version - doubled product property', '2018-04-06 19:15:56'),
(311, 'Jiri.JKShop', 'script', '1.7.3', 'update_173.php', '2018-04-06 19:15:58'),
(312, 'Jiri.JKShop', 'comment', '1.7.3', 'Add virtual product option; Product - New Tab: Virtual Product; Order Status - new checkbox: is_paid => Consumer can download virtual product from an order detail if current order status is paid.', '2018-04-06 19:15:58'),
(313, 'Jiri.JKShop', 'comment', '1.7.4', 'Fix: installation on PostgreSQL', '2018-04-06 19:15:58'),
(314, 'Jiri.JKShop', 'comment', '1.7.5', 'Update vendors (dompdf, omnipay, etc..), small fix', '2018-04-06 19:15:59'),
(315, 'RainLab.Pages', 'comment', '1.0.1', 'Implemented the static pages management and the Static Page component.', '2018-06-21 13:01:28'),
(316, 'RainLab.Pages', 'comment', '1.0.2', 'Fixed the page preview URL.', '2018-06-21 13:01:28'),
(317, 'RainLab.Pages', 'comment', '1.0.3', 'Implemented menus.', '2018-06-21 13:01:28'),
(318, 'RainLab.Pages', 'comment', '1.0.4', 'Implemented the content block management and placeholder support.', '2018-06-21 13:01:28'),
(319, 'RainLab.Pages', 'comment', '1.0.5', 'Added support for the Sitemap plugin.', '2018-06-21 13:01:28'),
(320, 'RainLab.Pages', 'comment', '1.0.6', 'Minor updates to the internal API.', '2018-06-21 13:01:29'),
(321, 'RainLab.Pages', 'comment', '1.0.7', 'Added the Snippets feature.', '2018-06-21 13:01:29'),
(322, 'RainLab.Pages', 'comment', '1.0.8', 'Minor improvements to the code.', '2018-06-21 13:01:29'),
(323, 'RainLab.Pages', 'comment', '1.0.9', 'Fixes issue where Snippet tab is missing from the Partials form.', '2018-06-21 13:01:29'),
(324, 'RainLab.Pages', 'comment', '1.0.10', 'Add translations for various locales.', '2018-06-21 13:01:29'),
(325, 'RainLab.Pages', 'comment', '1.0.11', 'Fixes issue where placeholders tabs were missing from Page form.', '2018-06-21 13:01:29'),
(326, 'RainLab.Pages', 'comment', '1.0.12', 'Implement Media Manager support.', '2018-06-21 13:01:29'),
(327, 'RainLab.Pages', 'script', '1.1.0', 'snippets_rename_viewbag_properties.php', '2018-06-21 13:01:32'),
(328, 'RainLab.Pages', 'comment', '1.1.0', 'Adds meta title and description to pages. Adds |staticPage filter.', '2018-06-21 13:01:32'),
(329, 'RainLab.Pages', 'comment', '1.1.1', 'Add support for Syntax Fields.', '2018-06-21 13:01:32'),
(330, 'RainLab.Pages', 'comment', '1.1.2', 'Static Breadcrumbs component now respects the hide from navigation setting.', '2018-06-21 13:01:32'),
(331, 'RainLab.Pages', 'comment', '1.1.3', 'Minor back-end styling fix.', '2018-06-21 13:01:32'),
(332, 'RainLab.Pages', 'comment', '1.1.4', 'Minor fix to the StaticPage component API.', '2018-06-21 13:01:32'),
(333, 'RainLab.Pages', 'comment', '1.1.5', 'Fixes bug when using syntax fields.', '2018-06-21 13:01:32'),
(334, 'RainLab.Pages', 'comment', '1.1.6', 'Minor styling fix to the back-end UI.', '2018-06-21 13:01:33'),
(335, 'RainLab.Pages', 'comment', '1.1.7', 'Improved menu item form to include CSS class, open in a new window and hidden flag.', '2018-06-21 13:01:33'),
(336, 'RainLab.Pages', 'comment', '1.1.8', 'Improved the output of snippet partials when saved.', '2018-06-21 13:01:33'),
(337, 'RainLab.Pages', 'comment', '1.1.9', 'Minor update to snippet inspector internal API.', '2018-06-21 13:01:33'),
(338, 'RainLab.Pages', 'comment', '1.1.10', 'Fixes a bug where selecting a layout causes permanent unsaved changes.', '2018-06-21 13:01:33'),
(339, 'RainLab.Pages', 'comment', '1.1.11', 'Add support for repeater syntax field.', '2018-06-21 13:01:33'),
(340, 'RainLab.Pages', 'comment', '1.2.0', 'Added support for translations, UI updates.', '2018-06-21 13:01:33'),
(341, 'RainLab.Pages', 'comment', '1.2.1', 'Use nice titles when listing the content files.', '2018-06-21 13:01:33'),
(342, 'RainLab.Pages', 'comment', '1.2.2', 'Minor styling update.', '2018-06-21 13:01:33'),
(343, 'RainLab.Pages', 'comment', '1.2.3', 'Snippets can now be moved by dragging them.', '2018-06-21 13:01:34'),
(344, 'RainLab.Pages', 'comment', '1.2.4', 'Fixes a bug where the cursor is misplaced when editing text files.', '2018-06-21 13:01:34'),
(345, 'RainLab.Pages', 'comment', '1.2.5', 'Fixes a bug where the parent page is lost upon changing a page layout.', '2018-06-21 13:01:34'),
(346, 'RainLab.Pages', 'comment', '1.2.6', 'Shared view variables are now passed to static pages.', '2018-06-21 13:01:34'),
(347, 'RainLab.Pages', 'comment', '1.2.7', 'Fixes issue with duplicating properties when adding multiple snippets on the same page.', '2018-06-21 13:01:34'),
(348, 'RainLab.Pages', 'comment', '1.2.8', 'Fixes a bug where creating a content block without extension doesn\'t save the contents to file.', '2018-06-21 13:01:35'),
(349, 'RainLab.Pages', 'comment', '1.2.9', 'Add conditional support for translating page URLs.', '2018-06-21 13:01:35'),
(350, 'RainLab.Pages', 'comment', '1.2.10', 'Streamline generation of URLs to use the new Cms::url helper.', '2018-06-21 13:01:35'),
(351, 'RainLab.Pages', 'comment', '1.2.11', 'Implements repeater usage with translate plugin.', '2018-06-21 13:01:35'),
(352, 'RainLab.Pages', 'comment', '1.2.12', 'Fixes minor issue when using snippets and switching the application locale.', '2018-06-21 13:01:35'),
(353, 'RainLab.Pages', 'comment', '1.2.13', 'Fixes bug when AJAX is used on a page that does not yet exist.', '2018-06-21 13:01:35'),
(354, 'RainLab.Pages', 'comment', '1.2.14', 'Add theme logging support for changes made to menus.', '2018-06-21 13:01:36'),
(355, 'RainLab.Pages', 'comment', '1.2.15', 'Back-end navigation sort order updated.', '2018-06-21 13:01:36'),
(356, 'RainLab.Pages', 'comment', '1.2.16', 'Fixes a bug when saving a template that has been modified outside of the CMS (mtime mismatch).', '2018-06-21 13:01:36'),
(357, 'RainLab.Pages', 'comment', '1.2.17', 'Changes locations of custom fields to secondary tabs instead of the primary Settings area. New menu search ability on adding menu items', '2018-06-21 13:01:36'),
(358, 'RainLab.Pages', 'comment', '1.2.18', 'Fixes cache-invalidation issues when RainLab.Translate is not installed. Added Greek & Simplified Chinese translations. Removed deprecated calls. Allowed saving HTML in snippet properties. Added support for the MediaFinder in menu items.', '2018-06-21 13:01:36'),
(359, 'RainLab.Sitemap', 'comment', '1.0.1', 'First version of Sitemap', '2018-06-21 13:01:36'),
(360, 'RainLab.Sitemap', 'script', '1.0.2', 'create_definitions_table.php', '2018-06-21 13:01:39'),
(361, 'RainLab.Sitemap', 'comment', '1.0.2', 'Create definitions table', '2018-06-21 13:01:39'),
(362, 'RainLab.Sitemap', 'comment', '1.0.3', 'Minor improvements to the code.', '2018-06-21 13:01:39'),
(363, 'RainLab.Sitemap', 'comment', '1.0.4', 'Fixes issue where correct headers not being sent.', '2018-06-21 13:01:39'),
(364, 'RainLab.Sitemap', 'comment', '1.0.5', 'Minor back-end styling fix.', '2018-06-21 13:01:39'),
(365, 'RainLab.Sitemap', 'comment', '1.0.6', 'Minor fix to internal API.', '2018-06-21 13:01:40'),
(366, 'RainLab.Sitemap', 'comment', '1.0.7', 'Added access premissions.', '2018-06-21 13:01:40'),
(367, 'RainLab.Sitemap', 'comment', '1.0.8', 'Minor styling updates.', '2018-06-21 13:01:40'),
(368, 'Ideas.Shop', 'script', '1.0.1', 'create_attribute_table.php', '2018-06-21 13:01:46'),
(369, 'Ideas.Shop', 'script', '1.0.1', 'create_product_table.php', '2018-06-21 13:02:04'),
(370, 'Ideas.Shop', 'script', '1.0.1', 'create_order_table.php', '2018-06-21 13:02:19'),
(371, 'Ideas.Shop', 'script', '1.0.1', 'create_tax_ship_table.php', '2018-06-21 13:02:28'),
(372, 'Ideas.Shop', 'script', '1.0.1', 'create_setting_table.php', '2018-06-21 13:02:38'),
(373, 'Ideas.Shop', 'script', '1.0.1', 'create_coupon_table.php', '2018-06-21 13:02:48'),
(374, 'Ideas.Shop', 'script', '1.0.1', 'shop_seeder.php', '2018-06-21 13:02:53'),
(375, 'Ideas.Shop', 'comment', '1.0.1', 'Initialize plugin.', '2018-06-21 13:02:53'),
(376, 'Ideas.Shop', 'comment', '1.0.2', 'fix bug when enable cache', '2018-06-21 13:02:53'),
(377, 'Ideas.Shop', 'comment', '1.0.3', 'fix bug when uninstall plugin', '2018-06-21 13:02:54'),
(378, 'Ideas.Shop', 'comment', '1.0.4', 'remove table config when uninstall plugin', '2018-06-21 13:02:54'),
(379, 'RainLab.Builder', 'comment', '1.0.1', 'Initialize plugin.', '2018-06-25 09:30:04'),
(380, 'RainLab.Builder', 'comment', '1.0.2', 'Fixes the problem with selecting a plugin. Minor localization corrections. Configuration files in the list and form behaviors are now autocomplete.', '2018-06-25 09:30:05'),
(381, 'RainLab.Builder', 'comment', '1.0.3', 'Improved handling of the enum data type.', '2018-06-25 09:30:05'),
(382, 'RainLab.Builder', 'comment', '1.0.4', 'Added user permissions to work with the Builder.', '2018-06-25 09:30:06'),
(383, 'RainLab.Builder', 'comment', '1.0.5', 'Fixed permissions registration.', '2018-06-25 09:30:07'),
(384, 'RainLab.Builder', 'comment', '1.0.6', 'Fixed front-end record ordering in the Record List component.', '2018-06-25 09:30:08'),
(385, 'RainLab.Builder', 'comment', '1.0.7', 'Builder settings are now protected with user permissions. The database table column list is scrollable now. Minor code cleanup.', '2018-06-25 09:30:08'),
(386, 'RainLab.Builder', 'comment', '1.0.8', 'Added the Reorder Controller behavior.', '2018-06-25 09:30:09'),
(387, 'RainLab.Builder', 'comment', '1.0.9', 'Minor API and UI updates.', '2018-06-25 09:30:10'),
(388, 'RainLab.Builder', 'comment', '1.0.10', 'Minor styling update.', '2018-06-25 09:30:10'),
(389, 'RainLab.Builder', 'comment', '1.0.11', 'Fixed a bug where clicking placeholder in a repeater would open Inspector. Fixed a problem with saving forms with repeaters in tabs. Minor style fix.', '2018-06-25 09:30:11'),
(390, 'RainLab.Builder', 'comment', '1.0.12', 'Added support for the Trigger property to the Media Finder widget configuration. Names of form fields and list columns definition files can now contain underscores.', '2018-06-25 09:30:12'),
(391, 'RainLab.Builder', 'comment', '1.0.13', 'Minor styling fix on the database editor.', '2018-06-25 09:30:13'),
(392, 'RainLab.Builder', 'comment', '1.0.14', 'Added support for published_at timestamp field', '2018-06-25 09:30:13'),
(393, 'RainLab.Builder', 'comment', '1.0.15', 'Fixed a bug where saving a localization string in Inspector could cause a JavaScript error. Added support for Timestamps and Soft Deleting for new models.', '2018-06-25 09:30:14'),
(394, 'RainLab.Builder', 'comment', '1.0.16', 'Fixed a bug when saving a form with the Repeater widget in a tab could create invalid fields in the form\'s outside area. Added a check that prevents creating localization strings inside other existing strings.', '2018-06-25 09:30:15'),
(395, 'RainLab.Builder', 'comment', '1.0.17', 'Added support Trigger attribute support for RecordFinder and Repeater form widgets.', '2018-06-25 09:30:15'),
(396, 'RainLab.Builder', 'comment', '1.0.18', 'Fixes a bug where \'::class\' notations in a model class definition could prevent the model from appearing in the Builder model list. Added emptyOption property support to the dropdown form control.', '2018-06-25 09:30:16'),
(397, 'RainLab.Builder', 'comment', '1.0.19', 'Added a feature allowing to add all database columns to a list definition. Added max length validation for database table and column names.', '2018-06-25 09:30:17'),
(398, 'RainLab.Builder', 'comment', '1.0.20', 'Fixes a bug where form the builder could trigger the \"current.hasAttribute is not a function\" error.', '2018-06-25 09:30:17'),
(399, 'RainLab.Builder', 'comment', '1.0.21', 'Back-end navigation sort order updated.', '2018-06-25 09:30:18'),
(400, 'RainLab.Builder', 'comment', '1.0.22', 'Added scopeValue property to the RecordList component.', '2018-06-25 09:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

DROP TABLE IF EXISTS `system_plugin_versions`;
CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_plugin_versions`
--

TRUNCATE TABLE `system_plugin_versions`;
--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'October.Demo', '1.0.1', '2018-04-06 12:39:20', 0, 0),
(2, 'Alexis.Banners', '1.0.11', '2018-04-06 19:01:14', 0, 0),
(3, 'MarcelHaupt.Email', '1.0.42', '2018-04-06 19:01:46', 0, 0),
(4, 'Renatio.FormBuilder', '1.4.2', '2018-04-06 19:02:07', 0, 0),
(5, 'Renatio.SeoManager', '1.1.9', '2018-04-06 19:02:11', 0, 0),
(6, 'RainLab.User', '1.4.5', '2018-04-06 19:15:10', 0, 0),
(7, 'Jiri.JKShop', '1.7.5', '2018-04-06 19:15:59', 0, 0),
(8, 'RainLab.Pages', '1.2.18', '2018-06-21 13:01:36', 0, 0),
(9, 'RainLab.Sitemap', '1.0.8', '2018-06-21 13:01:40', 0, 0),
(10, 'Ideas.Shop', '1.0.4', '2018-06-21 13:02:54', 0, 0),
(11, 'RainLab.Builder', '1.0.22', '2018-06-25 09:30:19', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

DROP TABLE IF EXISTS `system_request_logs`;
CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_request_logs`
--

TRUNCATE TABLE `system_request_logs`;
-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

DROP TABLE IF EXISTS `system_revisions`;
CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_revisions`
--

TRUNCATE TABLE `system_revisions`;
-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `system_settings`
--

TRUNCATE TABLE `system_settings`;
--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'jiri_jkshop_settings', '{\"number_format_thousands_sep\":\",\",\"number_format_decimals\":\"2\",\"number_format_dec_point\":\".\",\"bank_transfer_details_content\":\"\",\"paypal_use_sandbox\":\"1\",\"paypal_debug\":\"1\",\"paypal_business\":\"\",\"invoice_template_content\":\"<p><br>\\r\\n<\\/p><table><colgroup><col><col><col><col><col><col><col><col><\\/colgroup>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\" rowspan=\\\"4\\\">\\r\\n\\t\\t<h2>JK Shop<\\/h2>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\" rowspan=\\\"4\\\">\\r\\n\\t\\t<h2>Invoice: #{{order_id}}<\\/h2>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Supplier:<\\/h3>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Subscriber:<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Company No: 8546131657\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">VAT number: US687465784\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">Company No:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">VAT number:\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">JKShop\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Firm\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Jiri Kubak\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{first_name}}   {{last_name}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Address\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{address}} {{address2}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Postcode City\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{postcode}}   {{city}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Country<br>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{country}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">E mail: xx@yy.com\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Email:   {{email}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Phone: 555 666 222\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Phone:   {{phone}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Bank Details:<\\/h3>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Delivery Adress:<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Account Number:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">568741316\\/2548\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_first_name}}   {{ds_last_name}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Bank name:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">Europa Bank\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_address}} {{ds_address2}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_postcode}} {{ds_city}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_country}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t<h3>Payment   Terms<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Date of Issue:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{date_now}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">No.:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{order_id}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Due Date:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{date_now_14}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\"><br>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Form of Payment:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{payment_method}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\"><br>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t<h3>Products<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t{{products}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">Total   excl. VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_price_without_tax}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_tax}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">Total   incl. VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_price}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\"><br><br><br>Issued: JKShop\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\"><br><br><br>Took Over:\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table><p><br>\\r\\n<\\/p>\",\"cash_on_delivery_order_status_before_id\":\"1\",\"cash_on_delivery_order_status_after_id\":\"6\",\"bank_transfer_order_status_before_id\":\"3\",\"bank_transfer_order_status_after_id\":\"4\",\"paypal_order_status_before_id\":\"2\",\"paypal_order_status_after_id\":\"4\",\"currency_char\":\"$\",\"currency_char_position\":\"1\",\"copy_all_order_emails_to\":\"\",\"invoice_template_style\":\".product-title { width: 315px; display: inline-block; }\\r\\n.product-quantity { width: 50px; display: inline-block; }\\r\\n.product-price-without-tax { width: 100px; display: inline-block; text-align: right; }\\r\\n.product-tax { width: 100px; display: inline-block; text-align: right; }\\r\\n.product-price { width: 130px; display: inline-block; text-align: right; }\\r\\ntable { width: 100%; border-collapse: collapse;}\\r\\ntd, th { border: 1px solid #ccc; }\",\"cash_on_delivery_active\":\"1\",\"bank_transfer_active\":\"1\",\"paypal_active\":\"0\",\"paypal_currency_code\":\"USD\",\"paypal_return_url\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `jkshop_ds_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users_groups`
--

TRUNCATE TABLE `users_groups`;
-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `user_groups`
--

TRUNCATE TABLE `user_groups`;
--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Guest', 'guest', 'Default group for guest users.', '2018-04-06 19:15:00', '2018-04-06 19:15:00'),
(2, 'Registered', 'registered', 'Default group for registered users.', '2018-04-06 19:15:00', '2018-04-06 19:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_throttle`
--

DROP TABLE IF EXISTS `user_throttle`;
CREATE TABLE `user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `user_throttle`
--

TRUNCATE TABLE `user_throttle`;
--
-- Indexes for dumped tables
--

--
-- Indexes for table `alexis_banners_banners`
--
ALTER TABLE `alexis_banners_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alexis_banners_configs`
--
ALTER TABLE `alexis_banners_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alexis_banners_queues`
--
ALTER TABLE `alexis_banners_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_categories`
--
ALTER TABLE `ideas_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_categories_slug_unique` (`slug`);

--
-- Indexes for table `ideas_city`
--
ALTER TABLE `ideas_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_city_geo_zone_id_foreign` (`geo_zone_id`);

--
-- Indexes for table `ideas_config`
--
ALTER TABLE `ideas_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_config_slug_unique` (`slug`);

--
-- Indexes for table `ideas_coupon`
--
ALTER TABLE `ideas_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_coupon_history`
--
ALTER TABLE `ideas_coupon_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_coupon_history_coupon_id_foreign` (`coupon_id`),
  ADD KEY `ideas_coupon_history_order_id_foreign` (`order_id`);

--
-- Indexes for table `ideas_coupon_to_category`
--
ALTER TABLE `ideas_coupon_to_category`
  ADD KEY `ideas_coupon_to_category_coupon_id_foreign` (`coupon_id`),
  ADD KEY `ideas_coupon_to_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `ideas_coupon_to_product`
--
ALTER TABLE `ideas_coupon_to_product`
  ADD KEY `ideas_coupon_to_product_coupon_id_foreign` (`coupon_id`),
  ADD KEY `ideas_coupon_to_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `ideas_currency`
--
ALTER TABLE `ideas_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_document`
--
ALTER TABLE `ideas_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_filter`
--
ALTER TABLE `ideas_filter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_filter_slug_unique` (`slug`);

--
-- Indexes for table `ideas_filter_option`
--
ALTER TABLE `ideas_filter_option`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_filter_option_slug_unique` (`slug`),
  ADD KEY `ideas_filter_option_filter_id_foreign` (`filter_id`);

--
-- Indexes for table `ideas_geo_zone`
--
ALTER TABLE `ideas_geo_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_length`
--
ALTER TABLE `ideas_length`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_order`
--
ALTER TABLE `ideas_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_order_order_status_id_foreign` (`order_status_id`);

--
-- Indexes for table `ideas_order_product`
--
ALTER TABLE `ideas_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_order_product_order_id_foreign` (`order_id`);

--
-- Indexes for table `ideas_order_return`
--
ALTER TABLE `ideas_order_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_order_return_order_id_foreign` (`order_id`),
  ADD KEY `ideas_order_return_product_id_foreign` (`product_id`),
  ADD KEY `ideas_order_return_reason_id_foreign` (`reason_id`);

--
-- Indexes for table `ideas_order_return_reason`
--
ALTER TABLE `ideas_order_return_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_order_status`
--
ALTER TABLE `ideas_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_order_status_change`
--
ALTER TABLE `ideas_order_status_change`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_order_status_change_order_id_foreign` (`order_id`);

--
-- Indexes for table `ideas_product`
--
ALTER TABLE `ideas_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_product_slug_unique` (`slug`);

--
-- Indexes for table `ideas_product_attribute`
--
ALTER TABLE `ideas_product_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_product_attribute_product_id_index` (`product_id`);

--
-- Indexes for table `ideas_product_child_to_category`
--
ALTER TABLE `ideas_product_child_to_category`
  ADD KEY `ideas_product_child_to_category_product_id_foreign` (`product_id`),
  ADD KEY `ideas_product_child_to_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `ideas_product_configurable`
--
ALTER TABLE `ideas_product_configurable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_product_configurable_product_id_foreign` (`product_id`);

--
-- Indexes for table `ideas_product_downloadable`
--
ALTER TABLE `ideas_product_downloadable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_product_downloadable_product_id_foreign` (`product_id`);

--
-- Indexes for table `ideas_product_downloadable_link`
--
ALTER TABLE `ideas_product_downloadable_link`
  ADD KEY `ideas_product_downloadable_link_code_index` (`code`);

--
-- Indexes for table `ideas_product_reviews`
--
ALTER TABLE `ideas_product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `ideas_product_to_category`
--
ALTER TABLE `ideas_product_to_category`
  ADD KEY `ideas_product_to_category_product_id_foreign` (`product_id`),
  ADD KEY `ideas_product_to_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `ideas_product_to_filter_option`
--
ALTER TABLE `ideas_product_to_filter_option`
  ADD KEY `ideas_product_to_filter_option_product_id_foreign` (`product_id`);

--
-- Indexes for table `ideas_routes`
--
ALTER TABLE `ideas_routes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_routes_slug_unique` (`slug`),
  ADD KEY `ideas_routes_entity_id_index` (`entity_id`);

--
-- Indexes for table `ideas_ship_rule`
--
ALTER TABLE `ideas_ship_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_tax_class`
--
ALTER TABLE `ideas_tax_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_tax_rate`
--
ALTER TABLE `ideas_tax_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_tax_rate_geo_zone_id_foreign` (`geo_zone_id`);

--
-- Indexes for table `ideas_tax_rule`
--
ALTER TABLE `ideas_tax_rule`
  ADD KEY `ideas_tax_rule_tax_class_id_foreign` (`tax_class_id`),
  ADD KEY `ideas_tax_rule_tax_rate_id_foreign` (`tax_rate_id`);

--
-- Indexes for table `ideas_theme_config`
--
ALTER TABLE `ideas_theme_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ideas_theme_config_slug_unique` (`slug`);

--
-- Indexes for table `ideas_theme_config_image_detail`
--
ALTER TABLE `ideas_theme_config_image_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_theme_config_image_detail_theme_config_id_foreign` (`theme_config_id`);

--
-- Indexes for table `ideas_theme_config_text_value`
--
ALTER TABLE `ideas_theme_config_text_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ideas_theme_config_text_value_theme_config_id_foreign` (`theme_config_id`);

--
-- Indexes for table `ideas_users_extends`
--
ALTER TABLE `ideas_users_extends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ideas_weight`
--
ALTER TABLE `ideas_weight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_brands`
--
ALTER TABLE `jiri_jkshop_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_carriers`
--
ALTER TABLE `jiri_jkshop_carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_categories`
--
ALTER TABLE `jiri_jkshop_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jiri_jkshop_categories_parent_id_index` (`parent_id`);

--
-- Indexes for table `jiri_jkshop_categories_users_sale`
--
ALTER TABLE `jiri_jkshop_categories_users_sale`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jiri_jkshop_categories_users_sale_category_id_user_id_unique` (`category_id`,`user_id`);

--
-- Indexes for table `jiri_jkshop_coupons`
--
ALTER TABLE `jiri_jkshop_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_coupons_categories`
--
ALTER TABLE `jiri_jkshop_coupons_categories`
  ADD PRIMARY KEY (`coupon_id`,`category_id`),
  ADD KEY `jiri_jkshop_coupons_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `jiri_jkshop_coupons_products`
--
ALTER TABLE `jiri_jkshop_coupons_products`
  ADD PRIMARY KEY (`coupon_id`,`product_id`),
  ADD KEY `jiri_jkshop_coupons_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `jiri_jkshop_coupons_users`
--
ALTER TABLE `jiri_jkshop_coupons_users`
  ADD PRIMARY KEY (`coupon_id`,`user_id`),
  ADD KEY `jiri_jkshop_coupons_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `jiri_jkshop_orders`
--
ALTER TABLE `jiri_jkshop_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jiri_jkshop_orders_coupon_id_foreign` (`coupon_id`),
  ADD KEY `jiri_jkshop_orders_payment_gateway_id_foreign` (`payment_gateway_id`);

--
-- Indexes for table `jiri_jkshop_order_statuses`
--
ALTER TABLE `jiri_jkshop_order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_payment_gateways`
--
ALTER TABLE `jiri_jkshop_payment_gateways`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jiri_jkshop_payment_gateways_order_status_before_id_foreign` (`order_status_before_id`),
  ADD KEY `jiri_jkshop_payment_gateways_order_status_after_id_foreign` (`order_status_after_id`);

--
-- Indexes for table `jiri_jkshop_products`
--
ALTER TABLE `jiri_jkshop_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_products_accessories`
--
ALTER TABLE `jiri_jkshop_products_accessories`
  ADD PRIMARY KEY (`product_id`,`accessory_id`);

--
-- Indexes for table `jiri_jkshop_products_carriers_no`
--
ALTER TABLE `jiri_jkshop_products_carriers_no`
  ADD PRIMARY KEY (`product_id`,`carrier_id`);

--
-- Indexes for table `jiri_jkshop_products_categories`
--
ALTER TABLE `jiri_jkshop_products_categories`
  ADD PRIMARY KEY (`product_id`,`category_id`);

--
-- Indexes for table `jiri_jkshop_products_featured`
--
ALTER TABLE `jiri_jkshop_products_featured`
  ADD PRIMARY KEY (`product_id`,`featured_id`);

--
-- Indexes for table `jiri_jkshop_products_options`
--
ALTER TABLE `jiri_jkshop_products_options`
  ADD PRIMARY KEY (`product_id`,`option_id`),
  ADD KEY `jiri_jkshop_products_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `jiri_jkshop_products_properties`
--
ALTER TABLE `jiri_jkshop_products_properties`
  ADD PRIMARY KEY (`product_id`,`property_id`),
  ADD KEY `jiri_jkshop_products_properties_property_id_foreign` (`property_id`);

--
-- Indexes for table `jiri_jkshop_products_users_price`
--
ALTER TABLE `jiri_jkshop_products_users_price`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jiri_jkshop_products_users_price_product_id_user_id_unique` (`product_id`,`user_id`);

--
-- Indexes for table `jiri_jkshop_properties`
--
ALTER TABLE `jiri_jkshop_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jiri_jkshop_property_options`
--
ALTER TABLE `jiri_jkshop_property_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jiri_jkshop_property_options_property_id_index` (`property_id`);

--
-- Indexes for table `jiri_jkshop_taxes`
--
ALTER TABLE `jiri_jkshop_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `marcelhaupt_email_action`
--
ALTER TABLE `marcelhaupt_email_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcelhaupt_email_action_log`
--
ALTER TABLE `marcelhaupt_email_action_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcelhaupt_email_campaign`
--
ALTER TABLE `marcelhaupt_email_campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcelhaupt_email_condition`
--
ALTER TABLE `marcelhaupt_email_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcelhaupt_email_send_log`
--
ALTER TABLE `marcelhaupt_email_send_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marcelhaupt_email_user_settings`
--
ALTER TABLE `marcelhaupt_email_user_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_sitemap_definitions`
--
ALTER TABLE `rainlab_sitemap_definitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_sitemap_definitions_theme_index` (`theme`);

--
-- Indexes for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_user_mail_blockers_email_index` (`email`),
  ADD KEY `rainlab_user_mail_blockers_template_index` (`template`),
  ADD KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`);

--
-- Indexes for table `renatio_formbuilder_fields`
--
ALTER TABLE `renatio_formbuilder_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_fields_form_id_index` (`form_id`),
  ADD KEY `renatio_formbuilder_fields_field_type_id_index` (`field_type_id`),
  ADD KEY `renatio_formbuilder_fields_parent_id_index` (`parent_id`),
  ADD KEY `renatio_formbuilder_fields_section_id_index` (`section_id`);

--
-- Indexes for table `renatio_formbuilder_field_types`
--
ALTER TABLE `renatio_formbuilder_field_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `renatio_formbuilder_field_types_code_unique` (`code`);

--
-- Indexes for table `renatio_formbuilder_forms`
--
ALTER TABLE `renatio_formbuilder_forms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `renatio_formbuilder_forms_code_unique` (`code`),
  ADD KEY `renatio_formbuilder_forms_template_id_index` (`template_id`);

--
-- Indexes for table `renatio_formbuilder_form_logs`
--
ALTER TABLE `renatio_formbuilder_form_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_form_logs_form_id_index` (`form_id`);

--
-- Indexes for table `renatio_formbuilder_sections`
--
ALTER TABLE `renatio_formbuilder_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_sections_form_id_index` (`form_id`),
  ADD KEY `renatio_formbuilder_sections_sort_order_index` (`sort_order`);

--
-- Indexes for table `renatio_seomanager_seo_tags`
--
ALTER TABLE `renatio_seomanager_seo_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_seomanager_seo_tags_seo_tag_id_seo_tag_type_index` (`seo_tag_id`,`seo_tag_type`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_login_unique` (`username`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`),
  ADD KEY `users_login_index` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_groups_code_index` (`code`);

--
-- Indexes for table `user_throttle`
--
ALTER TABLE `user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_throttle_user_id_index` (`user_id`),
  ADD KEY `user_throttle_ip_address_index` (`ip_address`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alexis_banners_banners`
--
ALTER TABLE `alexis_banners_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alexis_banners_configs`
--
ALTER TABLE `alexis_banners_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alexis_banners_queues`
--
ALTER TABLE `alexis_banners_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_categories`
--
ALTER TABLE `ideas_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ideas_city`
--
ALTER TABLE `ideas_city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_config`
--
ALTER TABLE `ideas_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ideas_coupon`
--
ALTER TABLE `ideas_coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_coupon_history`
--
ALTER TABLE `ideas_coupon_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_currency`
--
ALTER TABLE `ideas_currency`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_document`
--
ALTER TABLE `ideas_document`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ideas_filter`
--
ALTER TABLE `ideas_filter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ideas_filter_option`
--
ALTER TABLE `ideas_filter_option`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ideas_geo_zone`
--
ALTER TABLE `ideas_geo_zone`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_length`
--
ALTER TABLE `ideas_length`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ideas_order`
--
ALTER TABLE `ideas_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_order_product`
--
ALTER TABLE `ideas_order_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_order_return`
--
ALTER TABLE `ideas_order_return`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_order_return_reason`
--
ALTER TABLE `ideas_order_return_reason`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_order_status`
--
ALTER TABLE `ideas_order_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ideas_order_status_change`
--
ALTER TABLE `ideas_order_status_change`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_product`
--
ALTER TABLE `ideas_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ideas_product_attribute`
--
ALTER TABLE `ideas_product_attribute`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ideas_product_configurable`
--
ALTER TABLE `ideas_product_configurable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_product_downloadable`
--
ALTER TABLE `ideas_product_downloadable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_product_reviews`
--
ALTER TABLE `ideas_product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_routes`
--
ALTER TABLE `ideas_routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `ideas_ship_rule`
--
ALTER TABLE `ideas_ship_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ideas_tax_class`
--
ALTER TABLE `ideas_tax_class`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_tax_rate`
--
ALTER TABLE `ideas_tax_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ideas_theme_config`
--
ALTER TABLE `ideas_theme_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ideas_theme_config_image_detail`
--
ALTER TABLE `ideas_theme_config_image_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ideas_theme_config_text_value`
--
ALTER TABLE `ideas_theme_config_text_value`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_users_extends`
--
ALTER TABLE `ideas_users_extends`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ideas_weight`
--
ALTER TABLE `ideas_weight`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jiri_jkshop_brands`
--
ALTER TABLE `jiri_jkshop_brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jiri_jkshop_carriers`
--
ALTER TABLE `jiri_jkshop_carriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jiri_jkshop_categories`
--
ALTER TABLE `jiri_jkshop_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jiri_jkshop_categories_users_sale`
--
ALTER TABLE `jiri_jkshop_categories_users_sale`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_coupons`
--
ALTER TABLE `jiri_jkshop_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_orders`
--
ALTER TABLE `jiri_jkshop_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_order_statuses`
--
ALTER TABLE `jiri_jkshop_order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jiri_jkshop_payment_gateways`
--
ALTER TABLE `jiri_jkshop_payment_gateways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_products`
--
ALTER TABLE `jiri_jkshop_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jiri_jkshop_products_users_price`
--
ALTER TABLE `jiri_jkshop_products_users_price`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_properties`
--
ALTER TABLE `jiri_jkshop_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_property_options`
--
ALTER TABLE `jiri_jkshop_property_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jiri_jkshop_taxes`
--
ALTER TABLE `jiri_jkshop_taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_action`
--
ALTER TABLE `marcelhaupt_email_action`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_action_log`
--
ALTER TABLE `marcelhaupt_email_action_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_campaign`
--
ALTER TABLE `marcelhaupt_email_campaign`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_condition`
--
ALTER TABLE `marcelhaupt_email_condition`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_send_log`
--
ALTER TABLE `marcelhaupt_email_send_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcelhaupt_email_user_settings`
--
ALTER TABLE `marcelhaupt_email_user_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_sitemap_definitions`
--
ALTER TABLE `rainlab_sitemap_definitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_fields`
--
ALTER TABLE `renatio_formbuilder_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_field_types`
--
ALTER TABLE `renatio_formbuilder_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_forms`
--
ALTER TABLE `renatio_formbuilder_forms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_form_logs`
--
ALTER TABLE `renatio_formbuilder_form_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_sections`
--
ALTER TABLE `renatio_formbuilder_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_seomanager_seo_tags`
--
ALTER TABLE `renatio_seomanager_seo_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_throttle`
--
ALTER TABLE `user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ideas_city`
--
ALTER TABLE `ideas_city`
  ADD CONSTRAINT `ideas_city_geo_zone_id_foreign` FOREIGN KEY (`geo_zone_id`) REFERENCES `ideas_geo_zone` (`id`);

--
-- Constraints for table `ideas_coupon_history`
--
ALTER TABLE `ideas_coupon_history`
  ADD CONSTRAINT `ideas_coupon_history_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `ideas_coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ideas_coupon_history_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ideas_order` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_coupon_to_category`
--
ALTER TABLE `ideas_coupon_to_category`
  ADD CONSTRAINT `ideas_coupon_to_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ideas_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ideas_coupon_to_category_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `ideas_coupon` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_coupon_to_product`
--
ALTER TABLE `ideas_coupon_to_product`
  ADD CONSTRAINT `ideas_coupon_to_product_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `ideas_coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ideas_coupon_to_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_filter_option`
--
ALTER TABLE `ideas_filter_option`
  ADD CONSTRAINT `ideas_filter_option_filter_id_foreign` FOREIGN KEY (`filter_id`) REFERENCES `ideas_filter` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_order`
--
ALTER TABLE `ideas_order`
  ADD CONSTRAINT `ideas_order_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `ideas_order_status` (`id`);

--
-- Constraints for table `ideas_order_product`
--
ALTER TABLE `ideas_order_product`
  ADD CONSTRAINT `ideas_order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ideas_order` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_order_return`
--
ALTER TABLE `ideas_order_return`
  ADD CONSTRAINT `ideas_order_return_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ideas_order` (`id`),
  ADD CONSTRAINT `ideas_order_return_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`),
  ADD CONSTRAINT `ideas_order_return_reason_id_foreign` FOREIGN KEY (`reason_id`) REFERENCES `ideas_order_return_reason` (`id`);

--
-- Constraints for table `ideas_order_status_change`
--
ALTER TABLE `ideas_order_status_change`
  ADD CONSTRAINT `ideas_order_status_change_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ideas_order` (`id`);

--
-- Constraints for table `ideas_product_attribute`
--
ALTER TABLE `ideas_product_attribute`
  ADD CONSTRAINT `ideas_product_attribute_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_child_to_category`
--
ALTER TABLE `ideas_product_child_to_category`
  ADD CONSTRAINT `ideas_product_child_to_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ideas_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ideas_product_child_to_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_configurable`
--
ALTER TABLE `ideas_product_configurable`
  ADD CONSTRAINT `ideas_product_configurable_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_downloadable`
--
ALTER TABLE `ideas_product_downloadable`
  ADD CONSTRAINT `ideas_product_downloadable_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_reviews`
--
ALTER TABLE `ideas_product_reviews`
  ADD CONSTRAINT `ideas_product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_to_category`
--
ALTER TABLE `ideas_product_to_category`
  ADD CONSTRAINT `ideas_product_to_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ideas_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ideas_product_to_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_product_to_filter_option`
--
ALTER TABLE `ideas_product_to_filter_option`
  ADD CONSTRAINT `ideas_product_to_filter_option_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ideas_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ideas_tax_rate`
--
ALTER TABLE `ideas_tax_rate`
  ADD CONSTRAINT `ideas_tax_rate_geo_zone_id_foreign` FOREIGN KEY (`geo_zone_id`) REFERENCES `ideas_geo_zone` (`id`);

--
-- Constraints for table `ideas_tax_rule`
--
ALTER TABLE `ideas_tax_rule`
  ADD CONSTRAINT `ideas_tax_rule_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `ideas_tax_class` (`id`),
  ADD CONSTRAINT `ideas_tax_rule_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `ideas_tax_rate` (`id`);

--
-- Constraints for table `ideas_theme_config_image_detail`
--
ALTER TABLE `ideas_theme_config_image_detail`
  ADD CONSTRAINT `ideas_theme_config_image_detail_theme_config_id_foreign` FOREIGN KEY (`theme_config_id`) REFERENCES `ideas_theme_config` (`id`);

--
-- Constraints for table `ideas_theme_config_text_value`
--
ALTER TABLE `ideas_theme_config_text_value`
  ADD CONSTRAINT `ideas_theme_config_text_value_theme_config_id_foreign` FOREIGN KEY (`theme_config_id`) REFERENCES `ideas_theme_config` (`id`);

--
-- Constraints for table `jiri_jkshop_orders`
--
ALTER TABLE `jiri_jkshop_orders`
  ADD CONSTRAINT `jiri_jkshop_orders_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `jiri_jkshop_coupons` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jiri_jkshop_orders_payment_gateway_id_foreign` FOREIGN KEY (`payment_gateway_id`) REFERENCES `jiri_jkshop_payment_gateways` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `jiri_jkshop_payment_gateways`
--
ALTER TABLE `jiri_jkshop_payment_gateways`
  ADD CONSTRAINT `jiri_jkshop_payment_gateways_order_status_after_id_foreign` FOREIGN KEY (`order_status_after_id`) REFERENCES `jiri_jkshop_order_statuses` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jiri_jkshop_payment_gateways_order_status_before_id_foreign` FOREIGN KEY (`order_status_before_id`) REFERENCES `jiri_jkshop_order_statuses` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `jiri_jkshop_property_options`
--
ALTER TABLE `jiri_jkshop_property_options`
  ADD CONSTRAINT `jiri_jkshop_property_options_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `jiri_jkshop_properties` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
