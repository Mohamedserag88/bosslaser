var filter_data = [];
(function($){
  $('#show-canvase').on('click', function(e){
    e.preventDefault();
    if($('body.canvas-off').length){
      $('body').removeClass('canvas-off');
    }else {
      $('body').addClass('canvas-off');
    }
  });


  $('.filter').on('click', function(e){
    e.preventDefault();
    if($(this).hasClass('active')){
      $(this).removeClass('active');
    }else {
      $(this).addClass('active');
    }
    update_filter_products();
  });
})(jQuery);

$.fn.extend({
  hasClasses: function (selectors) {
    var self = this;
    for (var i in selectors) {
        if ($(self).hasClass(selectors[i]))
            return true;
    }
    return false;
  }
});

function update_filter_products(){
  (function($){
    var i = 0;
    $filter = $('.filter.active');
    if($filter.length){
      $filter.each(function(index, element){
        filter_data[i] = $(element).data('filter-query');
        i++;
      });
      var data = filter_data.join(',');
      $('.product').each(function(index, element1){
        if($(element1).hasClasses(filter_data)){
          $(element1).removeClass('hidden');
        }else{
          $(element1).addClass('hidden');
        }
      });
      // $('.product').hasClass(filter_data.join(',')).removeClass('hidden');
      // $('.product').not(filter_data.join(',')).addClass('hidden');
    }else{
      filter_data = [];
    }
  })(jQuery);
}
