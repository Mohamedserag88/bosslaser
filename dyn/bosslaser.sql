-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 07, 2018 at 07:23 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bosslaser`
--

-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_banners`
--

DROP TABLE IF EXISTS `alexis_banners_banners`;
CREATE TABLE IF NOT EXISTS `alexis_banners_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `looks` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `banner_code` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show` int(11) NOT NULL DEFAULT '0',
  `dummy` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `percents` int(11) NOT NULL DEFAULT '100',
  `config_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `dummy_type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_configs`
--

DROP TABLE IF EXISTS `alexis_banners_configs`;
CREATE TABLE IF NOT EXISTS `alexis_banners_configs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alexis_banners_queues`
--

DROP TABLE IF EXISTS `alexis_banners_queues`;
CREATE TABLE IF NOT EXISTS `alexis_banners_queues` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `config_id` int(11) NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

DROP TABLE IF EXISTS `backend_access_log`;
CREATE TABLE IF NOT EXISTS `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-04-08 01:20:52', '2018-04-08 01:20:52'),
(2, 1, '::1', '2018-04-08 01:49:31', '2018-04-08 01:49:31'),
(3, 1, '::1', '2018-04-08 02:18:11', '2018-04-08 02:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

DROP TABLE IF EXISTS `backend_users`;
CREATE TABLE IF NOT EXISTS `backend_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `act_code_index` (`activation_code`),
  KEY `reset_code_index` (`reset_password_code`),
  KEY `admin_role_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
(1, 'Mostafa', 'Hussein', 'Admin', 'mostafa.mohamed.86@gmail.com', '$2y$10$XDzWa6LYYYacQILaAHCJ4uoqapinUjYJ.O0bKQ/nkur3gXxdoAgO6', NULL, '$2y$10$C0ZwAlSrTRfq/bOGkqaJyu954yFBMns3Bb70VWkzuHlH8jOijJGxq', NULL, '', 1, 2, NULL, '2018-04-08 02:18:08', '2018-04-08 01:20:24', '2018-04-08 02:18:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

DROP TABLE IF EXISTS `backend_users_groups`;
CREATE TABLE IF NOT EXISTS `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

DROP TABLE IF EXISTS `backend_user_groups`;
CREATE TABLE IF NOT EXISTS `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2018-04-08 01:20:24', '2018-04-08 01:20:24', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

DROP TABLE IF EXISTS `backend_user_preferences`;
CREATE TABLE IF NOT EXISTS `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

DROP TABLE IF EXISTS `backend_user_roles`;
CREATE TABLE IF NOT EXISTS `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_unique` (`name`),
  KEY `role_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2018-04-08 01:20:24', '2018-04-08 01:20:24'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2018-04-08 01:20:24', '2018-04-08 01:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

DROP TABLE IF EXISTS `backend_user_throttle`;
CREATE TABLE IF NOT EXISTS `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backend_user_throttle_user_id_index` (`user_id`),
  KEY `backend_user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

DROP TABLE IF EXISTS `cms_theme_data`;
CREATE TABLE IF NOT EXISTS `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_data_theme_index` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

DROP TABLE IF EXISTS `cms_theme_logs`;
CREATE TABLE IF NOT EXISTS `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_theme_logs_type_index` (`type`),
  KEY `cms_theme_logs_theme_index` (`theme`),
  KEY `cms_theme_logs_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

DROP TABLE IF EXISTS `deferred_bindings`;
CREATE TABLE IF NOT EXISTS `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deferred_bindings_master_type_index` (`master_type`),
  KEY `deferred_bindings_master_field_index` (`master_field`),
  KEY `deferred_bindings_slave_type_index` (`slave_type`),
  KEY `deferred_bindings_slave_id_index` (`slave_id`),
  KEY `deferred_bindings_session_key_index` (`session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_brands`
--

DROP TABLE IF EXISTS `jiri_jkshop_brands`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_carriers`
--

DROP TABLE IF EXISTS `jiri_jkshop_carriers`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_carriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `transit_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speed_grade` int(11) DEFAULT NULL,
  `tracking_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `free_shipping` tinyint(1) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `billing` int(11) DEFAULT NULL,
  `billing_total_price` text COLLATE utf8mb4_unicode_ci,
  `billing_weight` text COLLATE utf8mb4_unicode_ci,
  `maximum_package_width` double DEFAULT NULL,
  `maximum_package_height` double DEFAULT NULL,
  `maximum_package_depth` double DEFAULT NULL,
  `maximum_package_weight` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jiri_jkshop_carriers`
--

INSERT INTO `jiri_jkshop_carriers` (`id`, `created_at`, `updated_at`, `title`, `active`, `transit_time`, `speed_grade`, `tracking_url`, `free_shipping`, `tax_id`, `billing`, `billing_total_price`, `billing_weight`, `maximum_package_width`, `maximum_package_height`, `maximum_package_depth`, `maximum_package_weight`) VALUES
(1, NULL, NULL, 'Carrier', 1, '2 days', 0, 'http://exampl.com/track.php?num=@', 0, 1, 2, '{\\\"1\\\":{\\\"from\\\":\\\"0\\\",\\\"to\\\":\\\"999999999999\\\",\\\"price\\\":\\\"11\\\"}}', '{\\\"1\\\":{\\\"from\\\":\\\"0\\\",\\\"to\\\":\\\"9999\\\",\\\"price\\\":\\\"3.99\\\"}}', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_categories`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) NOT NULL DEFAULT '0',
  `nest_right` int(11) NOT NULL DEFAULT '0',
  `nest_depth` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `jiri_jkshop_categories_parent_id_index` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_categories_users_sale`
--

DROP TABLE IF EXISTS `jiri_jkshop_categories_users_sale`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_categories_users_sale` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sale` double UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jiri_jkshop_categories_users_sale_category_id_user_id_unique` (`category_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_coupons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valid_from` timestamp NULL DEFAULT NULL,
  `valid_to` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `used_count` int(11) DEFAULT NULL,
  `minimum_value_basket` double DEFAULT NULL,
  `type_value` int(11) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_categories`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_coupons_categories` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`),
  KEY `jiri_jkshop_coupons_categories_category_id_foreign` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_products`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_products`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_coupons_products` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`product_id`),
  KEY `jiri_jkshop_coupons_products_product_id_foreign` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_coupons_users`
--

DROP TABLE IF EXISTS `jiri_jkshop_coupons_users`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_coupons_users` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`user_id`),
  KEY `jiri_jkshop_coupons_users_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_orders`
--

DROP TABLE IF EXISTS `jiri_jkshop_orders`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `orderstatus_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ds_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ds_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_id` int(10) UNSIGNED DEFAULT NULL,
  `products_json` text COLLATE utf8mb4_unicode_ci,
  `total_price_without_tax` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `shipping_price_without_tax` double DEFAULT NULL,
  `shipping_tax` double DEFAULT NULL,
  `shipping_price` double DEFAULT NULL,
  `paid_date` timestamp NULL DEFAULT NULL,
  `paid_detail` text COLLATE utf8mb4_unicode_ci,
  `payment_method` int(10) UNSIGNED DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `ds_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_global_discount` double DEFAULT NULL,
  `coupon_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_gateway_id` int(10) UNSIGNED DEFAULT NULL,
  `security_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jiri_jkshop_orders_coupon_id_foreign` (`coupon_id`),
  KEY `jiri_jkshop_orders_payment_gateway_id_foreign` (`payment_gateway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_order_statuses`
--

DROP TABLE IF EXISTS `jiri_jkshop_order_statuses`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_order_statuses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `send_email_to_customer` tinyint(1) DEFAULT NULL,
  `attach_invoice_pdf_to_email` tinyint(1) DEFAULT NULL,
  `mail_template_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty_increase_back` tinyint(1) NOT NULL DEFAULT '0',
  `qty_decrease` tinyint(1) NOT NULL DEFAULT '0',
  `disallow_for_gateway` tinyint(1) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jiri_jkshop_order_statuses`
--

INSERT INTO `jiri_jkshop_order_statuses` (`id`, `created_at`, `updated_at`, `title`, `color`, `active`, `send_email_to_customer`, `attach_invoice_pdf_to_email`, `mail_template_id`, `qty_increase_back`, `qty_decrease`, `disallow_for_gateway`, `paid`) VALUES
(1, NULL, NULL, 'New Order - Cash on Delivery', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(2, NULL, NULL, 'New Order - PayPal', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(3, NULL, NULL, 'New Order - Bank transfer', '#f1c40f', 1, 1, 0, NULL, 0, 0, 0, 0),
(4, NULL, NULL, 'Payment Received', '#9b59b6', 1, 1, 0, NULL, 0, 0, 0, 0),
(5, NULL, NULL, 'Cancel Order', '#c0392b', 1, 1, 0, NULL, 0, 0, 0, 0),
(6, NULL, NULL, 'Expedited Order', '#2ecc71', 1, 1, 1, NULL, 0, 0, 0, 0),
(7, NULL, NULL, 'Expedited Order - Cash on Delivery', '#27ae60', 1, 1, 1, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_payment_gateways`
--

DROP TABLE IF EXISTS `jiri_jkshop_payment_gateways`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_payment_gateways` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_status_before_id` int(10) UNSIGNED DEFAULT NULL,
  `order_status_after_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `gateway` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway_currency` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_page` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `jiri_jkshop_payment_gateways_order_status_before_id_foreign` (`order_status_before_id`),
  KEY `jiri_jkshop_payment_gateways_order_status_after_id_foreign` (`order_status_after_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products`
--

DROP TABLE IF EXISTS `jiri_jkshop_products`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ean_13` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `on_sale` tinyint(1) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT NULL,
  `available_for_order` tinyint(1) DEFAULT NULL,
  `show_price` tinyint(1) DEFAULT NULL,
  `condition` int(11) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `brand_id` int(11) DEFAULT NULL,
  `pre_tax_wholesale_price` double DEFAULT NULL,
  `pre_tax_retail_price` double DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `retail_price_with_tax` double DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `default_category_id` int(11) DEFAULT NULL,
  `package_width` double DEFAULT NULL,
  `package_height` double DEFAULT NULL,
  `package_depth` double DEFAULT NULL,
  `package_weight` double DEFAULT NULL,
  `additional_shipping_fees` double DEFAULT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `when_out_of_stock` int(11) DEFAULT NULL,
  `minimum_quantity` int(10) UNSIGNED NOT NULL,
  `availability_date` timestamp NULL DEFAULT NULL,
  `customization` text COLLATE utf8mb4_unicode_ci,
  `virtual_product` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_accessories`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_accessories`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_accessories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `accessory_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`accessory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_carriers_no`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_carriers_no`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_carriers_no` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `carrier_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`carrier_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_categories`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_categories`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_categories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_featured`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_featured`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_featured` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `featured_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`featured_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_options`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_options`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_options` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price_difference_with_tax` double DEFAULT NULL,
  `extended_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`option_id`),
  KEY `jiri_jkshop_products_options_option_id_foreign` (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_properties`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_properties`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_properties` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`product_id`,`property_id`),
  KEY `jiri_jkshop_products_properties_property_id_foreign` (`property_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_products_users_price`
--

DROP TABLE IF EXISTS `jiri_jkshop_products_users_price`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_products_users_price` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `price` double UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jiri_jkshop_products_users_price_product_id_user_id_unique` (`product_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_properties`
--

DROP TABLE IF EXISTS `jiri_jkshop_properties`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_properties` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_property_options`
--

DROP TABLE IF EXISTS `jiri_jkshop_property_options`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_property_options` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_index` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jiri_jkshop_property_options_property_id_index` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jiri_jkshop_taxes`
--

DROP TABLE IF EXISTS `jiri_jkshop_taxes`;
CREATE TABLE IF NOT EXISTS `jiri_jkshop_taxes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `percent` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jiri_jkshop_taxes`
--

INSERT INTO `jiri_jkshop_taxes` (`id`, `created_at`, `updated_at`, `name`, `active`, `percent`) VALUES
(1, NULL, NULL, 'Default DPH', 1, 21),
(2, NULL, NULL, 'Reduced DPH', 1, 15);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_action`
--

DROP TABLE IF EXISTS `marcelhaupt_email_action`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_action` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `redirect_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_action_log`
--

DROP TABLE IF EXISTS `marcelhaupt_email_action_log`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_action_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_triggered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_campaign`
--

DROP TABLE IF EXISTS `marcelhaupt_email_campaign`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_campaign` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `emails_sent` int(11) NOT NULL DEFAULT '0',
  `emails_opened` int(11) NOT NULL DEFAULT '0',
  `emails_clicked` int(11) NOT NULL DEFAULT '0',
  `incl_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'all',
  `excl_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'any',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_unsubscribed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_condition`
--

DROP TABLE IF EXISTS `marcelhaupt_email_condition`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_condition` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `property` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_send_log`
--

DROP TABLE IF EXISTS `marcelhaupt_email_send_log`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_send_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `pixel_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_opened` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marcelhaupt_email_user_settings`
--

DROP TABLE IF EXISTS `marcelhaupt_email_user_settings`;
CREATE TABLE IF NOT EXISTS `marcelhaupt_email_user_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT '1',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2013_10_01_000001_Db_Backend_Users', 2),
(26, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(27, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(28, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(29, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(30, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(31, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(32, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(33, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(34, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(35, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(36, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(37, '2017_10_01_000003_Db_Cms_Theme_Logs', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_sitemap_definitions`
--

DROP TABLE IF EXISTS `rainlab_sitemap_definitions`;
CREATE TABLE IF NOT EXISTS `rainlab_sitemap_definitions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_sitemap_definitions_theme_index` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_user_mail_blockers`
--

DROP TABLE IF EXISTS `rainlab_user_mail_blockers`;
CREATE TABLE IF NOT EXISTS `rainlab_user_mail_blockers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_user_mail_blockers_email_index` (`email`),
  KEY `rainlab_user_mail_blockers_template_index` (`template`),
  KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_fields`
--

DROP TABLE IF EXISTS `renatio_formbuilder_fields`;
CREATE TABLE IF NOT EXISTS `renatio_formbuilder_fields` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `field_type_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation_messages` text COLLATE utf8mb4_unicode_ci,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_attributes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `renatio_formbuilder_fields_form_id_index` (`form_id`),
  KEY `renatio_formbuilder_fields_field_type_id_index` (`field_type_id`),
  KEY `renatio_formbuilder_fields_parent_id_index` (`parent_id`),
  KEY `renatio_formbuilder_fields_section_id_index` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `renatio_formbuilder_fields`
--

INSERT INTO `renatio_formbuilder_fields` (`id`, `form_id`, `section_id`, `field_type_id`, `label`, `name`, `default`, `validation`, `validation_messages`, `comment`, `class`, `wrapper_class`, `placeholder`, `custom_attributes`, `options`, `is_visible`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 1, 'Name', 'name', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 2, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(2, 1, NULL, 1, 'Subject', 'subject', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 3, 4, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(3, 1, NULL, 1, 'E-mail', 'email', NULL, 'required|email', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, 6, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(4, 1, NULL, 1, 'Phone', 'phone', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 7, 8, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(5, 1, NULL, 2, 'Message', 'content_message', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 9, 10, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(6, 1, NULL, 7, 'reCaptcha', 'g-recaptcha-response', NULL, 'required|recaptcha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 11, 12, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(7, 1, NULL, 8, 'Send', 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 13, 14, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(8, 2, NULL, 1, 'Text', 'text', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 15, 16, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(9, 2, NULL, 3, 'Dropdown', 'dropdown', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 17, 18, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(10, 2, NULL, 4, 'Checkbox', 'checkbox', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 19, 20, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(11, 2, NULL, 5, 'Checkbox list', 'checkbox_list', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 21, 22, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(12, 2, NULL, 6, 'Radio list', 'radio_list', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, '{\"1\":{\"o_key\":\"option_1\",\"o_label\":\"Option 1\"},\"2\":{\"o_key\":\"option_2\",\"o_label\":\"Option 2\"}}', 1, NULL, 23, 24, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(13, 2, NULL, 10, 'Country select', 'country', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, NULL, 1, NULL, 25, 26, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(14, 2, NULL, 11, 'State select', 'state', NULL, 'required', NULL, NULL, NULL, NULL, '-- choose --', NULL, NULL, 1, NULL, 27, 28, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(15, 2, NULL, 2, 'Textarea', 'textarea', NULL, 'required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 29, 30, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(16, 2, NULL, 9, 'File uploader', 'files', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 31, 32, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(17, 2, NULL, 7, 'reCaptcha', 'g-recaptcha-response', NULL, 'required|recaptcha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 33, 34, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(18, 2, NULL, 8, 'Send', 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 35, 36, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_field_types`
--

DROP TABLE IF EXISTS `renatio_formbuilder_field_types`;
CREATE TABLE IF NOT EXISTS `renatio_formbuilder_field_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `markup` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `renatio_formbuilder_field_types_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `renatio_formbuilder_field_types`
--

INSERT INTO `renatio_formbuilder_field_types` (`id`, `name`, `description`, `code`, `markup`, `created_at`, `updated_at`) VALUES
(1, 'Text', 'Renders a single line text box.', 'text', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <input type=\"text\"\n           name=\"{{ name }}\"\n           value=\"{{ default }}\"\n           class=\"{{ class }} form-control\"\n           placeholder=\"{{ placeholder }}\"\n           {{ custom_attributes|raw }}\n    >\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(2, 'Textarea', 'Renders a multiline text box.', 'textarea', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <textarea name=\"{{ name }}\"\n              class=\"{{ class }} form-control\"\n              placeholder=\"{{ placeholder }}\"\n              {{ custom_attributes|raw }}\n    >{{ default }}</textarea>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(3, 'Dropdown', 'Renders a dropdown with specified options.', 'dropdown', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    <select class=\"{{ class }} form-control\" name=\"{{ name }}\" {{ custom_attributes|raw }}>\n\n        {% if placeholder %}\n\n            <option value=\"\">{{ placeholder }}</option>\n\n        {% endif %}\n\n        {% for option in options %}\n\n            <option value=\"{{ option.o_key }}\" {{ option.o_key == default ? \'selected\' : \'\' }}>{{ option.o_label }}</option>\n\n        {% endfor %}\n\n    </select>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(4, 'Checkbox', 'Renders a single checkbox.', 'checkbox', '<div class=\"{{ wrapper_class }} checkbox\">\n\n    <label>\n        <input {{ default ? \'checked\' : \'\' }}\n            name=\"{{ name }}\"\n            class=\"{{ class }}\"\n            value=\"1\"\n            type=\"checkbox\"\n            {{ custom_attributes|raw }}\n        >\n        {{ label }}\n    </label>\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(5, 'Checkbox List', 'Renders a list of checkboxes.', 'checkbox_list', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label class=\"btn-block\" for=\"{{ name }}\">{{ label }}</label>\n\n    {% set values = default|split(\'|\') %}\n    {% for option in options %}\n\n        <label class=\"checkbox-inline\">\n            <input {{ values[loop.index0] == option.o_key ? \'checked\' : \'\' }}\n                type=\"checkbox\"\n                name=\"{{ name }}[]\"\n                class=\"{{ class }}\"\n                id=\"{{ option.o_label }}\"\n                value=\"{{ option.o_key }}\"\n                {{ custom_attributes|raw }}\n            >\n            {{ option.o_label }}\n        </label>\n\n    {% endfor %}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(6, 'Radio List', 'Renders a list of radio options, where only one item can be selected at a time.', 'radio_list', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label class=\"btn-block\" for=\"{{ name }}\">{{ label }}</label>\n\n    {% for option in options %}\n\n        <label class=\"radio-inline\">\n            <input {{ default == option.o_key ? \'checked\' : \'\' }}\n                type=\"radio\"\n                name=\"{{ name }}\"\n                class=\"{{ class }}\"\n                id=\"{{ option.o_label }}\"\n                value=\"{{ option.o_key }}\"\n                {{ custom_attributes|raw }}\n            >\n            {{ option.o_label }}\n        </label>\n\n    {% endfor %}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:46'),
(7, 'ReCaptcha', 'Renders a reCaptcha box.', 'recaptcha', '<div class=\"{{ wrapper_class }} form-group\">\n    <div class=\"g-recaptcha\" data-sitekey=\"{{ settings.site_key }}\" data-theme=\"{{ settings.theme }}\"></div>\n    <script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api.js?hl={{ settings.lang }}\"></script>\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n</div>', '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(8, 'Submit', 'Renders a submit button.', 'submit', '<span class=\"{{ wrapper_class }}\">\n    <button type=\"submit\" class=\"{{ class }} btn btn-primary\" {{ custom_attributes|raw }}>{{ label }}</button>\n</span>', '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(9, 'File uploader', 'Renders a file uploader for regular files.', 'file_uploader', '<div class=\"control-multi-file-uploader form-group\"\n     data-handler=\"{{ __SELF__ ~ \'::onUpdateFile\' }}\"\n     data-control=\"multi-file-uploader\"\n     data-max-size=\"{{ fileConfig.maxSize }}\"\n     data-file-types=\"{{ fileConfig.fileTypes|join(\', \') }}\">\n\n    <div class=\"clickable\"></div>\n    <div class=\"content\">\n        <p class=\"placeholder\">{{ fileConfig.placeholder }}</p>\n    </div>\n\n    <div class=\"template\" style=\"display: none\">\n        <div class=\"dz-preview dz-file-preview\">\n            <img class=\"thumbnail\" src=\"{{ \'plugins/renatio/formbuilder/assets/images/upload.png\'|app }}\"\n                 data-dz-thumbnail/>\n\n            <div class=\"dz-details\">\n                <div class=\"dz-filename\"><span data-dz-name></span></div>\n                <div class=\"dz-size\" data-dz-size></div>\n                <div class=\"activity\">\n                    <div class=\"loading\"></div>\n                </div>\n            </div>\n            <div class=\"action-panel\">\n                <a href=\"javascript:;\" class=\"delete\">&times;</a>\n            </div>\n            <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n\n            <div class=\"dz-error-message\">\n                <div class=\"dz-error-mark\">&cross;</div>\n                <span data-dz-errormessage></span>\n            </div>\n        </div>\n    </div>\n\n</div>', '2018-04-08 01:26:40', '2018-04-08 01:26:40'),
(10, 'Country select', 'Renders a dropdown with country options.', 'country_select', '<div class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    {{ form_select_country(name, null,\n    {\n        class: class ~ \' form-control\',\n        emptyOption: placeholder,\n        \'data-request\': \'onChangeCountry\',\n        \'data-request-success\': \'updateStateDropdown(data)\'\n    }) }}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:41', '2018-04-08 01:26:41'),
(11, 'State select', 'Renders a dropdown with state options.', 'state_select', '<div id=\"country-state\" class=\"{{ wrapper_class }} form-group\">\n\n    <label for=\"{{ name }}\">{{ label }}</label>\n\n    {{ form_select_state(name, null, null,\n    {\n        class: class ~ \' form-control\',\n        emptyOption: placeholder\n    }) }}\n\n    {% if comment %}<p class=\"help-block\">{{ comment }}</p>{% endif %}\n\n</div>', '2018-04-08 01:26:41', '2018-04-08 01:26:41'),
(12, 'Section', 'Renders a section with assigned fields.', 'section', '{{ wrapper_begin|raw }}\n\n    <fieldset class=\"{{ class }}\" id=\"{{ name }}\">\n        {% if label %}<legend>{{ label }}</legend>{% endif %}\n        {{ fields|raw }}\n    </fieldset>\n\n{{ wrapper_end|raw }}', '2018-04-08 01:26:44', '2018-04-08 01:26:44');

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_forms`
--

DROP TABLE IF EXISTS `renatio_formbuilder_forms`;
CREATE TABLE IF NOT EXISTS `renatio_formbuilder_forms` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `template_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `from_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8mb4_unicode_ci,
  `error_message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_email_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `renatio_formbuilder_forms_code_unique` (`code`),
  KEY `renatio_formbuilder_forms_template_id_index` (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `renatio_formbuilder_forms`
--

INSERT INTO `renatio_formbuilder_forms` (`id`, `template_id`, `name`, `code`, `description`, `from_email`, `from_name`, `to_email`, `to_name`, `bcc_email`, `bcc_name`, `success_message`, `error_message`, `created_at`, `updated_at`, `css_class`, `reply_email`, `reply_name`, `response_email_field`) VALUES
(1, 1, 'Contact Form', 'renatio.formbuilder::contact', 'Renders contact form.', NULL, NULL, 'mostafa.mohamed.86@gmail.com', 'Mostafa Hussein', NULL, NULL, 'Message was sent successfully!', 'There was an error. Please try again!', '2018-04-08 01:26:39', '2018-04-08 01:26:39', NULL, NULL, NULL, NULL),
(2, 2, 'Default Form', 'renatio.formbuilder::default', 'Renders default form with all available system fields.', NULL, NULL, 'mostafa.mohamed.86@gmail.com', 'Mostafa Hussein', NULL, NULL, 'Message was sent successfully!', 'There was an error. Please try again!', '2018-04-08 01:26:39', '2018-04-08 01:26:39', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_form_logs`
--

DROP TABLE IF EXISTS `renatio_formbuilder_form_logs`;
CREATE TABLE IF NOT EXISTS `renatio_formbuilder_form_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `form_id` int(10) UNSIGNED NOT NULL,
  `form_data` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `renatio_formbuilder_form_logs_form_id_index` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_sections`
--

DROP TABLE IF EXISTS `renatio_formbuilder_sections`;
CREATE TABLE IF NOT EXISTS `renatio_formbuilder_sections` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `sort_order` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_begin` text COLLATE utf8mb4_unicode_ci,
  `wrapper_end` text COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `renatio_formbuilder_sections_form_id_index` (`form_id`),
  KEY `renatio_formbuilder_sections_sort_order_index` (`sort_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_seomanager_seo_tags`
--

DROP TABLE IF EXISTS `renatio_seomanager_seo_tags`;
CREATE TABLE IF NOT EXISTS `renatio_seomanager_seo_tags` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `seo_tag_id` int(10) UNSIGNED DEFAULT NULL,
  `seo_tag_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `robot_index` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'index',
  `robot_follow` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'follow',
  `robot_advanced` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `renatio_seomanager_seo_tags_seo_tag_id_seo_tag_type_index` (`seo_tag_id`,`seo_tag_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

DROP TABLE IF EXISTS `system_event_logs`;
CREATE TABLE IF NOT EXISTS `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_event_logs_level_index` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

DROP TABLE IF EXISTS `system_files`;
CREATE TABLE IF NOT EXISTS `system_files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_files_field_index` (`field`),
  KEY `system_files_attachment_id_index` (`attachment_id`),
  KEY `system_files_attachment_type_index` (`attachment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

DROP TABLE IF EXISTS `system_mail_layouts`;
CREATE TABLE IF NOT EXISTS `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-04-08 01:20:24', '2018-04-08 01:20:24'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-04-08 01:20:24', '2018-04-08 01:20:24'),
(3, 'Form Builder Default Layout', 'form_builder', '<!doctype html>\n<html>\n<head>\n    <meta name=\"viewport\" content=\"width=device-width\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <title>Renatio FormBuilder</title>\n    <style>\n        {{ css|raw }}\n    </style>\n</head>\n<body class=\"\">\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">\n    <tr>\n        <td>&nbsp;</td>\n        <td class=\"container\">\n            <div class=\"content\">\n\n                {{ content|raw }}\n\n                <!-- START FOOTER -->\n                <div class=\"footer\">\n                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                        <tr>\n                            <td class=\"content-block powered-by\">\n                                Powered by <a href=\"https://renatio.com\">Renatio</a>.\n                            </td>\n                        </tr>\n                    </table>\n                </div>\n\n                <!-- END FOOTER -->\n\n                <!-- END CENTERED WHITE CONTAINER --></div>\n        </td>\n        <td>&nbsp;</td>\n    </tr>\n</table>\n</body>\n</html>', '{{ content }}', '/* -------------------------------------\n    GLOBAL RESETS\n------------------------------------- */\nimg {\n    border: none;\n    -ms-interpolation-mode: bicubic;\n    max-width: 100%;\n}\n\nbody {\n    background-color: #f6f6f6;\n    font-family: sans-serif;\n    -webkit-font-smoothing: antialiased;\n    font-size: 14px;\n    line-height: 1.4;\n    margin: 0;\n    padding: 0;\n    -ms-text-size-adjust: 100%;\n    -webkit-text-size-adjust: 100%;\n}\n\ntable {\n    border-collapse: separate;\n    mso-table-lspace: 0pt;\n    mso-table-rspace: 0pt;\n    width: 100%;\n}\n\ntable td {\n    font-family: sans-serif;\n    font-size: 14px;\n    vertical-align: top;\n}\n\n/* -------------------------------------\n    BODY & CONTAINER\n------------------------------------- */\n\n.body {\n    background-color: #f6f6f6;\n    width: 100%;\n}\n\n/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n.container {\n    display: block;\n    Margin: 0 auto !important;\n    /* makes it centered */\n    max-width: 580px;\n    padding: 10px;\n    width: auto !important;\n    width: 580px;\n}\n\n/* This should also be a block element, so that it will fill 100% of the .container */\n.content {\n    box-sizing: border-box;\n    display: block;\n    Margin: 0 auto;\n    max-width: 580px;\n    padding: 10px;\n}\n\n/* -------------------------------------\n    HEADER, FOOTER, MAIN\n------------------------------------- */\n.main {\n    background: #fff;\n    border-radius: 3px;\n    width: 100%;\n}\n\n.wrapper {\n    box-sizing: border-box;\n    padding: 20px;\n}\n\n.contact {\n    border-collapse: collapse;\n    padding-bottom: 100px;\n}\n\n.contact td {\n    padding: 0.25rem;\n    text-align: left;\n}\n\n.contact tr {\n    border-bottom: 1px solid #eee;\n}\n\n.footer {\n    clear: both;\n    padding-top: 10px;\n    text-align: center;\n    width: 100%;\n}\n\n.footer td,\n.footer p,\n.footer span,\n.footer a {\n    color: #999999;\n    font-size: 12px;\n    text-align: center;\n}\n\n/* -------------------------------------\n    TYPOGRAPHY\n------------------------------------- */\nh1,\nh2,\nh3,\nh4 {\n    color: #000000;\n    font-family: sans-serif;\n    font-weight: 400;\n    line-height: 1.4;\n    margin: 0;\n    Margin-bottom: 30px;\n}\n\nh1 {\n    font-size: 35px;\n    font-weight: 300;\n    text-align: center;\n    text-transform: capitalize;\n}\n\np,\nul,\nol {\n    font-family: sans-serif;\n    font-size: 14px;\n    font-weight: normal;\n    margin: 0;\n    Margin-bottom: 15px;\n}\n\np li,\nul li,\nol li {\n    list-style-position: inside;\n    margin-left: 5px;\n}\n\na {\n    color: #3498db;\n    text-decoration: underline;\n}\n\n/* -------------------------------------\n    BUTTONS\n------------------------------------- */\n.btn {\n    box-sizing: border-box;\n    width: 100%;\n}\n\n.btn > tbody > tr > td {\n    padding-bottom: 15px;\n}\n\n.btn table {\n    width: auto;\n}\n\n.btn table td {\n    background-color: #ffffff;\n    border-radius: 5px;\n    text-align: center;\n}\n\n.btn a {\n    background-color: #ffffff;\n    border: solid 1px #3498db;\n    border-radius: 5px;\n    box-sizing: border-box;\n    color: #3498db;\n    cursor: pointer;\n    display: inline-block;\n    font-size: 14px;\n    font-weight: bold;\n    margin: 0;\n    padding: 12px 25px;\n    text-decoration: none;\n    text-transform: capitalize;\n}\n\n.btn-primary table td {\n    background-color: #3498db;\n}\n\n.btn-primary a {\n    background-color: #3498db;\n    border-color: #3498db;\n    color: #ffffff;\n}\n\n/* -------------------------------------\n    OTHER STYLES THAT MIGHT BE USEFUL\n------------------------------------- */\n.last {\n    margin-bottom: 0;\n}\n\n.first {\n    margin-top: 0;\n}\n\n.align-center {\n    text-align: center;\n}\n\n.align-right {\n    text-align: right;\n}\n\n.align-left {\n    text-align: left;\n}\n\n.clear {\n    clear: both;\n}\n\n.mt0 {\n    margin-top: 0;\n}\n\n.mb0 {\n    margin-bottom: 0;\n}\n\n.preheader {\n    color: transparent;\n    display: none;\n    height: 0;\n    max-height: 0;\n    max-width: 0;\n    opacity: 0;\n    overflow: hidden;\n    mso-hide: all;\n    visibility: hidden;\n    width: 0;\n}\n\n.powered-by a {\n    text-decoration: none;\n}\n\nhr {\n    border: 0;\n    border-bottom: 1px solid #f6f6f6;\n    Margin: 20px 0;\n}\n\n/* -------------------------------------\n    RESPONSIVE AND MOBILE FRIENDLY STYLES\n------------------------------------- */\n@media only screen and (max-width: 620px) {\n    table[class=body] h1 {\n        font-size: 28px !important;\n        margin-bottom: 10px !important;\n    }\n\n    table[class=body] p,\n    table[class=body] ul,\n    table[class=body] ol,\n    table[class=body] td,\n    table[class=body] span,\n    table[class=body] a {\n        font-size: 16px !important;\n    }\n\n    table[class=body] .wrapper,\n    table[class=body] .article {\n        padding: 10px !important;\n    }\n\n    table[class=body] .content {\n        padding: 0 !important;\n    }\n\n    table[class=body] .container {\n        padding: 0 !important;\n        width: 100% !important;\n    }\n\n    table[class=body] .main {\n        border-left-width: 0 !important;\n        border-radius: 0 !important;\n        border-right-width: 0 !important;\n    }\n\n    table[class=body] .btn table {\n        width: 100% !important;\n    }\n\n    table[class=body] .btn a {\n        width: 100% !important;\n    }\n\n    table[class=body] .img-responsive {\n        height: auto !important;\n        max-width: 100% !important;\n        width: auto !important;\n    }\n}\n\n/* -------------------------------------\n    PRESERVE THESE STYLES IN THE HEAD\n------------------------------------- */\n@media all {\n    .ExternalClass {\n        width: 100%;\n    }\n\n    .ExternalClass,\n    .ExternalClass p,\n    .ExternalClass span,\n    .ExternalClass font,\n    .ExternalClass td,\n    .ExternalClass div {\n        line-height: 100%;\n    }\n\n    .apple-link a {\n        color: inherit !important;\n        font-family: inherit !important;\n        font-size: inherit !important;\n        font-weight: inherit !important;\n        line-height: inherit !important;\n        text-decoration: none !important;\n    }\n\n    .btn-primary table td:hover {\n        background-color: #34495e !important;\n    }\n\n    .btn-primary a:hover {\n        background-color: #34495e !important;\n        border-color: #34495e !important;\n    }\n}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

DROP TABLE IF EXISTS `system_mail_partials`;
CREATE TABLE IF NOT EXISTS `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_partials`
--

INSERT INTO `system_mail_partials` (`id`, `name`, `code`, `content_html`, `content_text`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'header', '<tr>\n    <td class=\"header\">\n        {% if url %}\n            <a href=\"{{ url }}\">\n                {{ body }}\n            </a>\n        {% else %}\n            <span>\n                {{ body }}\n            </span>\n        {% endif %}\n    </td>\n</tr>', '*** {{ body|trim }} <{{ url }}>', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(2, 'Footer', 'footer', '<tr>\n    <td>\n        <table class=\"footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n            <tr>\n                <td class=\"content-cell\" align=\"center\">\n                    {{ body|md_safe }}\n                </td>\n            </tr>\n        </table>\n    </td>\n</tr>', '-------------------\n{{ body|trim }}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(3, 'Button', 'button', '<table class=\"action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td align=\"center\">\n                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n                            <tr>\n                                <td>\n                                    <a href=\"{{ url }}\" class=\"button button-{{ type ?: \'primary\' }}\" target=\"_blank\">\n                                        {{ body }}\n                                    </a>\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }} <{{ url }}>', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(4, 'Panel', 'panel', '<table class=\"panel\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td class=\"panel-content\">\n            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                <tr>\n                    <td class=\"panel-item\">\n                        {{ body|md_safe }}\n                    </td>\n                </tr>\n            </table>\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(5, 'Table', 'table', '<div class=\"table\">\n    {{ body|md_safe }}\n</div>', '{{ body|trim }}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(6, 'Subcopy', 'subcopy', '<table class=\"subcopy\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td>\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '-----\n{{ body|trim }}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(7, 'Promotion', 'promotion', '<table class=\"promotion\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n    <tr>\n        <td align=\"center\">\n            {{ body|md_safe }}\n        </td>\n    </tr>\n</table>', '{{ body|trim }}', 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

DROP TABLE IF EXISTS `system_mail_templates`;
CREATE TABLE IF NOT EXISTS `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_mail_templates_layout_id_index` (`layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_templates`
--

INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
(1, 'renatio.formbuilder::mail.contact', NULL, NULL, NULL, NULL, NULL, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(2, 'renatio.formbuilder::mail.default', NULL, NULL, NULL, NULL, NULL, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(3, 'marcelhaupt.email::mail.sample', NULL, 'This is a sample newsletter', NULL, NULL, 1, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(4, 'backend::mail.invite', NULL, 'Invite new admin to the site', NULL, NULL, 2, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39'),
(5, 'backend::mail.restore', NULL, 'Reset an admin password', NULL, NULL, 2, 0, '2018-04-08 01:26:39', '2018-04-08 01:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

DROP TABLE IF EXISTS `system_parameters`;
CREATE TABLE IF NOT EXISTS `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `item_index` (`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'system', 'update', 'retry', '1523211655'),
(3, 'system', 'core', 'build', '\"434\"'),
(4, 'system', 'core', 'hash', '\"eb0dccd7698b8d533b4758b812ea10ad\"'),
(5, 'system', 'project', 'id', '\"1ZGx2BQtgAwx1BGxgMGp0AwHlAwSvMGDjMTHlA2AyZwVmZwH1MQuzZGEzZwR\"'),
(6, 'system', 'project', 'name', '\"Bosslaser\"'),
(7, 'system', 'project', 'owner', '\"Ahmed Ibrahim\"');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

DROP TABLE IF EXISTS `system_plugin_history`;
CREATE TABLE IF NOT EXISTS `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_plugin_history_code_index` (`code`),
  KEY `system_plugin_history_type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=324 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2018-04-08 01:20:22'),
(2, 'Alexis.Banners', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-08 01:26:21'),
(3, 'Alexis.Banners', 'script', '1.0.2', 'builder_table_create_alexis_banners_configs.php', '2018-04-08 01:26:22'),
(4, 'Alexis.Banners', 'comment', '1.0.2', 'Created table alexis_banners_configs', '2018-04-08 01:26:22'),
(5, 'Alexis.Banners', 'script', '1.0.3', 'builder_table_create_alexis_banners_banners.php', '2018-04-08 01:26:22'),
(6, 'Alexis.Banners', 'comment', '1.0.3', 'Created table alexis_banners_banners', '2018-04-08 01:26:22'),
(7, 'Alexis.Banners', 'script', '1.0.4', 'builder_table_update_alexis_banners_banners.php', '2018-04-08 01:26:24'),
(8, 'Alexis.Banners', 'comment', '1.0.4', 'Updated table alexis_banners_banners', '2018-04-08 01:26:24'),
(9, 'Alexis.Banners', 'script', '1.0.5', 'builder_table_update_alexis_banners_banners_2.php', '2018-04-08 01:26:25'),
(10, 'Alexis.Banners', 'comment', '1.0.5', 'Updated table alexis_banners_banners', '2018-04-08 01:26:25'),
(11, 'Alexis.Banners', 'script', '1.0.6', 'builder_table_update_alexis_banners_banners_3.php', '2018-04-08 01:26:26'),
(12, 'Alexis.Banners', 'comment', '1.0.6', 'Updated table alexis_banners_banners', '2018-04-08 01:26:26'),
(13, 'Alexis.Banners', 'script', '1.0.7', 'builder_table_update_alexis_banners_configs.php', '2018-04-08 01:26:27'),
(14, 'Alexis.Banners', 'comment', '1.0.7', 'Updated table alexis_banners_configs', '2018-04-08 01:26:27'),
(15, 'Alexis.Banners', 'script', '1.0.8', 'builder_table_update_alexis_banners_banners_4.php', '2018-04-08 01:26:27'),
(16, 'Alexis.Banners', 'comment', '1.0.8', 'Updated table alexis_banners_banners', '2018-04-08 01:26:27'),
(17, 'Alexis.Banners', 'script', '1.0.9', 'builder_table_update_alexis_banners_banners_5.php', '2018-04-08 01:26:27'),
(18, 'Alexis.Banners', 'comment', '1.0.9', 'Updated table alexis_banners_banners', '2018-04-08 01:26:27'),
(19, 'Alexis.Banners', 'script', '1.0.10', 'builder_table_update_alexis_banners_banners_6.php', '2018-04-08 01:26:27'),
(20, 'Alexis.Banners', 'comment', '1.0.10', 'Updated table alexis_banners_banners', '2018-04-08 01:26:27'),
(21, 'Alexis.Banners', 'script', '1.0.11', 'builder_table_create_alexis_banners_queues.php', '2018-04-08 01:26:27'),
(22, 'Alexis.Banners', 'comment', '1.0.11', 'Created table alexis_banners_queues', '2018-04-08 01:26:27'),
(23, 'MarcelHaupt.Email', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-08 01:26:27'),
(24, 'MarcelHaupt.Email', 'script', '1.0.2', 'builder_table_create_marcelhaupt_email_user_settings.php', '2018-04-08 01:26:28'),
(25, 'MarcelHaupt.Email', 'comment', '1.0.2', 'Created table marcelhaupt_email_user_settings', '2018-04-08 01:26:28'),
(26, 'MarcelHaupt.Email', 'script', '1.0.3', 'builder_table_create_marcelhaupt_email_campaigns.php', '2018-04-08 01:26:28'),
(27, 'MarcelHaupt.Email', 'comment', '1.0.3', 'Created table marcelhaupt_email_campaigns', '2018-04-08 01:26:28'),
(28, 'MarcelHaupt.Email', 'script', '1.0.4', 'builder_table_create_marcelhaupt_email_actions.php', '2018-04-08 01:26:28'),
(29, 'MarcelHaupt.Email', 'comment', '1.0.4', 'Created table marcelhaupt_email_actions', '2018-04-08 01:26:28'),
(30, 'MarcelHaupt.Email', 'script', '1.0.5', 'builder_table_create_marcelhaupt_email_conditions.php', '2018-04-08 01:26:28'),
(31, 'MarcelHaupt.Email', 'comment', '1.0.5', 'Created table marcelhaupt_email_conditions', '2018-04-08 01:26:28'),
(32, 'MarcelHaupt.Email', 'script', '1.0.6', 'builder_table_update_marcelhaupt_email_actions.php', '2018-04-08 01:26:29'),
(33, 'MarcelHaupt.Email', 'comment', '1.0.6', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:29'),
(34, 'MarcelHaupt.Email', 'script', '1.0.7', 'builder_table_update_marcelhaupt_email_user_settings.php', '2018-04-08 01:26:29'),
(35, 'MarcelHaupt.Email', 'comment', '1.0.7', 'Updated table marcelhaupt_email_user_settings', '2018-04-08 01:26:29'),
(36, 'MarcelHaupt.Email', 'script', '1.0.8', 'builder_table_update_marcelhaupt_email_actions_2.php', '2018-04-08 01:26:29'),
(37, 'MarcelHaupt.Email', 'comment', '1.0.8', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:29'),
(38, 'MarcelHaupt.Email', 'script', '1.0.9', 'builder_table_update_marcelhaupt_email_campaigns.php', '2018-04-08 01:26:29'),
(39, 'MarcelHaupt.Email', 'comment', '1.0.9', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:29'),
(40, 'MarcelHaupt.Email', 'script', '1.0.10', 'builder_table_update_marcelhaupt_email_campaigns_2.php', '2018-04-08 01:26:29'),
(41, 'MarcelHaupt.Email', 'comment', '1.0.10', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:29'),
(42, 'MarcelHaupt.Email', 'script', '1.0.11', 'builder_table_update_marcelhaupt_email_campaigns_3.php', '2018-04-08 01:26:30'),
(43, 'MarcelHaupt.Email', 'comment', '1.0.11', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:30'),
(44, 'MarcelHaupt.Email', 'script', '1.0.12', 'builder_table_update_marcelhaupt_email_conditions.php', '2018-04-08 01:26:30'),
(45, 'MarcelHaupt.Email', 'comment', '1.0.12', 'Updated table marcelhaupt_email_conditions', '2018-04-08 01:26:30'),
(46, 'MarcelHaupt.Email', 'script', '1.0.13', 'builder_table_update_marcelhaupt_email_campaigns_4.php', '2018-04-08 01:26:30'),
(47, 'MarcelHaupt.Email', 'comment', '1.0.13', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:30'),
(48, 'MarcelHaupt.Email', 'script', '1.0.14', 'builder_table_update_marcelhaupt_email_actions_3.php', '2018-04-08 01:26:32'),
(49, 'MarcelHaupt.Email', 'comment', '1.0.14', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:32'),
(50, 'MarcelHaupt.Email', 'script', '1.0.15', 'builder_table_update_marcelhaupt_email_actions_4.php', '2018-04-08 01:26:32'),
(51, 'MarcelHaupt.Email', 'comment', '1.0.15', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:32'),
(52, 'MarcelHaupt.Email', 'script', '1.0.16', 'builder_table_update_marcelhaupt_email_actions_5.php', '2018-04-08 01:26:33'),
(53, 'MarcelHaupt.Email', 'comment', '1.0.16', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:33'),
(54, 'MarcelHaupt.Email', 'script', '1.0.17', 'builder_table_update_marcelhaupt_email_actions_6.php', '2018-04-08 01:26:34'),
(55, 'MarcelHaupt.Email', 'comment', '1.0.17', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:34'),
(56, 'MarcelHaupt.Email', 'script', '1.0.18', 'builder_table_create_marcelhaupt_email_send_log.php', '2018-04-08 01:26:34'),
(57, 'MarcelHaupt.Email', 'comment', '1.0.18', 'Created table marcelhaupt_email_send_log', '2018-04-08 01:26:34'),
(58, 'MarcelHaupt.Email', 'script', '1.0.19', 'builder_table_create_marcelhaupt_email_action_log.php', '2018-04-08 01:26:34'),
(59, 'MarcelHaupt.Email', 'comment', '1.0.19', 'Created table marcelhaupt_email_action_log', '2018-04-08 01:26:34'),
(60, 'MarcelHaupt.Email', 'script', '1.0.20', 'builder_table_update_marcelhaupt_email_send_log.php', '2018-04-08 01:26:34'),
(61, 'MarcelHaupt.Email', 'comment', '1.0.20', 'Updated table marcelhaupt_email_send_log', '2018-04-08 01:26:34'),
(62, 'MarcelHaupt.Email', 'script', '1.0.21', 'builder_table_update_marcelhaupt_email_campaigns_5.php', '2018-04-08 01:26:34'),
(63, 'MarcelHaupt.Email', 'comment', '1.0.21', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:34'),
(64, 'MarcelHaupt.Email', 'script', '1.0.22', 'builder_table_update_marcelhaupt_email_campaigns_6.php', '2018-04-08 01:26:35'),
(65, 'MarcelHaupt.Email', 'comment', '1.0.22', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:35'),
(66, 'MarcelHaupt.Email', 'script', '1.0.23', 'builder_table_update_marcelhaupt_email_user_settings_2.php', '2018-04-08 01:26:35'),
(67, 'MarcelHaupt.Email', 'comment', '1.0.23', 'Updated table marcelhaupt_email_user_settings', '2018-04-08 01:26:35'),
(68, 'MarcelHaupt.Email', 'script', '1.0.24', 'builder_table_update_marcelhaupt_email_conditions_2.php', '2018-04-08 01:26:35'),
(69, 'MarcelHaupt.Email', 'comment', '1.0.24', 'Updated table marcelhaupt_email_conditions', '2018-04-08 01:26:35'),
(70, 'MarcelHaupt.Email', 'script', '1.0.25', 'builder_table_update_marcelhaupt_email_conditions_3.php', '2018-04-08 01:26:35'),
(71, 'MarcelHaupt.Email', 'comment', '1.0.25', 'Updated table marcelhaupt_email_conditions', '2018-04-08 01:26:35'),
(72, 'MarcelHaupt.Email', 'script', '1.0.26', 'builder_table_update_marcelhaupt_email_action_log.php', '2018-04-08 01:26:36'),
(73, 'MarcelHaupt.Email', 'comment', '1.0.26', 'Updated table marcelhaupt_email_action_log', '2018-04-08 01:26:36'),
(74, 'MarcelHaupt.Email', 'script', '1.0.27', 'builder_table_update_marcelhaupt_email_campaigns_7.php', '2018-04-08 01:26:36'),
(75, 'MarcelHaupt.Email', 'comment', '1.0.27', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:36'),
(76, 'MarcelHaupt.Email', 'script', '1.0.28', 'builder_table_update_marcelhaupt_email_send_log_2.php', '2018-04-08 01:26:36'),
(77, 'MarcelHaupt.Email', 'comment', '1.0.28', 'Updated table marcelhaupt_email_send_log', '2018-04-08 01:26:36'),
(78, 'MarcelHaupt.Email', 'script', '1.0.29', 'builder_table_update_marcelhaupt_email_user_settings_3.php', '2018-04-08 01:26:36'),
(79, 'MarcelHaupt.Email', 'comment', '1.0.29', 'Updated table marcelhaupt_email_user_settings', '2018-04-08 01:26:36'),
(80, 'MarcelHaupt.Email', 'script', '1.0.30', 'builder_table_update_marcelhaupt_email_action.php', '2018-04-08 01:26:36'),
(81, 'MarcelHaupt.Email', 'comment', '1.0.30', 'Updated table marcelhaupt_email_actions', '2018-04-08 01:26:36'),
(82, 'MarcelHaupt.Email', 'script', '1.0.31', 'builder_table_update_marcelhaupt_email_campaign.php', '2018-04-08 01:26:37'),
(83, 'MarcelHaupt.Email', 'comment', '1.0.31', 'Updated table marcelhaupt_email_campaigns', '2018-04-08 01:26:37'),
(84, 'MarcelHaupt.Email', 'script', '1.0.32', 'builder_table_update_marcelhaupt_email_condition.php', '2018-04-08 01:26:37'),
(85, 'MarcelHaupt.Email', 'comment', '1.0.32', 'Updated table marcelhaupt_email_conditions', '2018-04-08 01:26:37'),
(86, 'MarcelHaupt.Email', 'script', '1.0.33', 'builder_table_update_marcelhaupt_email_campaign_2.php', '2018-04-08 01:26:37'),
(87, 'MarcelHaupt.Email', 'comment', '1.0.33', 'Updated table marcelhaupt_email_campaign', '2018-04-08 01:26:37'),
(88, 'MarcelHaupt.Email', 'comment', '1.0.40', 'User Settings Hotfix', '2018-04-08 01:26:37'),
(89, 'MarcelHaupt.Email', 'comment', '1.0.41', 'Action Hotfix', '2018-04-08 01:26:37'),
(90, 'MarcelHaupt.Email', 'comment', '1.0.42', 'Download Emails button added', '2018-04-08 01:26:37'),
(91, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_forms_table.php', '2018-04-08 01:26:38'),
(92, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_fields_table.php', '2018-04-08 01:26:39'),
(93, 'Renatio.FormBuilder', 'script', '1.0.1', 'create_field_types_table.php', '2018-04-08 01:26:39'),
(94, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_field_types_table.php', '2018-04-08 01:26:39'),
(95, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_contact_form.php', '2018-04-08 01:26:39'),
(96, 'Renatio.FormBuilder', 'script', '1.0.1', 'seed_default_form.php', '2018-04-08 01:26:39'),
(97, 'Renatio.FormBuilder', 'comment', '1.0.1', 'First version of Form Builder plugin.', '2018-04-08 01:26:40'),
(98, 'Renatio.FormBuilder', 'comment', '1.0.2', 'Reference __SELF__ in component partial.', '2018-04-08 01:26:40'),
(99, 'Renatio.FormBuilder', 'script', '1.0.3', 'seed_file_field_type.php', '2018-04-08 01:26:40'),
(100, 'Renatio.FormBuilder', 'comment', '1.0.3', 'Add file attachment field type.', '2018-04-08 01:26:40'),
(101, 'Renatio.FormBuilder', 'script', '1.0.4', 'create_form_logs_table.php', '2018-04-08 01:26:40'),
(102, 'Renatio.FormBuilder', 'comment', '1.0.4', 'Add simple logs.', '2018-04-08 01:26:40'),
(103, 'Renatio.FormBuilder', 'comment', '1.1.0', '!!! Important update', '2018-04-08 01:26:40'),
(104, 'Renatio.FormBuilder', 'script', '1.1.1', 'add_css_class_column_to_forms_table.php', '2018-04-08 01:26:41'),
(105, 'Renatio.FormBuilder', 'comment', '1.1.1', 'Use Twig::parse() facade. Only update for October build 300 and above!', '2018-04-08 01:26:41'),
(106, 'Renatio.FormBuilder', 'comment', '1.1.2', 'Allow for multiple forms on the same cms page. Fix required validation rule for fileuploader field type.', '2018-04-08 01:26:41'),
(107, 'Renatio.FormBuilder', 'comment', '1.1.3', 'Minor changes.', '2018-04-08 01:26:41'),
(108, 'Renatio.FormBuilder', 'script', '1.1.4', 'seed_location_field_types.php', '2018-04-08 01:26:41'),
(109, 'Renatio.FormBuilder', 'script', '1.1.4', 'add_css_wrapper_class_column_to_fields_table.php', '2018-04-08 01:26:42'),
(110, 'Renatio.FormBuilder', 'comment', '1.1.4', 'RainLab Location Plugin support.', '2018-04-08 01:26:42'),
(111, 'Renatio.FormBuilder', 'comment', '1.1.5', 'Add Event to extend default functionality.', '2018-04-08 01:26:42'),
(112, 'Renatio.FormBuilder', 'script', '1.1.6', 'add_reply_and_autoresponse_fields.php', '2018-04-08 01:26:42'),
(113, 'Renatio.FormBuilder', 'comment', '1.1.6', 'Add reply to and autoresponse functionality.', '2018-04-08 01:26:42'),
(114, 'Renatio.FormBuilder', 'script', '1.1.7', 'create_sections_table.php', '2018-04-08 01:26:44'),
(115, 'Renatio.FormBuilder', 'script', '1.1.7', 'seed_section_field_type.php', '2018-04-08 01:26:44'),
(116, 'Renatio.FormBuilder', 'comment', '1.1.7', 'Add sections feature.', '2018-04-08 01:26:44'),
(117, 'Renatio.FormBuilder', 'script', '1.1.8', 'add_content_html_column_to_form_logs_table.php', '2018-04-08 01:26:45'),
(118, 'Renatio.FormBuilder', 'comment', '1.1.8', 'Add HTML preview in logs.', '2018-04-08 01:26:45'),
(119, 'Renatio.FormBuilder', 'script', '1.1.9', 'add_comment_column_to_fields_table.php', '2018-04-08 01:26:45'),
(120, 'Renatio.FormBuilder', 'comment', '1.1.9', 'Add comment to fields.', '2018-04-08 01:26:45'),
(121, 'Renatio.FormBuilder', 'comment', '1.2.0', 'Dutch translation.', '2018-04-08 01:26:45'),
(122, 'Renatio.FormBuilder', 'script', '1.2.1', 'add_validation_messages_column_to_fields_table.php', '2018-04-08 01:26:46'),
(123, 'Renatio.FormBuilder', 'comment', '1.2.1', 'Allow for custom validation messages.', '2018-04-08 01:26:46'),
(124, 'Renatio.FormBuilder', 'comment', '1.2.2', 'Allow to manually specify url for redirect.', '2018-04-08 01:26:46'),
(125, 'Renatio.FormBuilder', 'comment', '1.2.3', 'German translation.', '2018-04-08 01:26:46'),
(126, 'Renatio.FormBuilder', 'comment', '1.2.4', 'Minor changes.', '2018-04-08 01:26:46'),
(127, 'Renatio.FormBuilder', 'comment', '1.2.5', 'Allow to duplicate a form.', '2018-04-08 01:26:46'),
(128, 'Renatio.FormBuilder', 'comment', '1.2.6', 'Export form logs to CSV file.', '2018-04-08 01:26:46'),
(129, 'Renatio.FormBuilder', 'comment', '1.2.7', 'Improve seeding mail templates. Add Contact Form and Default Form. Update Mail Layout.', '2018-04-08 01:26:46'),
(130, 'Renatio.FormBuilder', 'comment', '1.2.8', 'Fix sending email with file attachment for October build 401+', '2018-04-08 01:26:46'),
(131, 'Renatio.FormBuilder', 'script', '1.2.9', 'fix_rainlab_translate_issues.php', '2018-04-08 01:26:46'),
(132, 'Renatio.FormBuilder', 'comment', '1.2.9', 'Fix RainLab.Translate compatibility issues.', '2018-04-08 01:26:46'),
(133, 'Renatio.FormBuilder', 'comment', '1.3.0', 'Allow to use CMS filters and functions in field templates.', '2018-04-08 01:26:46'),
(134, 'Renatio.FormBuilder', 'comment', '1.3.1', 'Fix example forms seeding.', '2018-04-08 01:26:46'),
(135, 'Renatio.FormBuilder', 'comment', '1.4.0', '!!! Nicer form field validation.', '2018-04-08 01:26:46'),
(136, 'Renatio.FormBuilder', 'comment', '1.4.1', 'Set default reCaptcha keys for testing.', '2018-04-08 01:26:46'),
(137, 'Renatio.FormBuilder', 'comment', '1.4.2', 'Fix fields and sections translation.', '2018-04-08 01:26:46'),
(138, 'Renatio.SeoManager', 'script', '1.0.1', 'create_seo_tags_table.php', '2018-04-08 01:26:46'),
(139, 'Renatio.SeoManager', 'comment', '1.0.1', 'First version of Seo Manager.', '2018-04-08 01:26:46'),
(140, 'Renatio.SeoManager', 'script', '1.0.2', 'import_default_values.php', '2018-04-08 01:26:48'),
(141, 'Renatio.SeoManager', 'comment', '1.0.2', 'Import data.', '2018-04-08 01:26:48'),
(142, 'Renatio.SeoManager', 'comment', '1.0.3', 'Import SeoExtension Settings.', '2018-04-08 01:26:48'),
(143, 'Renatio.SeoManager', 'comment', '1.0.4', 'Fix bug for PHP version < 5.6.', '2018-04-08 01:26:48'),
(144, 'Renatio.SeoManager', 'comment', '1.0.5', 'Fix spaces on prefix/suffix title.', '2018-04-08 01:26:48'),
(145, 'Renatio.SeoManager', 'comment', '1.0.6', 'Fix repeater field error in RainLab.Pages plugin.', '2018-04-08 01:26:48'),
(146, 'Renatio.SeoManager', 'comment', '1.0.7', 'Fix showing Seo fields in repeater.', '2018-04-08 01:26:48'),
(147, 'Renatio.SeoManager', 'comment', '1.1.0', '!!! Update only for Laravel 5.5 LTS.', '2018-04-08 01:26:48'),
(148, 'Renatio.SeoManager', 'comment', '1.1.1', 'Fix RainLab.Blog installation issue.', '2018-04-08 01:26:48'),
(149, 'Renatio.SeoManager', 'comment', '1.1.2', 'Fix attaching SEO fields to model with empty tab fields.', '2018-04-08 01:26:48'),
(150, 'Renatio.SeoManager', 'comment', '1.1.3', 'Fix attaching morphOne relation to model.', '2018-04-08 01:26:48'),
(151, 'Renatio.SeoManager', 'comment', '1.1.4', 'Fix adding SEO fields for model with pivot.', '2018-04-08 01:26:48'),
(152, 'Renatio.SeoManager', 'script', '1.1.5', 'update_seo_fields_length.php', '2018-04-08 01:26:49'),
(153, 'Renatio.SeoManager', 'comment', '1.1.5', 'Increase meta title and description fields length.', '2018-04-08 01:26:49'),
(154, 'Renatio.SeoManager', 'script', '1.1.6', 'update_morph_fields_to_nullable.php', '2018-04-08 01:26:49'),
(155, 'Renatio.SeoManager', 'comment', '1.1.6', 'Make polymorphic fields nullable.', '2018-04-08 01:26:49'),
(156, 'Renatio.SeoManager', 'comment', '1.1.7', 'Fix adding SEO fields inside repeater fields.', '2018-04-08 01:26:49'),
(157, 'Renatio.SeoManager', 'comment', '1.1.8', 'Ensure OG image dimensions are set.', '2018-04-08 01:26:49'),
(158, 'Renatio.SeoManager', 'comment', '1.1.9', 'Fix adding SEO fields to pivot models (edge case).', '2018-04-08 01:26:49'),
(159, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2018-04-08 01:28:16'),
(160, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2018-04-08 01:28:16'),
(161, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2018-04-08 01:28:16'),
(162, 'RainLab.User', 'comment', '1.0.2', 'Seed tables.', '2018-04-08 01:28:16'),
(163, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2018-04-08 01:28:16'),
(164, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2018-04-08 01:28:16'),
(165, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2018-04-08 01:28:16'),
(166, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2018-04-08 01:28:17'),
(167, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2018-04-08 01:28:17'),
(168, 'RainLab.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2018-04-08 01:28:17'),
(169, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2018-04-08 01:28:17'),
(170, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2018-04-08 01:28:17'),
(171, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2018-04-08 01:28:17'),
(172, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2018-04-08 01:28:18'),
(173, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2018-04-08 01:28:18'),
(174, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2018-04-08 01:28:19'),
(175, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2018-04-08 01:28:19'),
(176, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2018-04-08 01:28:19'),
(177, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2018-04-08 01:28:19'),
(178, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2018-04-08 01:28:19'),
(179, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2018-04-08 01:28:20'),
(180, 'RainLab.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2018-04-08 01:28:20'),
(181, 'RainLab.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2018-04-08 01:28:20'),
(182, 'RainLab.User', 'script', '1.1.1', 'create_user_groups_table.php', '2018-04-08 01:28:21'),
(183, 'RainLab.User', 'script', '1.1.1', 'seed_user_groups_table.php', '2018-04-08 01:28:21'),
(184, 'RainLab.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2018-04-08 01:28:21'),
(185, 'RainLab.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2018-04-08 01:28:21'),
(186, 'RainLab.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2018-04-08 01:28:21'),
(187, 'RainLab.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2018-04-08 01:28:22'),
(188, 'RainLab.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2018-04-08 01:28:22'),
(189, 'RainLab.User', 'script', '1.2.0', 'users_add_deleted_at.php', '2018-04-08 01:28:22'),
(190, 'RainLab.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2018-04-08 01:28:22'),
(191, 'RainLab.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2018-04-08 01:28:22'),
(192, 'RainLab.User', 'comment', '1.2.2', 'Add bulk action button to user list.', '2018-04-08 01:28:22'),
(193, 'RainLab.User', 'comment', '1.2.3', 'Included some descriptive paragraphs in the Reset Password component markup.', '2018-04-08 01:28:22'),
(194, 'RainLab.User', 'comment', '1.2.4', 'Added a checkbox for blocking all mail sent to the user.', '2018-04-08 01:28:22'),
(195, 'RainLab.User', 'script', '1.2.5', 'update_timestamp_nullable.php', '2018-04-08 01:28:23'),
(196, 'RainLab.User', 'comment', '1.2.5', 'Database maintenance. Updated all timestamp columns to be nullable.', '2018-04-08 01:28:23'),
(197, 'RainLab.User', 'script', '1.2.6', 'users_add_last_seen.php', '2018-04-08 01:28:23'),
(198, 'RainLab.User', 'comment', '1.2.6', 'Add a dedicated last seen column for users.', '2018-04-08 01:28:23'),
(199, 'RainLab.User', 'comment', '1.2.7', 'Minor fix to user timestamp attributes.', '2018-04-08 01:28:23'),
(200, 'RainLab.User', 'comment', '1.2.8', 'Add date range filter to users list. Introduced a logout event.', '2018-04-08 01:28:23'),
(201, 'RainLab.User', 'comment', '1.2.9', 'Add invitation mail for new accounts created in the back-end.', '2018-04-08 01:28:23'),
(202, 'RainLab.User', 'script', '1.3.0', 'users_add_guest_flag.php', '2018-04-08 01:28:24'),
(203, 'RainLab.User', 'script', '1.3.0', 'users_add_superuser_flag.php', '2018-04-08 01:28:24'),
(204, 'RainLab.User', 'comment', '1.3.0', 'Introduced guest user accounts.', '2018-04-08 01:28:24'),
(205, 'RainLab.User', 'comment', '1.3.1', 'User notification variables can now be extended.', '2018-04-08 01:28:24'),
(206, 'RainLab.User', 'comment', '1.3.2', 'Minor fix to the Auth::register method.', '2018-04-08 01:28:24'),
(207, 'RainLab.User', 'comment', '1.3.3', 'Allow prevention of concurrent user sessions via the user settings.', '2018-04-08 01:28:24'),
(208, 'RainLab.User', 'comment', '1.3.4', 'Added force secure protocol property to the account component.', '2018-04-08 01:28:24'),
(209, 'RainLab.User', 'comment', '1.4.0', '!!! The Notifications tab in User settings has been removed.', '2018-04-08 01:28:24'),
(210, 'RainLab.User', 'comment', '1.4.1', 'Added support for user impersonation.', '2018-04-08 01:28:24'),
(211, 'RainLab.User', 'comment', '1.4.2', 'Fixes security bug in Password Reset component.', '2018-04-08 01:28:25'),
(212, 'RainLab.User', 'comment', '1.4.3', 'Fixes session handling for AJAX requests.', '2018-04-08 01:28:25'),
(213, 'RainLab.User', 'comment', '1.4.4', 'Fixes bug where impersonation touches the last seen timestamp.', '2018-04-08 01:28:25'),
(214, 'RainLab.User', 'comment', '1.4.5', 'Added token fallback process to Account / Reset Password components when parameter is missing.', '2018-04-08 01:28:25'),
(215, 'Jiri.JKShop', 'script', '1.0.1', 'create_categories_table.php', '2018-04-08 01:28:25'),
(216, 'Jiri.JKShop', 'comment', '1.0.1', 'First version of JK Shop', '2018-04-08 01:28:25'),
(217, 'Jiri.JKShop', 'script', '1.0.2', 'create_brands_table.php', '2018-04-08 01:28:26'),
(218, 'Jiri.JKShop', 'comment', '1.0.2', 'Add brands', '2018-04-08 01:28:26'),
(219, 'Jiri.JKShop', 'script', '1.0.3', 'create_taxes_table.php', '2018-04-08 01:28:26'),
(220, 'Jiri.JKShop', 'comment', '1.0.3', 'Add Taxes', '2018-04-08 01:28:26'),
(221, 'Jiri.JKShop', 'script', '1.0.4', 'create_carriers_table.php', '2018-04-08 01:28:26'),
(222, 'Jiri.JKShop', 'comment', '1.0.4', 'Add Carriers', '2018-04-08 01:28:26'),
(223, 'Jiri.JKShop', 'script', '1.0.5', 'create_order_statuses_table.php', '2018-04-08 01:28:26'),
(224, 'Jiri.JKShop', 'comment', '1.0.5', 'Add Order Statuses', '2018-04-08 01:28:26'),
(225, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_table.php', '2018-04-08 01:28:27'),
(226, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_categories.php', '2018-04-08 01:28:27'),
(227, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_carriers_disallowed.php', '2018-04-08 01:28:27'),
(228, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_products_featured.php', '2018-04-08 01:28:27'),
(229, 'Jiri.JKShop', 'script', '1.0.6', 'create_products_products_accessories.php', '2018-04-08 01:28:27'),
(230, 'Jiri.JKShop', 'comment', '1.0.6', 'Add Products', '2018-04-08 01:28:27'),
(231, 'Jiri.JKShop', 'script', '1.0.7', 'create_orders_table.php', '2018-04-08 01:28:27'),
(232, 'Jiri.JKShop', 'comment', '1.0.7', 'Add orders', '2018-04-08 01:28:27'),
(233, 'Jiri.JKShop', 'script', '1.0.8', 'create_products_users_price.php', '2018-04-08 01:28:27'),
(234, 'Jiri.JKShop', 'script', '1.0.8', 'create_categories_users_sale.php', '2018-04-08 01:28:28'),
(235, 'Jiri.JKShop', 'comment', '1.0.8', 'Add support RainLab.User (sale for category, individual prices)', '2018-04-08 01:28:28'),
(236, 'Jiri.JKShop', 'script', '1.0.9', 'create_sample_data.php', '2018-04-08 01:28:28'),
(237, 'Jiri.JKShop', 'comment', '1.0.9', 'First Release', '2018-04-08 01:28:28'),
(238, 'Jiri.JKShop', 'script', '1.0.10', 'Component - My Orders - fix link on invoice', '2018-04-08 01:28:28'),
(239, 'Jiri.JKShop', 'comment', '1.0.10', 'Fixes', '2018-04-08 01:28:28'),
(240, 'Jiri.JKShop', 'comment', '1.0.11', 'Backend - language files [EN], Fixes: Links featured + accessories, Basket - work with product minimum quantity, Complete Order - check stock before order', '2018-04-08 01:28:28'),
(241, 'Jiri.JKShop', 'comment', '1.0.12', 'Automatic rounding into order total price - Paypal allow max 2 decimal places', '2018-04-08 01:28:28'),
(242, 'Jiri.JKShop', 'comment', '1.0.13', 'Orders - fix search, add hidden fields into order list (Email, Phone, First name, Address, Postcode, City, Country), all fields are searchable', '2018-04-08 01:28:28'),
(243, 'Jiri.JKShop', 'comment', '1.0.14', 'Fixes: Order detail -  names on list of customers, Orders list - little UI bug', '2018-04-08 01:28:28'),
(244, 'Jiri.JKShop', 'comment', '1.0.15', 'Fix: Carrier - free shipping; Add: Italian translation', '2018-04-08 01:28:28'),
(245, 'Jiri.JKShop', 'script', '1.1.0', 'create_properties_table.php', '2018-04-08 01:28:28'),
(246, 'Jiri.JKShop', 'script', '1.1.0', 'create_property_options_table.php', '2018-04-08 01:28:28'),
(247, 'Jiri.JKShop', 'script', '1.1.0', 'create_products_properties.php', '2018-04-08 01:28:29'),
(248, 'Jiri.JKShop', 'comment', '1.1.0', 'Properties - product properties: sizes, colors, etc.. (https://octobercms.com/plugin/jiri-jkshop#Properties)', '2018-04-08 01:28:29'),
(249, 'Jiri.JKShop', 'script', '1.1.1', 'update_111.php', '2018-04-08 01:28:29'),
(250, 'Jiri.JKShop', 'comment', '1.1.1', 'Add: Order - note, county; New page variables for components (Products By Category, Products By Brand)', '2018-04-08 01:28:29'),
(251, 'Jiri.JKShop', 'script', '1.1.2', 'create_products_options.php', '2018-04-08 01:28:29'),
(252, 'Jiri.JKShop', 'comment', '1.1.2', 'Add: Product - Advanced Properties ( You can add property options with pivot data: title, description, price difference, image ); Update Components: Basket, Product Detail', '2018-04-08 01:28:29'),
(253, 'Jiri.JKShop', 'comment', '1.1.3', 'Fix: Advanced Properties - Options - Order by', '2018-04-08 01:28:29'),
(254, 'Jiri.JKShop', 'script', '1.1.4', 'update_114.php', '2018-04-08 01:28:29'),
(255, 'Jiri.JKShop', 'comment', '1.1.4', 'Add: Order - tracking number for tracking package', '2018-04-08 01:28:29'),
(256, 'Jiri.JKShop', 'comment', '1.2.0', 'Add: Stripe payment gateway', '2018-04-08 01:28:29'),
(257, 'Jiri.JKShop', 'comment', '1.2.1', 'Add: Czech translation', '2018-04-08 01:28:29'),
(258, 'Jiri.JKShop', 'script', '1.2.2', 'create_rainlab_extend_users.php', '2018-04-08 01:28:30'),
(259, 'Jiri.JKShop', 'comment', '1.2.2', 'Add: Settings - default image; Rainlab.User: save billing and delivery address into user; Products Components - add or extend order by', '2018-04-08 01:28:30'),
(260, 'Jiri.JKShop', 'comment', '1.2.3', 'Small fixes and improvements: add some language texts, page 404 for non-existent slug - products, categories, brands', '2018-04-08 01:28:30'),
(261, 'Jiri.JKShop', 'comment', '1.2.4', 'Fix: order list - order by order status', '2018-04-08 01:28:30'),
(262, 'Jiri.JKShop', 'comment', '1.3.0', 'Added translation support', '2018-04-08 01:28:30'),
(263, 'Jiri.JKShop', 'comment', '1.3.1', 'Small Fixes', '2018-04-08 01:28:30'),
(264, 'Jiri.JKShop', 'comment', '1.3.2', 'Fix name of Users without surname', '2018-04-08 01:28:30'),
(265, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_table.php', '2018-04-08 01:28:30'),
(266, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_categories.php', '2018-04-08 01:28:30'),
(267, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_products.php', '2018-04-08 01:28:30'),
(268, 'Jiri.JKShop', 'script', '1.4.0', 'create_coupons_users.php', '2018-04-08 01:28:31'),
(269, 'Jiri.JKShop', 'script', '1.4.0', 'udpate_orders_140.php', '2018-04-08 01:28:32'),
(270, 'Jiri.JKShop', 'comment', '1.4.0', 'Coupons - discount coupons (https://octobercms.com/plugin/jiri-jkshop#Coupons)', '2018-04-08 01:28:32'),
(271, 'Jiri.JKShop', 'comment', '1.4.1', 'Small Fixes in Coupons', '2018-04-08 01:28:32'),
(272, 'Jiri.JKShop', 'comment', '1.4.2', 'Increased character limits (name, title, etc.) to 255', '2018-04-08 01:28:32'),
(273, 'Jiri.JKShop', 'comment', '1.4.3', 'Add: German translation', '2018-04-08 01:28:32'),
(274, 'Jiri.JKShop', 'script', '1.4.4', 'update_144.php', '2018-04-08 01:28:32'),
(275, 'Jiri.JKShop', 'comment', '1.4.4', 'Add Components: BrandDetails, BrandsList. Thanks to Nick Gavanozov', '2018-04-08 01:28:32'),
(276, 'Jiri.JKShop', 'comment', '1.4.5', 'Small Fix in Coupons - datepicker issue', '2018-04-08 01:28:33'),
(277, 'Jiri.JKShop', 'comment', '1.4.6', 'Add Components: BreadcrumbsCategory, BreadcrumbsProduct', '2018-04-08 01:28:33'),
(278, 'Jiri.JKShop', 'comment', '1.4.7', 'Component BreadcrumbsProduct - fix URL', '2018-04-08 01:28:33'),
(279, 'Jiri.JKShop', 'script', '1.5.0', 'create_payment_gateways_table.php', '2018-04-08 01:28:34'),
(280, 'Jiri.JKShop', 'script', '1.5.0', 'update_orders_150.php', '2018-04-08 01:28:35'),
(281, 'Jiri.JKShop', 'comment', '1.5.0', 'Check upgrade guide before upgrading: Add payment gateways, remove previous payment system (https://octobercms.com/plugin/jiri-jkshop#upgrade)', '2018-04-08 01:28:35'),
(282, 'Jiri.JKShop', 'comment', '1.5.1', 'Basket component: moved htm files into basket folder, for easy overriding. Payment Gateways: gateway title is translatable.', '2018-04-08 01:28:35'),
(283, 'Jiri.JKShop', 'script', '1.5.2', 'update_152.php', '2018-04-08 01:28:36'),
(284, 'Jiri.JKShop', 'comment', '1.5.2', 'Small fixes: MyOrderDetail component, MyOrders component - if user or id order is bad, return 404, Add Foreign keys', '2018-04-08 01:28:36'),
(285, 'Jiri.JKShop', 'comment', '1.5.3', 'Basket Component: added method \'onGetSessionBasket\' - now you can easy call this method and get complete basket as JSON', '2018-04-08 01:28:36'),
(286, 'Jiri.JKShop', 'comment', '1.5.4', 'Upgrade: Omnipay payment gateways, Small Fixes in Components', '2018-04-08 01:28:36'),
(287, 'Jiri.JKShop', 'comment', '1.5.5', 'Add payment gateways: TwoCheckoutPlus, TwoCheckoutPlus_Token', '2018-04-08 01:28:36'),
(288, 'Jiri.JKShop', 'comment', '1.5.6', 'Fixed DB error on some MySQL databases', '2018-04-08 01:28:36'),
(289, 'Jiri.JKShop', 'comment', '1.5.7', 'Small fixes: Product Detail component fixed URL for accessories and featured products', '2018-04-08 01:28:36'),
(290, 'Jiri.JKShop', 'comment', '1.5.8', 'Add: French translation (thanks Théo Corée)', '2018-04-08 01:28:36'),
(291, 'Jiri.JKShop', 'comment', '1.5.9', 'Add security checks for payment components', '2018-04-08 01:28:36'),
(292, 'Jiri.JKShop', 'script', '1.6.0', 'update_statuses_160.php', '2018-04-08 01:28:36'),
(293, 'Jiri.JKShop', 'comment', '1.6.0', 'Add Extended Inventory Management. OrderStatus - add new fields: disallow for gateway, qty decrease, qty increase back', '2018-04-08 01:28:36'),
(294, 'Jiri.JKShop', 'comment', '1.6.1', 'Add some features into basket and product details components: allow to add product into basket with qty and etc..', '2018-04-08 01:28:37'),
(295, 'Jiri.JKShop', 'comment', '1.6.2', 'Update: Italian translation', '2018-04-08 01:28:37'),
(296, 'Jiri.JKShop', 'comment', '1.6.3', 'Fixed DB error - installation on PostgreSQL', '2018-04-08 01:28:37'),
(297, 'Jiri.JKShop', 'comment', '1.6.4', 'Backend - Products: Add checkboxes, Add multi delete, Add multi duplicate', '2018-04-08 01:28:37'),
(298, 'Jiri.JKShop', 'script', '1.6.5', 'update_orders_165.php', '2018-04-08 01:28:37'),
(299, 'Jiri.JKShop', 'comment', '1.6.5', 'Orders: add security token - which allow to generate unique URL for a payment pages', '2018-04-08 01:28:37'),
(300, 'Jiri.JKShop', 'comment', '1.6.6', 'Add: Dutch translation (thanks Jeroen)', '2018-04-08 01:28:37'),
(301, 'Jiri.JKShop', 'comment', '1.6.7', 'Add coupon - coupon_wrong_code update - error message about validation it shown only once', '2018-04-08 01:28:37'),
(302, 'Jiri.JKShop', 'comment', '1.6.8', 'Fixed new installation issue', '2018-04-08 01:28:37'),
(303, 'Jiri.JKShop', 'comment', '1.6.9', 'Fixed Payment Gateway component issue - https://octobercms.com/plugin/support/jiri-jkshop/error-onpaymentsubmit', '2018-04-08 01:28:37'),
(304, 'Jiri.JKShop', 'comment', '1.6.10', 'Update vendors (dompdf, omnipay, etc..), small fixes', '2018-04-08 01:28:37'),
(305, 'Jiri.JKShop', 'comment', '1.6.11', 'Small fixes, add search box for accessories and features', '2018-04-08 01:28:37'),
(306, 'Jiri.JKShop', 'comment', '1.6.12', 'Small fixes, add eager-loading for a images to a components: BrandList, ProdctsByBrand, ProductsByCategory, ProductsList - https://octobercms.com/plugin/support/jiri-jkshop/optimize-loading-product-images', '2018-04-08 01:28:37'),
(307, 'Jiri.JKShop', 'script', '1.7.0', 'update_170.php', '2018-04-08 01:28:37'),
(308, 'Jiri.JKShop', 'comment', '1.7.0', 'Extended stock inventory - every product property can have set extended quantity - if this is set main product quantity will be ignored. Example: size X-10pcs; S-5pcs. More info there - https://octobercms.com/plugin/support/jiri-jkshop/feature-request-adding-inventory-per-property?page=1', '2018-04-08 01:28:37'),
(309, 'Jiri.JKShop', 'comment', '1.7.1', 'Small fixes on components: custom payment stripe, product detail, basket - filtering available carrier by total weight', '2018-04-08 01:28:37'),
(310, 'Jiri.JKShop', 'comment', '1.7.2', 'Fix: previous version - doubled product property', '2018-04-08 01:28:37'),
(311, 'Jiri.JKShop', 'script', '1.7.3', 'update_173.php', '2018-04-08 01:28:38'),
(312, 'Jiri.JKShop', 'comment', '1.7.3', 'Add virtual product option; Product - New Tab: Virtual Product; Order Status - new checkbox: is_paid => Consumer can download virtual product from an order detail if current order status is paid.', '2018-04-08 01:28:38'),
(313, 'Jiri.JKShop', 'comment', '1.7.4', 'Fix: installation on PostgreSQL', '2018-04-08 01:28:38'),
(314, 'Jiri.JKShop', 'comment', '1.7.5', 'Update vendors (dompdf, omnipay, etc..), small fix', '2018-04-08 01:28:38'),
(315, 'RainLab.Sitemap', 'comment', '1.0.1', 'First version of Sitemap', '2018-04-08 01:29:02'),
(316, 'RainLab.Sitemap', 'script', '1.0.2', 'create_definitions_table.php', '2018-04-08 01:29:02'),
(317, 'RainLab.Sitemap', 'comment', '1.0.2', 'Create definitions table', '2018-04-08 01:29:02'),
(318, 'RainLab.Sitemap', 'comment', '1.0.3', 'Minor improvements to the code.', '2018-04-08 01:29:02'),
(319, 'RainLab.Sitemap', 'comment', '1.0.4', 'Fixes issue where correct headers not being sent.', '2018-04-08 01:29:02'),
(320, 'RainLab.Sitemap', 'comment', '1.0.5', 'Minor back-end styling fix.', '2018-04-08 01:29:02'),
(321, 'RainLab.Sitemap', 'comment', '1.0.6', 'Minor fix to internal API.', '2018-04-08 01:29:02'),
(322, 'RainLab.Sitemap', 'comment', '1.0.7', 'Added access premissions.', '2018-04-08 01:29:03'),
(323, 'RainLab.Sitemap', 'comment', '1.0.8', 'Minor styling updates.', '2018-04-08 01:29:03');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

DROP TABLE IF EXISTS `system_plugin_versions`;
CREATE TABLE IF NOT EXISTS `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_plugin_versions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'October.Demo', '1.0.1', '2018-04-08 01:20:22', 0, 0),
(2, 'Alexis.Banners', '1.0.11', '2018-04-08 01:26:27', 0, 0),
(3, 'MarcelHaupt.Email', '1.0.42', '2018-04-08 01:26:37', 0, 0),
(4, 'Renatio.FormBuilder', '1.4.2', '2018-04-08 01:26:46', 0, 0),
(5, 'Renatio.SeoManager', '1.1.9', '2018-04-08 01:26:49', 0, 0),
(6, 'RainLab.User', '1.4.5', '2018-04-08 01:28:25', 0, 0),
(7, 'Jiri.JKShop', '1.7.5', '2018-04-08 01:28:38', 0, 0),
(8, 'RainLab.Sitemap', '1.0.8', '2018-04-08 01:29:03', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

DROP TABLE IF EXISTS `system_request_logs`;
CREATE TABLE IF NOT EXISTS `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

DROP TABLE IF EXISTS `system_revisions`;
CREATE TABLE IF NOT EXISTS `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  KEY `system_revisions_user_id_index` (`user_id`),
  KEY `system_revisions_field_index` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE IF NOT EXISTS `system_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `system_settings_item_index` (`item`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'jiri_jkshop_settings', '{\"number_format_thousands_sep\":\",\",\"number_format_decimals\":\"2\",\"number_format_dec_point\":\".\",\"bank_transfer_details_content\":\"\",\"paypal_use_sandbox\":\"1\",\"paypal_debug\":\"1\",\"paypal_business\":\"\",\"invoice_template_content\":\"<p><br>\\r\\n<\\/p><table><colgroup><col><col><col><col><col><col><col><col><\\/colgroup>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\" rowspan=\\\"4\\\">\\r\\n\\t\\t<h2>JK Shop<\\/h2>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\" rowspan=\\\"4\\\">\\r\\n\\t\\t<h2>Invoice: #{{order_id}}<\\/h2>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Supplier:<\\/h3>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Subscriber:<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Company No: 8546131657\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">VAT number: US687465784\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">Company No:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">VAT number:\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">JKShop\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Firm\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Jiri Kubak\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{first_name}}   {{last_name}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Address\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{address}} {{address2}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Postcode City\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{postcode}}   {{city}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Country<br>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{country}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">E mail: xx@yy.com\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Email:   {{email}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">Phone: 555 666 222\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">Phone:   {{phone}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Bank Details:<\\/h3>\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t\\t<h3>Delivery Adress:<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Account Number:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">568741316\\/2548\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_first_name}}   {{ds_last_name}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Bank name:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">Europa Bank\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_address}} {{ds_address2}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_postcode}} {{ds_city}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">{{ds_country}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t<h3>Payment   Terms<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Date of Issue:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{date_now}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">No.:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{order_id}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Due Date:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{date_now_14}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\"><br>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"2\\\">Form of Payment:\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"2\\\">{{payment_method}}\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\"><br>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t<h3>Products<\\/h3>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n\\r\\n<tr>\\r\\n\\t<td colspan=\\\"8\\\">\\r\\n\\t\\t{{products}}\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">Total   excl. VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_price_without_tax}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_tax}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\">\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"3\\\">Total   incl. VAT:\\r\\n\\t<\\/td>\\r\\n\\t<td style=\\\"text-align: right;\\\"><strong>{{total_price}}<\\/strong>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td colspan=\\\"4\\\"><br><br><br>Issued: JKShop\\r\\n\\t<\\/td>\\r\\n\\t<td colspan=\\\"4\\\"><br><br><br>Took Over:\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table><p><br>\\r\\n<\\/p>\",\"cash_on_delivery_order_status_before_id\":\"1\",\"cash_on_delivery_order_status_after_id\":\"6\",\"bank_transfer_order_status_before_id\":\"3\",\"bank_transfer_order_status_after_id\":\"4\",\"paypal_order_status_before_id\":\"2\",\"paypal_order_status_after_id\":\"4\",\"currency_char\":\"$\",\"currency_char_position\":\"1\",\"copy_all_order_emails_to\":\"\",\"invoice_template_style\":\".product-title { width: 315px; display: inline-block; }\\r\\n.product-quantity { width: 50px; display: inline-block; }\\r\\n.product-price-without-tax { width: 100px; display: inline-block; text-align: right; }\\r\\n.product-tax { width: 100px; display: inline-block; text-align: right; }\\r\\n.product-price { width: 130px; display: inline-block; text-align: right; }\\r\\ntable { width: 100%; border-collapse: collapse;}\\r\\ntd, th { border: 1px solid #ccc; }\",\"cash_on_delivery_active\":\"1\",\"bank_transfer_active\":\"1\",\"paypal_active\":\"0\",\"paypal_currency_code\":\"USD\",\"paypal_return_url\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `jkshop_ds_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_ds_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_is_county` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_contact_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jkshop_contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_login_unique` (`username`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`),
  KEY `users_login_index` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_groups_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Guest', 'guest', 'Default group for guest users.', '2018-04-08 01:28:21', '2018-04-08 01:28:21'),
(2, 'Registered', 'registered', 'Default group for registered users.', '2018-04-08 01:28:21', '2018-04-08 01:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_throttle`
--

DROP TABLE IF EXISTS `user_throttle`;
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_throttle_user_id_index` (`user_id`),
  KEY `user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jiri_jkshop_orders`
--
ALTER TABLE `jiri_jkshop_orders`
  ADD CONSTRAINT `jiri_jkshop_orders_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `jiri_jkshop_coupons` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jiri_jkshop_orders_payment_gateway_id_foreign` FOREIGN KEY (`payment_gateway_id`) REFERENCES `jiri_jkshop_payment_gateways` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `jiri_jkshop_payment_gateways`
--
ALTER TABLE `jiri_jkshop_payment_gateways`
  ADD CONSTRAINT `jiri_jkshop_payment_gateways_order_status_after_id_foreign` FOREIGN KEY (`order_status_after_id`) REFERENCES `jiri_jkshop_order_statuses` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jiri_jkshop_payment_gateways_order_status_before_id_foreign` FOREIGN KEY (`order_status_before_id`) REFERENCES `jiri_jkshop_order_statuses` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `jiri_jkshop_property_options`
--
ALTER TABLE `jiri_jkshop_property_options`
  ADD CONSTRAINT `jiri_jkshop_property_options_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `jiri_jkshop_properties` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
