<?php namespace Jiri\JKShop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class update_173 extends Migration
{

    public function up()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->boolean('virtual_product')->default(0);
        });
        
        Schema::table('jiri_jkshop_order_statuses', function($table)
        {
            $table->boolean('paid')->default(0);
        });
    }

    public function down()
    {
        Schema::table('jiri_jkshop_products', function($table)
        {
            $table->dropColumn('virtual_product');
        });
        Schema::table('jiri_jkshop_order_statuses', function($table)
        {
            $table->dropColumn('paid');
        });
    }

}
