<?php namespace Jiri\JKShop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class update_170 extends Migration
{

    public function up()
    {
        Schema::table('jiri_jkshop_products_options', function($table)
        {
            $table->integer('extended_quantity')->nullable();
        });
    }

    public function down()
    {
        Schema::table('jiri_jkshop_products_options', function($table)
        {
            $table->dropColumn('extended_quantity');
        });
    }

}
