<?php namespace Jiri\JKShop\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use URL;
use Auth;


class ProductDetail extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Product Detail',
            'description' => 'Display detail of product'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'URL Slug',
                'description' => 'URL Slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],    
            'brandPage' => [
                'title'       => 'Brand page',
                'description' => 'List of products by brand',
                'type'        => 'dropdown',
                'default'     => 'brands',
                'group'       => 'Links',
            ],           
        ];
    }
    
    public function getBrandPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }    
    
    public function onRun()
    {
        $slug = $this->property('slug');
        $product = \Jiri\JKShop\Models\Product::where('slug', $slug)->where("visibility", 1)->first();
        
        if ($product == null) {
            return \Response::make($this->controller->run('404'), 404);
        }
        
        // add featured links
        $featured = $product->featured;
        $featured->each(function($featuredProduct) {
            $featuredProduct->setUrl($this->page->id, $this->controller);
        });         
        // add accessories links
        $accessories = $product->accessories;
        $accessories->each(function($accessoriesProduct) {
            $accessoriesProduct->setUrl($this->page->id, $this->controller);
        });
        
        $this->page['product'] = $product;
        $this->page['featured'] = $featured;
        $this->page['accessories'] = $accessories;
        
        // jkshopsetting
        $this->page['jkshopSetting'] = \Jiri\JKShop\Models\Settings::instance();
        
        // brand url
        if ($product->brand != null) {
            $params = [
                'brand' => $product->brand->slug,
            ];
            $product->brand->url = $this->controller->pageUrl($this->property('brandPage'), $params);
        }
        else {
            //$product->brand->url = null;
        }        
        
        // meta info
        if (isset($product)) {
            $this->page->meta_title = $product->meta_title;
            $this->page->meta_description = $product->meta_description;
            $this->page->meta_keywords = $product->meta_keywords;
        }
    }
    
    /**
     * Update price, desc and title by property options
     * 
     * @return array
     */
    public function onChangePropertyOption() {
        $id = post("id");
        $options = post("options");
        $user = Auth::getUser();

        $productOptionTitle = "";
        $productDetailDescription = "";
        $optionImages = [];
        
        $product = \Jiri\JKShop\Models\Product::find($id);
        $quantity = $product->quantity;
        $productPropertyOptions = $product->getProductPropertyOptions($options);
        foreach ($productPropertyOptions as $productPropertyOption) {
            // title
            if (($productPropertyOption->pivot["title"]) && ($productPropertyOption->pivot["title"] != "")) {
                if ($productOptionTitle) {
                    $productOptionTitle .= ", ".$productPropertyOption->pivot["title"];
                }
                else {
                    $productOptionTitle = $productPropertyOption->pivot["title"];
                }
            }

            // desc
            if (($productPropertyOption->pivot["description"]) && ($productPropertyOption->pivot["description"] != "")) {
                $productDetailDescription .= $productPropertyOption->pivot["description"];
            }
            
            // image
            if (($productPropertyOption->pivot["image"]) && ($productPropertyOption->pivot["image"] != "")) {
                $optionImages[] = URL::to("storage/app/media".$productPropertyOption->pivot["image"]);
            }
            
            // quantity
            if (isset($productPropertyOption->pivot["extended_quantity"])) {
                $quantity = $productPropertyOption->pivot["extended_quantity"];
            }
        }        

        
        
        $rtnData = [];
        
        // title
        $rtnData["#product-option-title"] = $productOptionTitle;
        
        // desc
        $rtnData["#product-option-description"] = $productDetailDescription;
        
        // price
        $rtnData["#product-price"] = $product->getFinalPriceFormated($options);
        
        // product quantity
        
        $rtnData["#product-quantity"] = $this->renderPartial('@product-quantity', [ "final_quantity" => $quantity, "availability_date" => $product->availability_date ]);
        
        // isAllowOrder
        $isAllowOrderMsg = "";
        $isAllowOrder = $product->isAllowOrder($quantity);
        if (($product->virtual_product) && ($user == null)) {
            $isAllowOrder = false;
            $isAllowOrderMsg = "Virtual product can be order only by registered user";
        }
        $rtnData["#product-allow-order"] = $this->renderPartial('@product-allow-order', [ "is_allow_order" => $isAllowOrder, "is_allow_order_msg" => $isAllowOrderMsg ]);
        
        // property images
        $rtnData["product-option-images"] = $optionImages;
        
        // refresh main image
        if (count($optionImages) > 0) {
            $rtnData["product-main-image"] = $optionImages[0];
        }
        else {
            $rtnData["product-main-image"] = $product->getMainImagePath();
        }
        
        return $rtnData;
    }

}