<?php

namespace Renatio\SeoManager\Console;

use AnandPatel\SeoExtension\models\Settings as SeoExtensionSettings;
use Carbon\Carbon;
use Cms\Classes\Page;
use Illuminate\Console\Command;
use RainLab\Blog\Models\Category;
use RainLab\Blog\Models\Post;
use RainLab\Pages\Classes\Page as StaticPage;
use Renatio\SeoManager\Models\SeoTag;
use Renatio\SeoManager\Models\Settings as SeoManagerSettings;
use System\Classes\PluginManager;

/**
 * Class Import
 * @package Renatio\SeoManager\Console
 */
class Import extends Command
{

    /**
     * @var string
     */
    protected $name = 'seo:import';

    /**
     * @var string
     */
    protected $description = 'Import default meta values for pages and blog';

    /**
     * @return void
     */
    public function handle()
    {
        $this->importSeoFromCmsPages();

        $this->importSeoFromStaticPages();

        $this->importSeoFromBlog();

        $this->importSettings();
    }

    /**
     * @return void
     */
    protected function importSeoFromCmsPages()
    {
        foreach (Page::all() as $page) {
            $page->meta_title = $page->meta_title ?: $page->title;
            $page->meta_description = $page->meta_description ?: $page->title;
            $page->meta_keywords = $page->seo_keywords;
            $page->robot_index = $page->robot_index ?: 'index';
            $page->robot_follow = $page->robot_follow ?: 'follow';

            unset($page->seo_keywords);

            $page->forceSave();
        }
    }

    /**
     * @return void
     */
    protected function importSeoFromStaticPages()
    {
        if ($this->isPluginInstalled('RainLab.Pages')) {
            foreach (StaticPage::all() as $page) {
                $viewBag = $page->viewBag;

                $viewBag['meta_title'] = ! empty($viewBag['seo_title']) ? $viewBag['seo_title'] : $page->title;
                $viewBag['meta_description'] = ! empty($viewBag['seo_description']) ? $viewBag['seo_description'] : $page->title;
                $viewBag['meta_keywords'] = ! empty($viewBag['seo_keywords']) ? $viewBag['seo_keywords'] : '';
                $viewBag['robot_index'] = ! empty($viewBag['robot_index']) ? $viewBag['robot_index'] : 'index';
                $viewBag['robot_follow'] = ! empty($viewBag['robot_follow']) ? $viewBag['robot_follow'] : 'follow';
                unset($viewBag['seo_title'], $viewBag['seo_description'], $viewBag['seo_keywords']);

                $data['settings'] = ['viewBag' => $viewBag];

                $page->fill($data);
                $page->forceSave();
            }
        }
    }

    /**
     * @return void
     */
    protected function importSeoFromBlog()
    {
        if ($this->isPluginInstalled('RainLab.Blog')) {
            $this->importPosts();
            $this->importCategories();
        }
    }

    /**
     * @return void
     */
    protected function importSettings()
    {
        if ($this->isPluginInstalled('AnandPatel.SeoExtension')) {
            $oldSettings = SeoExtensionSettings::instance();

            if ($oldSettings->enable_title) {
                $oldSettings->title_position == 'prefix'
                    ? SeoManagerSettings::set('title_prefix', $oldSettings->title)
                    : SeoManagerSettings::set('title_suffix', $oldSettings->title);
            }

            SeoManagerSettings::set('common_tags', $oldSettings->other_tags);
            SeoManagerSettings::set('og_enabled', $oldSettings->enable_og_tags);
            SeoManagerSettings::set('site_name', $oldSettings->og_sitename);
            SeoManagerSettings::set('fb_app_id', $oldSettings->og_fb_appid);
        }
    }

    /**
     * @return void
     */
    protected function importPosts()
    {
        $this->importRecords(Post::class);
    }

    /**
     * @return void
     */
    protected function importCategories()
    {
        $this->importRecords(Category::class, 'name', 'name');
    }

    /**
     * @param $class
     * @param string $titleFrom
     * @param string $descFrom
     */
    protected function importRecords($class, $titleFrom = 'title', $descFrom = 'excerpt')
    {
        $data = [];

        foreach ($class::all() as $record) {
            $row = [
                'seo_tag_id' => $record->id,
                'seo_tag_type' => get_class($record),
                'meta_title' => str_limit($record->$titleFrom, 255, null),
                'meta_description' => str_limit($record->$descFrom, 255, null),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $data[] = $this->importRecordFromSeoExtension($row, $record);
        }

        SeoTag::insert($data);
    }

    /**
     * @param $plugin
     * @return bool
     */
    protected function isPluginInstalled($plugin)
    {
        return PluginManager::instance()->exists($plugin);
    }

    /**
     * @param $row
     * @param $record
     * @return array
     */
    protected function importRecordFromSeoExtension($row, $record)
    {
        if ($this->isPluginInstalled('AnandPatel.SeoExtension') && $record instanceof Post) {
            return array_merge($row, [
                'meta_title' => str_limit($record->seo_title, 255, null),
                'meta_description' => str_limit($record->seo_description, 255, null),
                'meta_keywords' => $record->seo_keywords,
                'robot_index' => $record->robot_index ?: 'index',
                'robot_follow' => $record->robot_follow ?: 'follow',
                'canonical_url' => $record->canonical_url,
                'redirect_url' => $record->redirect_url,
            ]);
        }

        return $row;
    }

}