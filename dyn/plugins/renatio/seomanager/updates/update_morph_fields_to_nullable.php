<?php

namespace Renatio\SeoManager\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

/**
 * Class UpdateMorphFieldsToNullable
 * @package Renatio\SeoManager\Updates
 */
class UpdateMorphFieldsToNullable extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::table('renatio_seomanager_seo_tags', function ($table) {
            $table->unsignedInteger('seo_tag_id')->nullable()->change();
            $table->string('seo_tag_type')->nullable()->change();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        // do nothing
    }

}