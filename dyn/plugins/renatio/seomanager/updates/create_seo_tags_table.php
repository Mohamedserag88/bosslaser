<?php

namespace Renatio\SeoManager\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

/**
 * Class CreateSeoTagsTable
 * @package Renatio\SeoManager\Updates
 */
class CreateSeoTagsTable extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::create('renatio_seomanager_seo_tags', function ($table) {
            $table->increments('id');
            $table->morphs('seo_tag');
            $table->string('meta_title', 60)->nullable();
            $table->string('meta_description', 160)->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('canonical_url')->nullable();
            $table->string('redirect_url')->nullable();
            $table->string('robot_index', 10)->default('index')->nullable();
            $table->string('robot_follow', 10)->default('follow')->nullable();
            $table->string('robot_advanced', 50)->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_description')->nullable();
            $table->string('og_type', 50)->nullable();
            $table->string('og_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renatio_seomanager_seo_tags');
    }

}