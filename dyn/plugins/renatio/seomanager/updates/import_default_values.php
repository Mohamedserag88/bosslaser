<?php

namespace Renatio\SeoManager\Updates;

use Artisan;
use October\Rain\Database\Updates\Migration;

/**
 * Class ImportDefaultValues
 * @package Renatio\SeoManager\Updates
 */
class ImportDefaultValues extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        if (app()->environment() != 'testing') {
            Artisan::call('seo:import');
        }
    }

    /**
     * @return void
     */
    public function down()
    {
    }

}