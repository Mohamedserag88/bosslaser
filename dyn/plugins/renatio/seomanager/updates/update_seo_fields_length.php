<?php

namespace Renatio\SeoManager\Updates;

use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

/**
 * Class UpdateSeoFieldsLength
 * @package Renatio\SeoManager\Updates
 */
class UpdateSeoFieldsLength extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::table('renatio_seomanager_seo_tags', function ($table) {
            $table->string('meta_title')->change();
            $table->string('meta_description')->change();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        // do nothing
    }

}