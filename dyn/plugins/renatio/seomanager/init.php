<?php

/*
 * 301 Redirect
 */
Event::listen('seo.beforeComponentRender', function ($page, $seoTag) {
    if ($seoTag && $seoTag->redirect_url) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: $seoTag->redirect_url");
    }
});