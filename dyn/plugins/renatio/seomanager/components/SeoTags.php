<?php

namespace Renatio\SeoManager\Components;

use Cms\Classes\ComponentBase;
use Event;
use Model;
use Renatio\SeoManager\Classes\SeoFields;
use Renatio\SeoManager\Models\SeoTag;
use Renatio\SeoManager\Models\Settings;
use Request;

/**
 * Class SeoTags
 * @package Renatio\SeoManager\Components
 */
class SeoTags extends ComponentBase
{

    /**
     * @var
     */
    protected $seoTag;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'renatio.seomanager::lang.component.name',
            'description' => 'renatio.seomanager::lang.component.description'
        ];
    }

    /**
     * @return void
     */
    public function onRender()
    {
        $this->seoTag = $this->findSeoTagInController();

        if ( ! $this->seoTag) {
            $this->seoTag = $this->page;
        }

        $this->prepareSeoTag();

        Event::fire('seo.beforeComponentRender', [$this->page, $this->seoTag]);

        $this->setPageVars();
    }

    /**
     * @return mixed
     */
    protected function findSeoTagInController()
    {
        foreach ($this->controller->vars as $var) {
            if ($this->isCmsModel($var)) {

                $relation = [
                    'seo_tag_id' => $var->id,
                    'seo_tag_type' => get_class($var)
                ];

                if ($seoTag = SeoTag::where($relation)->first()) {
                    return $seoTag;
                }
            }
        }
    }

    /**
     * @param $var
     * @return bool
     */
    protected function isCmsModel($var)
    {
        return is_subclass_of($var, Model::class);
    }

    /**
     * @return void
     */
    protected function setPageVars()
    {
        $this->page['seoTag'] = $this->seoTag;
        $this->page['currentUrl'] = Request::url();
        $this->page['seoSettings'] = Settings::instance();
    }

    /**
     * @return void
     */
    protected function prepareSeoTag()
    {
        $this->setOgImageDimensions();

        $this->setRobots();

        $this->prepareAttributes();
    }

    /**
     * @return void
     */
    protected function setOgImageDimensions()
    {
        $this->seoTag->og_image_width = null;
        $this->seoTag->og_image_height = null;

        $filePath = base_path(config('cms.storage.media.path') . $this->seoTag->og_image);

        if (is_file($filePath)) {
            list($width, $height) = getimagesize($filePath);

            $this->seoTag->og_image_width = $width;
            $this->seoTag->og_image_height = $height;
        }
    }

    /**
     * @return void
     */
    protected function setRobots()
    {
        $this->seoTag->robots = $this->seoTag->robot_index . ', ' . $this->seoTag->robot_follow;

        if ($this->seoTag->robot_advanced) {
            $this->seoTag->robots .= ', ' . $this->seoTag->robot_advanced;
        }
    }

    /**
     * @return void
     */
    protected function prepareAttributes()
    {
        foreach ((new SeoFields)->keys() as $key) {
            if (is_null($this->seoTag->$key)) {
                $this->seoTag->$key = '';
            }
        }
    }

}