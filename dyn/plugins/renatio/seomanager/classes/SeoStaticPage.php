<?php

namespace Renatio\SeoManager\Classes;

use RainLab\Pages\Classes\Page;
use System\Classes\PluginManager;

/**
 * Class SeoStaticPage
 * @package Renatio\SeoManager\Classes
 */
class SeoStaticPage extends SeoCmsPage
{

    /**
     * @var string
     */
    protected $type = Page::class;

    /**
     * @var string
     */
    protected $key = 'viewBag';

    /**
     * @return void
     */
    public function extend()
    {
        if (PluginManager::instance()->exists('RainLab.Pages')) {
            parent::extend();
        }
    }

}