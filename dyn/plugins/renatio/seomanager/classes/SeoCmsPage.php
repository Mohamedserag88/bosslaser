<?php

namespace Renatio\SeoManager\Classes;

use Cms\Classes\Page;
use Illuminate\Support\Facades\Event;
use Renatio\SeoManager\Models\SeoTag;

/**
 * Class SeoCmsPage
 * @package Renatio\SeoManager\Classes
 */
class SeoCmsPage
{

    /**
     * @var string
     */
    protected $type = Page::class;

    /**
     * @var string
     */
    protected $key = 'settings';

    /**
     * @return void
     */
    public function extend()
    {
        $this->extendFields();
    }

    /**
     * @return void
     */
    protected function extendFields()
    {
        Event::listen('backend.form.extendFieldsBefore', function ($widget) {
            $model = $widget->model;

            if ( ! $model instanceof $this->type) {
                return;
            }

            $this->addAssets($widget->getController());

            if (is_array($model->translatable)) {
                $model->translatable = array_merge($model->translatable, $this->getTranslatableFields());
            }

            $widget->removeField($this->key . '[meta_title]');
            $widget->removeField($this->key . '[meta_description]');

            if (is_array($widget->tabs['fields'])) {
                $widget->tabs['fields'] = array_merge($widget->tabs['fields'], $this->fields());
            }
        });
    }

    /**
     * @return array
     */
    protected function fields()
    {
        $fields = (new SeoFields)->fields();

        foreach ($fields as $key => $field) {
            $pageKey = str_replace('seo_tag', $this->key, $key);

            $fields[$pageKey] = $field;
            unset($fields[$key]);
        }

        return $fields;
    }

    /**
     * @return array
     */
    protected function getTranslatableFields()
    {
        $translatable = (new SeoTag)->translatable;

        return array_map(function ($value) {
            return str_replace('seo_tag', $this->key, $value);
        }, $translatable);
    }

    /**
     * @param $controller
     */
    protected function addAssets($controller)
    {
        $controller->addCss('/plugins/renatio/seomanager/assets/css/style.css');
        $controller->addJs('/plugins/renatio/seomanager/assets/js/main.js');
    }

}