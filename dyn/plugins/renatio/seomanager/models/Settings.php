<?php

namespace Renatio\SeoManager\Models;

use Model;

/**
 * Class Settings
 * @package Renatio\SeoManager\Models
 */
class Settings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'renatio_seomanager_settings';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

    /**
     * @return void
     */
    public function initSettingsData()
    {
        $this->og_enabled = true;
    }

    /**
     * @return string
     */
    public function getRobotsAttribute()
    {
        return $this->readFile('robots.txt') ?: $this->setDefaultContentForRobots();
    }

    /**
     * @param $content
     */
    public function setRobotsAttribute($content)
    {
        $this->writeFile('robots.txt', $content);
    }

    /**
     * @return string
     */
    public function getHtaccessAttribute()
    {
        return $this->readFile('.htaccess');
    }

    /**
     * @param $content
     */
    public function setHtaccessAttribute($content)
    {
        $this->writeFile('.htaccess', $content);
    }

    /**
     * @param $path
     * @return string
     */
    protected function readFile($path)
    {
        if (file_exists($path)) {
            return file_get_contents($path);
        }
    }

    /**
     * @param $path
     * @param $content
     */
    protected function writeFile($path, $content)
    {
        file_put_contents($path, $content);
    }

    /**
     * @return void
     */
    protected function setDefaultContentForRobots()
    {
        $default = "User-agent: *\r\nAllow: /";

        file_put_contents('robots.txt', $default);

        return $default;
    }

}