<?php namespace MarcelHaupt\Email;
    
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UserController;
use MarcelHaupt\Email\Models\UserSettings;
use MarcelHaupt\Email\Models\Campaign;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function boot() {
        UserModel::extend(function($model) {
            $model->hasOne['email_settings'] = ["MarcelHaupt\Email\Models\UserSettings"];
            $model->hasMany['send_log'] = ["MarcelHaupt\Email\Models\SendLog"];
            $model->hasMany['action_log'] = ["MarcelHaupt\Email\Models\ActionLog"];
        });
        
        
        UserController::extendFormFields(function ($form, $model, $context) {
            if (!$model instanceof UserModel)
                return;
                
            if (!$model->exists)
                return;
                
            UserSettings::getFromUser($model);
            
            $form->addTabFields([
                'email_settings[is_subscribed]' => [
                    "label" => "Subscribed",
                    "tab" => "Email",
                    "type" => "Switch",
                ],
                'email_settings[token]' => [
                    "label" => "Email token (do not change!)",
                    "tab" => "Email",
                    "type" => "Text",
                ],
                'email_settings[is_blocked]' => [
                    "label" => "Block all outgoing marketing emails to this user",
                    "tab" => "Email",
                    "type" => "Switch",
                ]
            ]);
        });
        
        // Extend all backend list usage
        \Event::listen('backend.list.extendColumns', function($widget) {

            // Only for the User controller
            if (!$widget->getController() instanceof \RainLab\User\Controllers\Users) {
                return;
            }

            // Only for the User model
            if (!$widget->model instanceof \RainLab\User\Models\User) {
                return;
            }

            // Add an extra birthday column
            $widget->addColumns([
                'email_settings[is_subscribed]' => [
                    'type' => 'Switch',
                    'label' => 'Subscribed',
                    'sortable' => true,
                    'searchable' => true,
                ]
            ]);
        });
    }
    
    public function registerComponents()
    {
        return [
            'MarcelHaupt\Email\Components\Unsubscribe' => 'Unsubscribe',
            'MarcelHaupt\Email\Components\Settings' => 'Settings',
            'MarcelHaupt\Email\Components\Action' => 'Action',
            'MarcelHaupt\Email\Components\Cron' => 'Cron'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Emails',
                'description' => 'Marketing Email Settings',
                'category'    => 'Emails',
                'icon'        => 'icon-envelope',
                'class'       => 'MarcelHaupt\Email\Models\Settings',
                'order'       => 500,
                'permissions' => ['marcelhaupt.email.backend_permission'],
            ]
        ];
    }
    
    public function registerMailTemplates()
    {
        return [
            'marcelhaupt.email::mail.sample'  => 'A Sample Newsletter',
        ];
    }
    
    // send away emails every hour
    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            $camp = Campaign::where('is_active', '1')->get();
            foreach($camp as $c) {
                $rec = $c->selectUsers();
                $c->sendEmail($rec);
            }
        })->cron('*/15 * * * *');
    }
}
