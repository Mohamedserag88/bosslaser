<?php namespace MarcelHaupt\Email\Models;

use Model;
use MarcelHaupt\Email\Models\Action;
use MarcelHaupt\Email\Models\SendLog;
use MarcelHaupt\Email\Models\Campaign;

/**
 * Model
 */
class ActionLog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_action_log';
    
    public $belongsTo = [
       'user' => ["RainLab\User\Models\User"],
       'action' => ['MarcelHaupt\Email\Models\Action']
    ];
    
    
    public function trigger() 
    {
        //set own triggered state
        if ($this->is_triggered) return;
        $this->is_triggered = true;
        $this->save();
        
        
        //increment action click
        if (!$this->action) return;
        Action::where('id', $this->action->id)->increment('clicks');
        
        
        //increment campaign clicks
        if (!$this->action->campaign) return;
        
        //check if there are other actions that have been clicked
        $this_id = $this->id;
        $this_userid = $this->user_id;
        if (Action::where('campaign_id', $this->action->campaign->id)->whereHas('action_log', function($query) use ($this_id, $this_userid) {
            $query->where('id', '<>', $this_id)->where('user_id', $this_userid)->where('is_triggered', 1);
        })->exists()) return;
        
        Campaign::where('id', $this->action->campaign->id)->increment('emails_clicked');
        
        
        //check if email has been marked opened - mark open if not
        if (!$this->user) return;
        $sl = SendLog::where('user_id', $this->user->id)->where('campaign_id', $this->action->campaign->id)->first();
        if (!$sl) return;
        if ($sl->is_opened) return;
        $sl->open();
    }
}