<?php namespace MarcelHaupt\Email\Models;

use Model;
use MarcelHaupt\Email\Models\Settings as EmailSettings;

/**
 * Model
 */
class UserSettings extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_user_settings';
    
    public $belongsTo = [
       'user' => ["RainLab\User\Models\User"]
    ];
    
    public static function getFromUser($user) {
        if ($user->email_settings) {
            return $user->email_settings;
        }
        
        $email_settings = new static;
        $email_settings->user = $user;
        $email_settings->is_subscribed = EmailSettings::get('is_default_subscribed', false);
        $email_settings->is_blocked = false;
        $email_settings->token = \Str::random(20);
        $email_settings->save();
        
        $user->email_settings = $email_settings;
        
        return $email_settings;
    }
}