<?php namespace MarcelHaupt\Email\Models;

use Model;
use MarcelHaupt\Email\Models\Campaign;

/**
 * Model
 */
class SendLog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_send_log';
    
    
    public $belongsTo = [
       'user' => ["RainLab\User\Models\User"],
       'campaign' => ['MarcelHaupt\Email\Models\Campaign']
    ];
    
    
    public function open() 
    {
        
        //mark self as open
        if ($this->is_opened) return;
        $this->is_opened = true;
        $this->save();
        
        //increment campaign open
        if (!$this->campaign) return;
        Campaign::where('id', $this->campaign->id)->increment('emails_opened');
    }
}