<?php namespace MarcelHaupt\Email\Models;

use Model;
use Mail;
use RainLab\User\Models\User;
use MarcelHaupt\Email\Models\Condition;
use MarcelHaupt\Email\Models\SendLog;
use MarcelHaupt\Email\Models\UserSettings;
use MarcelHaupt\Email\Models\ActionLog;
use MarcelHaupt\Email\Models\Settings;

/**
 * Model
 */
class Campaign extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        'slug'    => 'required|between:2,255|unique:marcelhaupt_email_campaign,slug',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_campaign';
    
    public $hasMany = [
        'conditions' => ['MarcelHaupt\Email\Models\Condition', 'key'=>'campaign_id'],
        'actions' => ['MarcelHaupt\Email\Models\Action', 'key'=>'campaign_id'],
        'send_log' => ['MarcelHaupt\Email\Models\SendLog', 'key'=>'campaign_id'],
    ];
    
    
    public function selectUsers() {
        //only select users, that have no send entry
        //$user = User::whereNotExists(function($query) { 
        //    $query->where('campaign_id', $this->id);
        //})->get();
        $user = User::get();

        //empty receiver array            
        $receivers = array();
        
        
        $number_incl_cond = Condition::where('campaign_id', $this->id)->where('property', 'incl')->count();
        $number_excl_cond = Condition::where('campaign_id', $this->id)->where('property', 'excl')->count();
        
        foreach($user as $u) 
        {
            //skip if user has already gotten email
            $continue = $u->send_log->first(function ($model) {
                return $model->campaign_id == $this->id;   
            });
            if ($continue) continue;
            
            
            // all = true // any == false (true if no condition)
            $include = ($this->incl_method == 'all' || $number_incl_cond == 0); 
            // all = true (false if no condition) // any == false
            $exclude = ($this->excl_method == 'all' && $number_excl_cond != 0); 
            
            //include conditions
            foreach($this->conditions as $condition) 
            {
                $result = false;
                
                ////////////////////////////////////////////////////////////////
                // CONDITION LOGIC
                // 
                
                if ($condition->type == 'acc_days_old') 
                {
                    $now = date_create();
                    $then = date_create($u->created_at);
                    $diff = date_diff($now, $then, true);
                    
                    $diff_h = $diff->days*24 + $diff->h;
                    
                    //true / false
                    $result = $diff_h >= intval($condition->content)*24;
                } 
                
                else if ($condition->type == 'not_logged_in') {
                    $last_login = $u->last_login;
                    if (!$last_login) 
                    {
                        $last_login = $u->created_at;
                    }
                    
                    $now = date_create();
                    $then = date_create($u->last_login);
                    $diff = date_diff($now, $then, true);
                    
                    $diff_h = $diff->days*24 + $diff->h;
                    
                    //true / false
                    $result = $diff_h >= intval($condition->content)*24;
                }
                
                else if ($condition->type == 'in_user_group') 
                {
                    
                    //true / false
                    $result = $u->groups()->whereCode($condition->content)->exists();
                }
                
                else if ($condition->type == 'has_opened') 
                {
                    //true / false
                    $sl = self::where('slug', $condition->content)->whereHas('send_log', function($query) use ($u) {
                        $query->where('user_id', $u->id)->where('is_opened', 1);
                    })->exists();
                    
                    $result = $sl;
                }
                
                else if ($condition->type == 'has_clicked') 
                {
                    //true / false
                    $al = self::where('slug', $condition->content)->whereHas('actions', function($query) use ($u) {
                        $query->whereHas('action_log', function($query) use ($u) {
                            $query->where('user_id', $u->id)->where('is_triggered', 1);
                        });
                    })->exists();
                    $result = $al;
                }
                
                else if ($condition->type == 'has_received') 
                {
                    //true / false
                    $sl = self::where('slug', $condition->content)->whereHas('send_log', function($query) use ($u) {
                        $query->where('user_id', $u->id);
                    })->exists();
                    
                    $result = $sl;
                }
                
                else if ($condition->type == 'is_delay') 
                {
                    //true / false
                    $sl = SendLog::where('user_id', $u->id)->orderBy('created_at', 'DESC')->first();
                    
                    if ($sl) 
                    {
                        $now = date_create();
                        $then = date_create($sl->created_at);
                        $diff = date_diff($now, $then, true);
                        
                        $diff_h = $diff->days*24 + $diff->h;
                        
                        //true / false
                        $result = $diff_h >= intval($condition->content)*24;
                    } 
                    else 
                    {
                        $result = true;
                    }
                }
                
                //
                // CONDITION LOGIC
                ////////////////////////////////////////////////////////////////
                
                if ($condition->property == 'incl') 
                {
                    if ($this->incl_method == 'all') 
                    {
                        $include = $include && $result;
                    } 
                    else 
                    {
                        $include = $include || $result;
                    }
                } 
                else 
                {
                    if ($this->excl_method == 'all') 
                    {
                        $exclude = $exclude && $result;
                    } 
                    else 
                    {
                        $exclude = $exclude || $result;
                    }
                }
            }
            
            
            if ($include && !$exclude) 
            {
                //take user
                $receivers[] = $u;
            }
        }
        
        return $receivers;
    }
    
    
    //testing needed
    public function sendEmail($receivers) 
    {
        foreach($receivers as $rec) 
        {
            $email_vars = [];
            
            //create send log
            $sl = new SendLog();
            $sl->user = $rec;
            $sl->campaign = $this;
            $sl->pixel_token = \Str::random(30);
            $sl->save();
            
            
            //abort, if user is unsubscribed
            $es = UserSettings::getFromUser($rec);
            if (!$es->is_subscribed || $es->is_blocked) 
            {
                continue;
            }
            
            
            //create actions and actionlogs
            $email_actions = array();
            foreach($this->actions as $action) 
            {
                $al = new ActionLog();
                $al->user = $rec;
                $al->action = $action;
                $al->token = \Str::random(30);
                $al->save();
                
                $email_vars['email_action_' . $action->slug] = $this->generateActionLink('a', $al->id, $al->token);
            }
            
            
            //main email vars
            $email_vars['user'] = $rec;
            $email_vars['email_pixel'] = $this->generateActionLink('p', $sl->id, $sl->pixel_token);
            $email_vars['email_unsubscribe'] = $this->generateUnsubscribeLink($rec->id, $es->token);
            $email_vars['email_sender'] = Settings::get('sender_email', '');
            $email_vars['email_sender_name'] = Settings::get('sender_name', '');
            $email_vars['subject'] = $this->subject;
            

            //send email
            //Mail::pretend(); - does not work with queue
            Mail::queue($this->email_template, $email_vars, function($message) use ($rec) {
            
                $message->from(Settings::get('sender_email', ''), Settings::get('sender_name', ''));
                $message->to($rec->email, $rec->name . ' ' . $rec->surname);
                $message->subject($this->subject);
                
            });
            
            
            //increment campaign sent mail
            self::where('id', $this->id)->increment('emails_sent');
        }
    }
    
    public function sendTestMail($email) 
    {
        $email_vars = [];
        
        foreach($this->actions as $action) 
        {
            $email_vars['email_action_' . $action->slug] = $action->redirect_url;
        }
        
        $rec = User::first();
        
        //main email vars
        $email_vars['user'] = $rec;
        $email_vars['email_pixel'] = "";
        $email_vars['email_unsubscribe'] = Settings::get('unsubscribe_link', '');
        $email_vars['email_sender'] = Settings::get('sender_email', '');
        $email_vars['email_sender_name'] = Settings::get('sender_name', '');
        $email_vars['subject'] = $this->subject;
        
        //send email
        //Mail::pretend(); - does not work with queue
        Mail::queue($this->email_template, $email_vars, function($message) use ($rec, $email) {
        
            $message->from(Settings::get('sender_email', ''), Settings::get('sender_name', ''));
            $message->to($email);
            $message->subject($this->subject);
            
        });
    }
    
    
    public function generateUnsubscribeLink($uid, $token) 
    {
        return Settings::get('unsubscribe_link', '') . "?id=" . urlencode($uid) . "&token=" . urlencode($token) . "&c=" . urlencode($this->id);
    }
    
    public function generateActionLink($type, $id, $token) 
    {
        return Settings::get('action_link', '') . "?t=" . urlencode($type) . "&id=" . urlencode($id) . "&token=" . urlencode($token);
    }
}