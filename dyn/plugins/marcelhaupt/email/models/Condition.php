<?php namespace MarcelHaupt\Email\Models;

use Model;

/**
 * Model
 */
class Condition extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_condition';
    
    
    public $belongsTo = [
        'campaign' => ['MarcelHaupt\Email\Models\Campaign']
    ];
}