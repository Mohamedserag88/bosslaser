<?php namespace MarcelHaupt\Email\Models;

use Lang;
use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'email_settings';
    public $settingsFields = 'fields.yaml';


    public function initSettingsData()
    {
        $this->is_default_subscribed = true;
        $this->unsubscribe_link = 'https://your-domain.com/unsubscribe';
        $this->action_link = 'https://your-domain.com/email_action';
        $this->sender_email = 'info@your-domain.com';
        $this->sender_name = 'Your Company';
    }
}