<?php namespace MarcelHaupt\Email\Models;

use Model;

/**
 * Model
 */
class Action extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
        //'slug' => 'required|between:2,255|unique:marcelhaupt_email_actions,slug',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'marcelhaupt_email_action';
    
    
    public $belongsTo = [
        'campaign' => ['MarcelHaupt\Email\Models\Campaign']
    ];
    
    public $hasMany = [
        'action_log' => ['MarcelHaupt\Email\Models\ActionLog', 'key'=>'action_id'],
    ];
}