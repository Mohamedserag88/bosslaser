<?php
namespace MarcelHaupt\Email\Components;

use MarcelHaupt\Email\Models\Campaign;

class Cron extends \Cms\Classes\ComponentBase
{
    
    public function componentDetails()
    {
        return [
            'name' => 'Email Cron',
            'description' => 'Alternative Email Sending Schedule'
        ];
    }
    
    public function onRun() 
    {
        
        $camp = Campaign::where('is_active', '1')->get();
        
        foreach($camp as $c) 
        {
            //echo "<h1>" . $c->name . "</h1>";
            
            $rec = $c->selectUsers();
            
            //foreach($rec as $u) {
            //    echo $u->email . '<br>';
            //}
            
            $c->sendEmail($rec);
        }
        
    }
    
}

?>