<?php
namespace MarcelHaupt\Email\Components;

use MarcelHaupt\Email\Models\ActionLog;
use MarcelHaupt\Email\Models\SendLog;

class Action extends \Cms\Classes\ComponentBase
{
    
    
    public function componentDetails()
    {
        return [
            'name' => 'Action',
            'description' => 'User Email Action'
        ];
    }
    
    public function onRun() {
        $data = get();
        
        if (!array_key_exists('t', $data) || !array_key_exists('id', $data) || !array_key_exists('token', $data)) {
            return \Redirect::to('404');
        }
        
        
        /////// PIXEL
        if ($data['t'] == 'p') {
            $sl = SendLog::where('id', $data['id'])->where('pixel_token', $data['token'])->first();
            if ($sl) {
                $sl->open();
            }
            return \Redirect::to(\Config::get('cms.pluginsPath') . "/marcelhaupt/email/assets/images/pixel.jpg");
        } 
        
        
        ////// ACTION
        else if ($data['t'] == 'a') {
            $al = ActionLog::where('id', $data['id'])->where('token', $data['token'])->first();
            if ($al) {
                $al->trigger();
                if ($al->action) {
                    return \Redirect::to($al->action->redirect_url);
                } else {
                    return \Redirect::to('404');
                }
            } else {
                return \Redirect::to('404');
            }
        }
        
        
        ////// ELSE
        else {
            return \Redirect::to('404');
        }
        
        
    }
    
}  
    
?>