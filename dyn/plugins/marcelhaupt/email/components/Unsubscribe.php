<?php
namespace MarcelHaupt\Email\Components;
use RainLab\User\Models\User;
use MarcelHaupt\Email\Models\UserSettings;
use MarcelHaupt\Email\Models\Campaign;
use Redirect;


class Unsubscribe extends \Cms\Classes\ComponentBase
{
    
    public function componentDetails()
    {
        return [
            'name' => 'Unsubscribe',
            'description' => 'Unsubscribe functionality'
        ];
    }
    
    
    
    public function onRun() {
        
        $data = get();
        $post_data = post();
        
        if (!array_key_exists("id", $data) || !array_key_exists("token", $data)) {
            return Redirect("404");
        }
        
        //get user
        $user = User::where("id", $data['id'])->first();
        if (!$user) {
            return Redirect("404");
        }
        
        //check token
        $us = UserSettings::getFromUser($user);
        if (!$us) {
            return Redirect("404");
        }
        if ($us->token != $data['token']) {
            return Redirect("404");
        }
        
        
        // present user a button first
        if (!array_key_exists("affirm", $post_data)) 
        {
            $this->page['unsubscribe_affirm'] = true;
        } 
        
        // unsubscribe user
        else {
            //tell campaign
            if (array_key_exists("c", $data) && $us->is_subscribed) {
                Campaign::where("id", $data['c'])->increment('user_unsubscribed');
            }
            
            //set user unsubscribed
            $us->is_subscribed = 0;
            $us->save();
            
            $this->page['unsubscribe_affirm'] = false;
        }
    }
}

?>