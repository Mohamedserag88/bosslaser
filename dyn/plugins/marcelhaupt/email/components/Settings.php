<?php
namespace MarcelHaupt\Email\Components;

class Settings extends \Cms\Classes\ComponentBase
{
    
    
    public function componentDetails()
    {
        return [
            'name' => 'Settings',
            'description' => 'User Email Notification Settings'
        ];
    }
    
    function onRun() {
        //create user settings
        if (\Auth::check()) {
            $user = \Auth::getUser();
            \MarcelHaupt\Email\Models\UserSettings::getFromUser($user);
        }
    }
    
    function onReceiveNotificationSettings() {
        if (\Auth::check()) {
            $user = \Auth::getUser();
            $us = \MarcelHaupt\Email\Models\UserSettings::getFromUser($user);
            
            $sub = (post('email_sub')) ? 1 : 0;
            
            $us->is_subscribed = $sub;
            $us->save();
        }
    }
    
}