<?php namespace MarcelHaupt\Email\Controllers;

use MarcelHaupt\Email\Models\Condition;
use MarcelHaupt\Email\Models\Campaign;
use RainLab\User\Models\User;
use Backend\Classes\Controller;
use BackendMenu;

use MarcelHaupt\Email\Models\Settings as EmailSettings;

class Emails extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController', 'Backend.Behaviors.RelationController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'marcelhaupt.email.campaign_management' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('MarcelHaupt.Emails', 'emails');
    }
    
    public function getcsv() {
        $csv = "email,";
        $user = User::get();
        
        foreach($user as $u) {
            $csv .= "\n" . $u->email;
        }
        
        return \Response::make($csv)
                ->header('Content-Type', 'application/octet-stream')
                ->header('Content-Disposition', 'attachment; filename="email-list.csv"');
    }
    
    public function index() {
        //download email list
        if (array_key_exists('dlcsv', $_GET)) {
            return $this->getcsv();
        }
        
        parent::index();
        
        
        $total_sent = 0;
        $total_openings = 0;
        $total_clicks = 0;
        
        $campaigns = Campaign::get();
        foreach ($campaigns as $c) {
            $total_sent += $c->emails_sent;
            $total_openings += $c->emails_opened;
            $total_clicks += $c->emails_clicked;
        }
        
        //get user count and unsubscribed count
        $user_count = User::whereHas('email_settings', function($query) {
            $query->where('is_blocked', 0);
        })->count();
        
        $unsub_count = User::whereHas('email_settings', function($query) {
            $query->where('is_blocked', 0)->where('is_subscribed', 0);
        })->count();
        
        
        //set main variables;
        $this->vars['total_sent'] = $total_sent;
        $this->vars['total_openings'] = $total_openings;
        $this->vars['total_clicks'] = $total_clicks;
        $this->vars['user_count'] = $user_count;
        $this->vars['unsub_count'] = $unsub_count;
    }
    
    public function onConditionAdd() {
        $c = Campaign::where('id', $this->params[0])->first();
        if (!$c) return;
        
        $cd = new Condition();
        $cd->campaign_id = $c->id;
        $cd->property = post('property');
        $cd->type = post('type');
        $cd->content = post('content');
        $cd->save();
        
        return $this->onGetConditions();
    }
    
    public function onConditionRemove() {
        Condition::where('id', post('id'))->delete();
        return $this->onGetConditions();
    }
    
    public function onGetConditions() {
        $c = Campaign::where('id', $this->params[0])->first();
        return $c->conditions;
    }
    
    public function onSendTestEmail() {
        $c = Campaign::where('id', $this->params[0])->first();
        if (!$c) return;
        $email = \BackendAuth::getUser()->email;
        if (!$email) return;
        
        $c->sendTestMail($email);
        
    }
}