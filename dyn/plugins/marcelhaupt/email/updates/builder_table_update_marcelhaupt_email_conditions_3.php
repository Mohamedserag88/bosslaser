<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailConditions3 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_conditions', function($table)
        {
            $table->dropColumn('content2');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_conditions', function($table)
        {
            $table->string('content2', 255)->nullable();
        });
    }
}
