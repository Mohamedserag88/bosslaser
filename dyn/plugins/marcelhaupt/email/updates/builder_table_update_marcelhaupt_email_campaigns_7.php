<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaigns7 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->renameColumn('active', 'is_active');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->renameColumn('is_active', 'active');
        });
    }
}
