<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActions extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->integer('triggered_at');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->dropColumn('triggered_at');
        });
    }
}
