<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailAction extends Migration
{
    public function up()
    {
        Schema::rename('marcelhaupt_email_actions', 'marcelhaupt_email_action');
    }
    
    public function down()
    {
        Schema::rename('marcelhaupt_email_action', 'marcelhaupt_email_actions');
    }
}
