<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActions3 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->string('slug', 255);
            $table->string('type', 255);
            $table->dropColumn('user_id');
            $table->dropColumn('token');
            $table->dropColumn('action');
            $table->dropColumn('triggered_at');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->dropColumn('slug');
            $table->dropColumn('type');
            $table->integer('user_id');
            $table->string('token', 255);
            $table->string('action', 255);
            $table->integer('triggered_at');
        });
    }
}
