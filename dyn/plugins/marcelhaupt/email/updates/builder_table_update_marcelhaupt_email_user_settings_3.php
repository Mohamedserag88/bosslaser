<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailUserSettings3 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->renameColumn('subscribed', 'is_subscribed');
            $table->renameColumn('block_emails', 'is_blocked');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->renameColumn('is_subscribed', 'subscribed');
            $table->renameColumn('is_blocked', 'block_emails');
        });
    }
}
