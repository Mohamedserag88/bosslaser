<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActions2 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->integer('campaign_id');
            $table->renameColumn('userid', 'user_id');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->dropColumn('campaign_id');
            $table->renameColumn('user_id', 'userid');
        });
    }
}
