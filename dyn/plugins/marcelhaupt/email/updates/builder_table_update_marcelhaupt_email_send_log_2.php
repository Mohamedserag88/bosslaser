<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailSendLog2 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_send_log', function($table)
        {
            $table->renameColumn('opened', 'is_opened');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_send_log', function($table)
        {
            $table->renameColumn('is_opened', 'opened');
        });
    }
}
