<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaigns3 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->string('add_method')->default('all');
            $table->string('exclude_method')->default('any');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->dropColumn('add_method');
            $table->dropColumn('exclude_method');
        });
    }
}
