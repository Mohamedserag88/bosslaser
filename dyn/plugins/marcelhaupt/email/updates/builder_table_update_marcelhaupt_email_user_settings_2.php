<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailUserSettings2 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->boolean('block_emails')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->dropColumn('block_emails');
        });
    }
}
