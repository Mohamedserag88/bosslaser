<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActionLog extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_action_log', function($table)
        {
            $table->renameColumn('triggered', 'is_triggered');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_action_log', function($table)
        {
            $table->renameColumn('is_triggered', 'triggered');
        });
    }
}
