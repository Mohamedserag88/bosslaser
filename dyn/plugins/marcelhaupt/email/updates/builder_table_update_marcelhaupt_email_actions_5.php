<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActions5 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->integer('clicks')->default(0);
            $table->renameColumn('triggered', 'unique_clicks');
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->dropColumn('clicks');
            $table->renameColumn('unique_clicks', 'triggered');
            $table->string('type', 255);
        });
    }
}
