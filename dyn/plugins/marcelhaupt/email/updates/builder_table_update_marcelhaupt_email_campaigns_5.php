<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaigns5 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
