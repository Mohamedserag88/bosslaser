<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailCampaigns extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_campaigns', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email_template');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_campaigns');
    }
}
