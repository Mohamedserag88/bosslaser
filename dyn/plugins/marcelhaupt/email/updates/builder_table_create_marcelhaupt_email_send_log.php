<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailSendLog extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_send_log', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('campaign_id');
            $table->string('pixel_token');
            $table->boolean('opened');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_send_log');
    }
}
