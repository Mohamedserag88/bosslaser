<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaign2 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_campaign', function($table)
        {
            $table->integer('user_unsubscribed')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_campaign', function($table)
        {
            $table->dropColumn('user_unsubscribed');
        });
    }
}
