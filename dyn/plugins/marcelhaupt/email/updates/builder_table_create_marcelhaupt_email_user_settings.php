<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailUserSettings extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_user_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('subscribed')->default(1);
            $table->string('token')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_user_settings');
    }
}
