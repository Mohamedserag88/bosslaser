<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaign extends Migration
{
    public function up()
    {
        Schema::rename('marcelhaupt_email_campaigns', 'marcelhaupt_email_campaign');
    }
    
    public function down()
    {
        Schema::rename('marcelhaupt_email_campaign', 'marcelhaupt_email_campaigns');
    }
}
