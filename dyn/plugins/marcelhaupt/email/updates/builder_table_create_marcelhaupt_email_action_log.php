<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailActionLog extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_action_log', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('action_id');
            $table->integer('user_id');
            $table->string('token');
            $table->boolean('triggered')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('type')->default('link');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_action_log');
    }
}
