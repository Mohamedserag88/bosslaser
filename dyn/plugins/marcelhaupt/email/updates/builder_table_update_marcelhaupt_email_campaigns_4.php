<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCampaigns4 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->renameColumn('add_method', 'incl_method');
            $table->renameColumn('exclude_method', 'excl_method');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_campaigns', function($table)
        {
            $table->renameColumn('incl_method', 'add_method');
            $table->renameColumn('excl_method', 'exclude_method');
        });
    }
}
