<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailActions6 extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->dropColumn('unique_clicks');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_actions', function($table)
        {
            $table->integer('unique_clicks')->default(0);
        });
    }
}
