<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailSendLog extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_send_log', function($table)
        {
            $table->boolean('opened')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_send_log', function($table)
        {
            $table->boolean('opened')->default(null)->change();
        });
    }
}
