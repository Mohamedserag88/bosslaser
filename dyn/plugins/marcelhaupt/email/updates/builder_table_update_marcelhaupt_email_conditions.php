<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailConditions extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_conditions', function($table)
        {
            $table->renameColumn('method', 'property');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_conditions', function($table)
        {
            $table->renameColumn('property', 'method');
        });
    }
}
