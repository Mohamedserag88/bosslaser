<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailCondition extends Migration
{
    public function up()
    {
        Schema::rename('marcelhaupt_email_conditions', 'marcelhaupt_email_condition');
    }
    
    public function down()
    {
        Schema::rename('marcelhaupt_email_condition', 'marcelhaupt_email_conditions');
    }
}
