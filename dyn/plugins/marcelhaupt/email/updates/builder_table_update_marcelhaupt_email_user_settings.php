<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMarcelhauptEmailUserSettings extends Migration
{
    public function up()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::table('marcelhaupt_email_user_settings', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
