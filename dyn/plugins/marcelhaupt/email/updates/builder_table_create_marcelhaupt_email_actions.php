<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailActions extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_actions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('userid');
            $table->string('token');
            $table->string('action');
            $table->string('redirect_url');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_actions');
    }
}
