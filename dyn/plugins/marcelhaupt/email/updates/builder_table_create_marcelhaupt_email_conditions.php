<?php namespace MarcelHaupt\Email\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMarcelhauptEmailConditions extends Migration
{
    public function up()
    {
        Schema::create('marcelhaupt_email_conditions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('campaign_id');
            $table->string('method');
            $table->string('type');
            $table->string('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('marcelhaupt_email_conditions');
    }
}
