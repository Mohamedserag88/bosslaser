<?php namespace Alexis\Banners\Components;

use Cms\Classes\ComponentBase;
use Alexis\Banners\Models\Config;
use Alexis\Banners\Models\Banner;
use Alexis\Banners\Models\Queue;

class BannerPlace extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'alexis.banners::lang.component.name',
            'description' => 'alexis.banners::lang.component.description',
        ];
    }

    public function defineProperties()
    {
        return [
            'config' => [
                'title'       => 'alexis.banners::lang.component.config',
                'type'        => 'dropdown',
            ]
        ];
    }

    public function getConfigOptions() {
        return Config::lists('title', 'id');
    }

    /**
     * Gets banner id from queue and return banner if queue exist
     *
     * @return array|object
     */
    public function showBanner() {
        $queue = Queue::where('config_id', $this->property('config'))->first();
        $banner = [];
        if ($queue) {
            if (empty($queue->queue)) {
                $queue = $this->generateQueue($queue->config_id);
            }
            $bannersIds = (array)$queue->queue;
            $bannerId = array_shift($bannersIds);

            $queue->queue = $bannersIds;
            $queue->save();
            $banner = Banner::where('id', $bannerId)->first();
            $banner->increment('looks');
        }

        return $banner;
    }

    /**
     * Generates a queue for given config
     *
     * @param $configId
     * @return mixed
     */
    public function generateQueue($configId) {
        $banners = Banner::where('config_id', $configId)->get();
        if (count($banners) > 0) {
            $indices = [];
            $tmpArr = [];
            foreach ($banners as $banner) {
                $tmpArr = range(count($tmpArr), $banner->percents);
                $tmpIndices = array_fill(count($tmpArr), $banner->percents, $banner->id);
                $indices = array_merge($indices, $tmpIndices);
            }
            shuffle($indices);
            $queue = Queue::firstOrNew(['config_id' => $configId]);
            $queue->queue = $indices;
            $queue->save();

            return $queue;
        }
    }

    /**
     * Count number of clicks on banner
     *
     * @return void
     */
    public function onClick() {
        $banner = Banner::find(post('id'));
        $banner->increment('clicks');
    }
}
