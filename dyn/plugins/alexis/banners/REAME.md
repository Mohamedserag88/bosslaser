#Banner manager plugin for October CMS
###Manage banners and it's configurations

Create configuration, to set up width and height of a place to show your banners

Create a banner and assign it to configuration

Add component on a page or layout, where you want to show your banners
```
{% component 'BannerPlace' %}
```
Choose, which configuration component should use.

That's all.