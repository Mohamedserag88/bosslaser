<?php namespace Alexis\Banners\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Alexis\Banners\Models\Banner as BannerModel;
use Alexis\Banners\Models\Queue;

class Banner extends Controller
{
	public $layout = 'banners_list';

    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Alexis.Banners', 'main-menu-item', 'side-menu-item');
    }

    public function index() {
        $this->vars['banners'] = BannerModel::where('dummy', 0)->get();
        $this->asExtension('ListController')->index();
    }
}
