<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexisBannersBanners5 extends Migration
{
    public function up()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->renameColumn('configs_id', 'config_id');
        });
    }
    
    public function down()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->renameColumn('config_id', 'configs_id');
        });
    }
}
