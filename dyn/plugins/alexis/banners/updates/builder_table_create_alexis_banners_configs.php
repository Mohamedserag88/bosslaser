<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexisBannersConfigs extends Migration
{
    public function up()
    {
        Schema::create('alexis_banners_configs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 50)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexis_banners_configs');
    }
}
