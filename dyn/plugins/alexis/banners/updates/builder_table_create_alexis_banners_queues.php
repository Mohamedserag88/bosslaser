<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexisBannersQueues extends Migration
{
    public function up()
    {
        Schema::create('alexis_banners_queues', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('config_id');
            $table->text('queue');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexis_banners_queues');
    }
}
