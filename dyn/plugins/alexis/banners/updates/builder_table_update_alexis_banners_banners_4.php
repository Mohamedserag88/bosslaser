<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexisBannersBanners4 extends Migration
{
    public function up()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->integer('type')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->dropColumn('type');
        });
    }
}
