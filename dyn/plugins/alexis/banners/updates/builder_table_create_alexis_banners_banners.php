<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAlexisBannersBanners extends Migration
{
    public function up()
    {
        Schema::create('alexis_banners_banners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->integer('looks')->default(0);
            $table->integer('clicks')->default(0);
            $table->text('banner_code')->nullable();
            $table->string('link', 255)->nullable();
            $table->integer('show')->default(0);
            $table->boolean('dummy')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('alexis_banners_banners');
    }
}
