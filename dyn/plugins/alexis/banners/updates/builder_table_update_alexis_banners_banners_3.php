<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexisBannersBanners3 extends Migration
{
    public function up()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->integer('configs_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('alexis_banners_banners', function($table)
        {
            $table->dropColumn('configs_id');
        });
    }
}
