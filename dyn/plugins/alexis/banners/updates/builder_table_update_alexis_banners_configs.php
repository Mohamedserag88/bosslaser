<?php namespace Alexis\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAlexisBannersConfigs extends Migration
{
    public function up()
    {
        Schema::table('alexis_banners_configs', function($table)
        {
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('alexis_banners_configs', function($table)
        {
            $table->dropColumn('width');
            $table->dropColumn('height');
        });
    }
}
