<?php return [
    'plugin' => [
        'name' => 'Banner manager',
        'description' => 'Manage banners and it\'s configurations',
    ],
    'banner_list' => [
        'config' => 'Configuration',
        'percents' => 'Percents of shows',
        'title' => 'Title',
    ],
    'banner_fields' => [
        'title' => 'Title',
        'link' => 'Link',
        'percents' => 'Percents of shows',
        'config' => 'Configuration',
        'banner_type' => 'Banner type',
        'banner_type_image' => 'Image',
        'banner_type_code' => 'Code',
        'banner' => 'Banner',
        'banner_code' => 'Banner code',
        'dummy' => 'Show placeholder',
        'dummy_image' => 'Placeholder image',
        'dummy_type' => 'Placeholder type',
        'dummy_placehold' => 'Placeholder from placehold.it',
        'dummy_own' => 'Own image',
        'position' => 'for this config',
    ],
    'config_list' => [
        'title' => 'Title',
    ],
    'config_fields' => [
        'title' => 'Title',
        'width' => 'Width',
        'height' => 'Height',
    ],
    'menu' => [
        'banners' => 'Banners',
        'configs' => 'Configurations',
    ],
    'stats' => [
        'title' => 'Banners statistics',
        'shows' => 'Shows',
        'clicks' => 'Clicks',
    ],
    'messages' => [
        'percents_less' => 'Percents must be less then 100!',
        'percents_amount' => 'Total amount of percents must be less then 100!',
    ],
    'component' => [
        'name'        => 'Banner manager',
        'description' => 'Shows banners according to given configuration',
        'config' => 'Configuration'
    ],
];