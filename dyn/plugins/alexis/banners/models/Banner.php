<?php namespace Alexis\Banners\Models;

use Model;
use Validator;
use Flash;
use Alexis\Banners\Components\BannerPlace;
use Alexis\Banners\Models\Queue;

/**
 * Model
 */
class Banner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'title' => 'required',
        'percents' => 'required|percents',
    ];

    public $customMessages = [
        'percents' => 'Percents are wrong!',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'alexis_banners_banners';

    public $belongsTo = [
        'config' => 'Alexis\Banners\Models\Config'
    ];

    public $attachOne = [
        'banner_image' => 'System\Models\File',
        'dummy_image' => 'System\Models\File'
    ];

    public function filterFields($fields, $context = null) {
        if ($fields->dummy->value == 1) {
            $fields->title->hidden = false;
            $fields->link->hidden = true;
            $fields->percents->hidden = true;
            $fields->config->hidden = false;
            $fields->type->hidden = true;
            $fields->banner_image->hidden = true;
            $fields->banner_code->hidden = true;
            $fields->dummy_type->hidden = false;
            if ($fields->dummy_type->value == 1) {
                $fields->dummy_image->hidden = false;
            } elseif ($fields->dummy_type->value == 0) {
                $fields->dummy_image->hidden = true;
            }

        } elseif ($fields->dummy->value == 0) {
            $fields->type->hidden = false;
            if ($fields->type->value == 1 && $fields->type->hidden == false) {
                $fields->banner_code->hidden = false;
                $fields->banner_image->hidden = true;
            } elseif ($fields->type->value == 0 && $fields->type->hidden == false) {
                $fields->banner_code->hidden = true;
                $fields->banner_image->hidden = false;
            }
            $fields->title->hidden = false;
            $fields->link->hidden = false;
            $fields->percents->hidden = false;
            $fields->config->hidden = false;
            $fields->dummy_type->hidden = true;
            $fields->dummy_image->hidden = true;
        }
    }

    public function afterSave() {
        $controller = new BannerPlace;
        $controller->generateQueue($this->config_id);
    }

    public function afterDelete() {
        Queue::where('config_id', $this->config_id)->delete();
    }

    public function beforeValidate() {
        Validator::extend('percents', function($attribute, $value, $parameters) {
            if ($value > 100) {
                Flash::error(e(trans('alexis.banners::lang.messages.percents_less')));
            }
            $percents = $this->where('config_id', $this->config_id)->where('id', '!=',$this->id)->sum('percents');
            $percents = 100 - $percents;
            if ($value <= $percents) {
                return true;
            }
            Flash::error(e(trans('alexis.banners::lang.messages.percents_amount')));
        });
    }

}