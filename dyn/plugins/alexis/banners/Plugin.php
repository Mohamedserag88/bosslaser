<?php namespace Alexis\Banners;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Alexis\Banners\Components\BannerPlace' => 'BannerPlace'
        ];
    }

    public function registerSettings()
    {
    }
}
