function addOrUpdateQueryParam(search, param, newval) {
    var questionIndex = search.indexOf('?');
    if (questionIndex < 0) {
        search = search + '?';
        search = search + param + '=' + newval;
        return search;
    }
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    var indexOfEquals = query.indexOf('=');
    return (indexOfEquals >= 0 ? query + '&' : query + '') + (newval ? param + '=' + newval : '');
}

function xZoomGallery() {
    $(".xzoom").xzoom({tint: '#333', Xoffset: 15});
    $('#bxslider').bxSlider({
        minSlides: 1,
        slideWidth:90,
        controls: false
    });
}

$(document).ready(function() {
    var currentUrl = window.location.href;

    /**
     * CATEGORY
     */
    //filter
    $('.filter-option').click(function() {
        var filterOptionIdArray = [];
        $('.filter-option').each(function() {
           if ($(this).is(':checked')) {
               filterOptionIdArray.push($(this).val());
           }
        });
        var strFilterOptionId = filterOptionIdArray.join('_');
        var url = addOrUpdateQueryParam(currentUrl, 'filter', strFilterOptionId);
        window.location.href = addOrUpdateQueryParam(url, 'page', 1);//return page 1
    });
    // With JQuery - bootstrap slider http://seiyria.com/bootstrap-slider/
    if (document.getElementById('category-div') !== null) {
        $("#ex2").slider().on('slideStop', function() {
            var minMaxArray = $("#ex2").slider('getValue');
            var min = minMaxArray[0];
            var max = minMaxArray[1];
            //history.pushState(location.reload(), null, url);
            var url = addOrUpdateQueryParam(currentUrl, 'price-range', min+'-'+max);
            window.location.href = addOrUpdateQueryParam(url, 'page', 1);//return page 1
        });
    }
    //sort by
    $('#product-sort-by').change(function() {
        var sortBy = $(this).val();
        window.location.href = addOrUpdateQueryParam(currentUrl, 'sort-by', sortBy);
    });
    //pagination
    $('.cat-pag').click(function() {
        var page = $(this).attr('attr-page');
        window.location.href = addOrUpdateQueryParam(currentUrl, 'page', page);
    });
    jQuery.searchProduct = function() {
        var searchKey = $('#search_product').val();
        if (searchKey != '') {
            var url = addOrUpdateQueryParam(currentUrl, 'key', searchKey);
            window.location.href = addOrUpdateQueryParam(url, 'page', 1);//return page 1
        }
    };
    //Search
    $('#category-fa-search').click(function() {
        $.searchProduct();
    });
    //catch keypress event - enter when search product
    $('#search_product').keypress(function(e){
        if(e.which == 13){
            e.preventDefault();//Enter key pressed
            $.searchProduct();
        }
    });
    //for back button when use push state
    $(window).on("popstate", function (e) {
        location.reload();
    });
    /**
     * END CATEGORY
     */

    /**
     * Product detail
     */

        //handle last option: change price, name, gallery
    jQuery.handleLastOption = function() {
        var activeOption = [];
        $('.filter-option-detail').each(function() {
            if ($(this).hasClass('active')) {
                activeOption.push($(this).attr('attr-option-id'));
            }
        });
        var activeOptionString = activeOption.join(',');
        var configurableData = $('#configurable-data').text();
        var configurableObject = JSON.parse(configurableData);
        var childProduct = configurableObject[activeOptionString];
        //assign product child data: name, price, gallery
        $('.detail-name').text(childProduct.name);
        $('#product-price').text(childProduct.price);

        //use ajax to reload gallery
        $.request('onReloadGallery', {
            data: childProduct,
            update: { 'Product::ajaxGallery' : '#product-gallery'},
            evalSuccess: 'xZoomGallery()' //call function when success /modules/system/assets/js/framework.js
        });
        //end reload gallery

        $('#qty-detail-div').removeClass('class-hidden');

        var buttonBuyNowDetail = $('#buy-now-detail-span');
        buttonBuyNowDetail.attr('attr-name', childProduct.name);
        buttonBuyNowDetail.attr('attr-qty', childProduct.qty);
        buttonBuyNowDetail.attr('attr-qty-order', childProduct.qty_order);
        buttonBuyNowDetail.attr('attr-image', childProduct.featured_image);
        buttonBuyNowDetail.attr('attr-price', childProduct.final_price);
        buttonBuyNowDetail.attr('attr-product-id', childProduct.id);
    };

    //display option next level
    jQuery.displayOptionNextLevel = function(optionNext, currentLevel) {
        var arrayOptionNext = optionNext.split(',');
        var nextLevel = parseInt(currentLevel) + 1;
        $('.level-'+ nextLevel).hide();
        $.each(arrayOptionNext, function(index, value) {
            $('#filter-option-'+value).show();
        })
    }

    //initial option
    if (document.getElementById('configurable-data') !== null) {
        var firstOption = $('.level-1').eq(0);
        var optionNext = firstOption.attr('attr-next');
        var currentLevel = firstOption.attr('attr-level');
        $('.level-1').removeClass('active');
        firstOption.addClass('active');
        $.displayOptionNextLevel(optionNext, currentLevel);
    }

    //configurable
    $('.filter-option-detail').click(function() {
        var optionNext = $(this).attr('attr-next');
        var currentLevel = $(this).attr('attr-level');
        $('.level-'+currentLevel).removeClass('active');
        $(this).addClass('active');
        if (optionNext != '') {
            $.displayOptionNextLevel(optionNext, currentLevel);
        } else {//last option
            $.handleLastOption();
        }
    });

    //display modal review
    $('.modal-review').click(function() {
        $.request('onCaptcha', {
            data: {},
            success: function(res) {
                var imagePath = '/storage/app/media/captcha/'+res.image;
                $('#captcha-image').attr('src', imagePath);
                sessionStorage.setItem('captcha_code', res.code);
                $('#modalReview').modal();
            }
        });
    });

    //submit review
    $('#submit-review').click(function() {
        var captcha = $('#captcha').val();
        var captchaSession = sessionStorage.getItem('captcha_code');
        if (captcha == captchaSession) {
            var formReview = $('#form-review-product').serializeArray();
            $.request('onSubmitReview', {
                data: formReview,
                success: function(res) {
                    $('#modalReview').modal('hide');
                    $.notify('You added review successful', 'success');
                }
            });
        } else {
            $.notify('captcha is not correct');
        }
    });

    /**
     * End product detail
     */

});