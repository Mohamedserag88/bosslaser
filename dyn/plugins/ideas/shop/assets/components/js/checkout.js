/**
 * Calculate separate ship and coupon based on totalPrice
 * then assign to div #ship-money and #coupon-reduce
 * finally: $.assignTotalPriceSpanDiv()
 * (tax : calculate before add-to-cart event)
 */
//find closest number
function closest (num, arr) {
    var mid;
    var lo = 0;
    var hi = arr.length - 1;
    while (hi - lo > 1) {
        mid = Math.floor ((lo + hi) / 2);
        if (arr[mid] < num) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
//    if (num - arr[lo] <= arr[hi] - num) {
//        return arr[lo];
//    }
//    return arr[hi];
    return arr[lo];
}

//function anotherPaymentMethod(paymentMethodId, params) {
//
//}

//generate random string
function generateRandomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
    for (var i = 0; i < parseInt(length); i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}


$(document).ready(function() {

    var baseUrl = window.location.origin;

    var TYPE_PRICE = 1;
    var TYPE_GEO = 2;
    var TYPE_WEIGHT_BASED = 3;
    var TYPE_PER_ITEM = 4;
    var TYPE_GEO_WEIGHT_BASED = 5;

    var WEIGHT_TYPE_FIXED = 1;
    var WEIGHT_TYPE_RATE = 2;

    var totalPriceSpanDiv = 'total-price-span';
    var totalPriceHiddenDiv = 'total-price-hidden';
    var shipMoneyDiv = 'ship-money';
    var couponReduceDiv = 'coupon-reduce';

    var CASH_ON_DELIVERY = 1;
    var PAYPAL = 2;
    var STRIPE = 3;

    var SAVE_ORDER_FAIL = 0;

    var waitMeColor = '#3c66c6';

    var currentUrl = window.location.href;

    jQuery.ajaxCartCheckout = function() {
        var cartRs = sessionStorage.getItem('cart');
        var cartRsObj = JSON.parse(cartRs);
        $.request('onAjaxCart', {
            data: cartRsObj,
            update: { 'Checkout::ajaxCartCheckout' : '#ajax-cart-checkout'}
        });
    };

    if (currentUrl.match(/checkout/)) {//if page cart
        $.ajaxCartCheckout();
    }

    jQuery.getTotalPriceFinal = function() {
        var totalPrice = $('#'+totalPriceHiddenDiv).val();
        var couponMoney = parseFloat($('#'+couponReduceDiv).text());
        var priceShip = parseFloat($('#'+shipMoneyDiv).text());
        var total = parseFloat(totalPrice) + priceShip - couponMoney;
        return total;
    }

    jQuery.assignTotalPriceSpanDiv = function() {
        var total = $.getTotalPriceFinal();
        $('#'+totalPriceSpanDiv).text(total);
    };

    jQuery.calculateShipPrice = function(thisDiv) {
        var type = thisDiv.attr('attr-type');
        var cost = thisDiv.attr('attr-cost');
        var priceShip = 0;
        if (type == TYPE_PRICE || type == TYPE_GEO) {
            priceShip = parseFloat(cost);
        }
        if (type == TYPE_PER_ITEM) {
            var qtyTotal = $('#qty-total-hidden').val();
            priceShip = parseFloat(cost) * qtyTotal;
        }
        if (type == TYPE_WEIGHT_BASED || type == TYPE_GEO_WEIGHT_BASED) {
            var weightTotal = $('#weight-total').text();
            var weightBased = thisDiv.attr('attr-weight-base');
            var weightType = thisDiv.attr('attr-weight-type');
            var weightTotalCeil = parseInt(Math.ceil(weightTotal));
            if (weightType == WEIGHT_TYPE_FIXED) {//fixed
                var weightRateFix = weightBased.split(':');
                var eachUnit = parseInt(weightRateFix[0]);
                var unitPrice = parseFloat(weightRateFix[1]);
                priceShip = (weightTotalCeil / eachUnit) * unitPrice;
            } else {//rate
                var weightRateRate = weightBased.split(',');
                var weightRateArray = [];
                var weightRateArrayIndex = [];
                for (var i=0; i<weightRateRate.length; i++) {
                    var weightRateSplit = weightRateRate[i].split(':');
                    weightRateArray[weightRateSplit[0]] = weightRateSplit[1];
                    weightRateArrayIndex.push(weightRateSplit[0]);
                }
                //find closet lowest in array
                var indexClosest = closest(weightTotalCeil, weightRateArrayIndex);
                priceShip = weightRateArray[indexClosest];
            }
        }
        $('#'+shipMoneyDiv).text(priceShip);
        $.assignTotalPriceSpanDiv();
    };

    /**
     * Choose shipping method
     */
    $('.shipping-method-type').click(function() {
        $.calculateShipPrice($(this))
    });

    //Handle coupon code
    $(document).on('click', '#apply-coupon', function() {
        var coupon = $('#coupon_code').val();
        var cartRs = sessionStorage.getItem('cart');
        var cartRsObj = JSON.parse(cartRs);
        var totalPrice = $('#'+totalPriceHiddenDiv).val();
        var params = {};
        params.cart = cartRsObj;
        params.coupon = coupon;
        params.totalPrice = totalPrice;
        $.request('onCheckCoupon', {
            data: params,
            success: function(res) {
                if (res.rs == false) {
                    $.notify(res.error);
                } else {
                    $('#'+couponReduceDiv).text(totalPrice - res.discount_price);
                    $('#coupon_id').val(res.coupon_id);
                    $.assignTotalPriceSpanDiv();
                }
            }
        });
    });

    //Checkout by paypal
    jQuery.checkoutPaypal = function(params, orderId) {
        var urlSuccessInput = $('#url_success');
        var numberRandom = generateRandomString(10);
        numberRandom = numberRandom + '_' + orderId;
        sessionStorage.setItem('token_paypal', numberRandom);
        sessionStorage.setItem('order_info', params);
        var urlSuccess = urlSuccessInput.val();
        urlSuccessInput.val(urlSuccess+'?token='+numberRandom);
        var cartRs = sessionStorage.getItem('cart');
        var cartObj = JSON.parse(cartRs);
        var nameItems = '';
        $.each(cartObj, function(index, value){
            nameItems += value.name+' x '+value.qty+', ';
        });
        nameItems = nameItems.substring(0, nameItems.length - 2);//remove last ', '
        //assign paypal information to checkout
        $('#paypal_item_name').val('Order Id : '+ orderId);
        $('#paypal_item_qty').val($('#qty-total-hidden').val());
        $('#total_amount_paypal').val($.getTotalPriceFinal());
        $('#currency_code_paypal').val($('#currency_code').text());
        $('#paypal_method_form').submit();
    };

    //checkout stripe
    jQuery.checkoutStripe = function(params, orderId) {
        var handler = StripeCheckout.configure({
            key: $('#stripe_publish').val(),//public key
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
                var tokenVar = token.id;
                $.request('onCheckOutStripe', {
                    data: { tokenVar: tokenVar, params: params, orderId: orderId}
                });
            }
        });
        handler.open({
            name: 'Checkout stripe',
            description: 'Checkout stripe',
            amount: $.getTotalPriceFinal() * 100
        });
    };

    //Save order
    jQuery.saveOrder = function(params) {
        $.request('onSaveOrder', {
            data: params,
            success: function(orderId) {
                if (orderId != SAVE_ORDER_FAIL) {
                    if (params.payment_method_id == CASH_ON_DELIVERY) {
                        window.location.href = baseUrl+'/checkout-ok';
                        sessionStorage.setItem('cart', '');
                        return;
                    }
                    if (params.payment_method_id == PAYPAL) {
                        $.checkoutPaypal(params, orderId);
                        sessionStorage.setItem('cart', '');
                        return;
                    }
                    if (params.payment_method_id == STRIPE) {
                        $.checkoutStripe(params, orderId);
                        sessionStorage.setItem('cart', '');
                        return;
                    }
                    // create hook for another payment method
                    $(document).trigger( "anotherPaymentMethod", {params:params, orderId:orderId} );
                } else {
                    window.location.href = baseUrl+'/checkout-fail';
                }
            }
        });
    }

    $('#use_same_address_not_login').click(function() {
       if (!$(this).is(':checked')) {
           $('#guest-shipping-detail').removeClass('class-hidden');
       } else {
           $('#guest-shipping-detail').addClass('class-hidden');
       }
    });

    //checkout
    $('#checkout-button').click(function() {
        if (!$('#payment_agree').is(':checked')) {
           $.notify('You have to agree Privacy Policy');
           return;
        }
        if (!$('#confirm_agree').is(':checked')) {
           $.notify('You have to agree Terms & Conditions');
           return;
        }
        var paymentMethodId = $('input[name="payment_method"]:checked').val();
        if (paymentMethodId == undefined) {
            $.notify('You have to choose a payment method');
        } else {
            var addressBilling = $('input[name="user_address_billing"]:checked').val();
            var addressShipping = 0;
            if (!$('input[name="use_same_address"]').is(':checked')) {
                addressShipping = $('input[name="user_address_ship"]:checked').val();
            } else {
                addressShipping = addressBilling;
            }
            var params = {};
            if (addressBilling == undefined || addressShipping == undefined) {
                $.notify('You have to have a billing address');
                return;
            }
            params.address_billing = addressBilling;
            params.address_shipping = addressShipping;
            params.user_id = $('#user_id').val();
            params.shipping_cost = parseFloat($('#'+shipMoneyDiv).text());
            params.total = $.getTotalPriceFinal();
            params.order_status_id = 1;
            var cartRs = sessionStorage.getItem('cart');
            params.cart = JSON.parse(cartRs);
            params.payment_method_id = paymentMethodId;
            params.form_payment_not_login = $('#form-payment-not-login').serializeArray();
            params.shipping_rule_id = $('input[name="shipping_rule"]:checked').attr('attr-id');
            params.currency_code = $('#currency_code').text();
            params.coupon_id = $('#coupon_id').val();
            params.coupon_total = $('#coupon-reduce').text();
            params.comment = $('#comment').val();
            sessionStorage.setItem('orderParams', JSON.stringify(params));
            console.log(params);
            $('#checkout-div').waitMe({color: waitMeColor});
            $.saveOrder(params);
        }

    });

    //paypal success
    if (document.getElementById('paypal_success') !== null) {
        var tokenPaypal = sessionStorage.getItem('token_paypal');
        sessionStorage.setItem('token_paypal', 0);//reset to 0
        var paypalTokenGet = $('#paypal_token').text();
        if (tokenPaypal == paypalTokenGet) {//save order
            $.request('onPaypalSuccess', {
                data: {token:tokenPaypal}
            });
        } else {
            document.location.href = baseUrl+'/paypal/cancel';
        }
    }


});
