<?php namespace Ideas\Shop\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Controller;
use Ideas\Shop\Classes\Captcha;
use Ideas\Shop\Facades\Price;
use Ideas\Shop\Models\Category;
use Ideas\Shop\Models\Filter;
use Ideas\Shop\Models\FilterOption;
use Ideas\Shop\Models\IdeasShop;
use Ideas\Shop\Models\ProductReview;
use Ideas\Shop\Models\Products;
use Ideas\Shop\Models\ProductToAttribute;
use Ideas\Shop\Models\ProductToCategory;
use Ideas\Shop\Models\ProductToFilterOption;
use Ideas\Shop\Models\Route;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Session;

class Product extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Ideas Shop Product',
            'description' => 'Get product list and product detail.'
        ];
    }

    /**
     * Pass params to component view
     */
    public function onRun()
    {
        $route = $this->getRouterBySlug();
        $seoInfo = self::getSeoInfo($route->entity_id, $route->type);
        $this->page['type'] = $route->type;
        $this->page['type_product'] = Route::ROUTE_PRODUCT;
        $this->page['type_category'] = Route::ROUTE_CATEGORY;
        $this->page['seo_title'] = $seoInfo['seo_title'];
        $this->page['seo_keyword'] = $seoInfo['seo_keyword'];
        $this->page['seo_description'] = $seoInfo['seo_description'];
        $data = $this->getProduct($route->entity_id, $route->type);
        $this->page['data'] = $data['data'];
        if ($route->type == Route::ROUTE_CATEGORY) {//category
            $this->page['filter'] = $data['filter'];
            $this->page['minMaxPrice'] = $data['minMaxPrice'];
            $this->page['priceRange'] = $data['priceRange'];
            $this->page['pages'] = $data['pages']; //custom pagination
            $this->page['sortByString'] = $data['sortByString'];
            $this->page['filterChecked'] = $data['filterChecked'];
            $this->page['count'] = $data['count'];
            $this->page['key'] = $data['key'];
            $this->page['firstArray'] = [1, 4, 7, 10, 13, 16, 19];//add class .first in first col in row
            $this->page['breadCrumb'] = ['type'=>'category', 'data'=>$data['breadCrumb']];
        } else {//product detail
            $this->page['gallery'] = $data['gallery'];
            $this->page['related'] = $data['related'];
            $this->page['configurable'] = $data['configurable'];
            $this->page['review'] = $data['review'];
            $this->page['filter_color'] = FilterOption::FILTER_TYPE_COLOR;
            $this->page['filter_label'] = FilterOption::FILTER_TYPE_LABEL;
            $this->page['filter_image'] = FilterOption::FILTER_TYPE_IMAGE;
            $this->page['breadCrumb'] = ['type'=>'product', 'data'=>$data['breadCrumb']];
        }
        $this->addCss('/plugins/ideas/shop/assets/vendor/bootstrap_slider/css/bootstrap-slider.min.css');
        $this->addJs('/plugins/ideas/shop/assets/vendor/bootstrap_slider/bootstrap-slider.min.js');
        $this->addJs('/plugins/ideas/shop/assets/components/js/product.js');
    }

    /**
     * Generate captcha
     */
    public function onCaptcha()
    {
        $captcha = new Captcha();
        $captchaData = $captcha->getAndShowImage([]);
        return $captchaData;
    }

    /**
     * save review
     */
    public function onSubmitReview()
    {
        $post = post();
        $formData = [];
        foreach ($post as $row) {
            $formData[$row['name']] = $row['value'];
        }
        $loggedIn = Auth::getUser();
        ProductReview::addReview($formData, $loggedIn);
    }

    /**
     * Get seo info
     */
    public static function getSeoInfo($id, $type)
    {
        if ($type == Route::ROUTE_PRODUCT) {
            $query = Products::select('id')->with(['attribute:id,product_id,seo_title,seo_keyword,seo_description']);
        } else {
            $query = Category::select('seo_title', 'seo_keyword', 'seo_description');
        }
        $data = $query->where('id', $id)
            ->first();
        if ($type == Route::ROUTE_PRODUCT) {
            $seoTitle = $data->attribute->seo_title;
            $seoKeyword = $data->attribute->seo_keyword;
            $seoDescription = $data->attribute->seo_description;
        } else {
            $seoTitle = $data->seo_title;
            $seoKeyword = $data->seo_keyword;
            $seoDescription = $data->seo_description;
        }
        $rs = [
            'seo_title' => $seoTitle,
            'seo_keyword' => $seoKeyword,
            'seo_description' => $seoDescription
        ];
        return $rs;
    }

    /**
     * Get router by current url
     */
    public function getRouterBySlug()
    {
        $url = $this->currentPageUrl();
        $baseUrl = url('/');
        $slug = str_replace($baseUrl, '', $url);
        $slug = substr($slug, 1);//remove first '/'
        $data = Route::select('entity_id', 'type')
            ->where('slug', $slug)
            ->first();
        return $data;
    }

    /**
     * Convert filter then count product per filter option
     */
    public static function convertFilter($data)
    {
        $rs = [];
        if (!empty($data)) {
            $filter = [];
            //group by filter_id
            foreach ($data as $row) {
                $filter[$row->filter_id][] = $row;
            }
            $filterOption = [];
            //group by option_id ~ filter_option_id
            $arrayProductCount = [];// count product per option_id
            foreach ($filter as $key => $value) {
                foreach ($value as $option) {
                    $filterOption[$key][$option->option_id] = $option;
                    $arrayProductCount[$option->option_id][] = $option;//assign option to count
                }
            }
            foreach ($filterOption as $key => $value) {
                foreach ($value as $option) {
                    $optionId = $option->option_id;
                    $option->product_count = array_key_exists($optionId, $arrayProductCount) ?
                        count($arrayProductCount[$optionId]) : 0;//count products
                    $rs[$option->filter_id]['filter'] = ['name'=>$option->filter_name];
                    $rs[$option->filter_id]['options'][] = $option;
                }
            }
        }
        return $rs;
    }
    /**
     * Get filter for category
     * $filterArray : filter that use to filter product list
     */
    public static function getFilterForCategory($productArray)
    {
        // get product id array to get filter
        $productIdArray = [];
        foreach ($productArray as $row) {
            $productIdArray[] = $row->id;
        };
        //get filter based o $productIdArray
        $tableFilter = with(new Filter())->getTable();
        $tableFilterOption = with(new FilterOption())->getTable();
        $tableProductToFilterOption = with(new ProductToFilterOption())->getTable();
        $fieldArray = [
            'f.name AS filter_name', 'f.id AS filter_id', 'fo.name AS option_name', 'fo.id AS option_id', 'pf.*'
        ];
        $query = DB::table($tableFilter.' AS f')->select($fieldArray)
            ->leftJoin($tableFilterOption.' AS fo', 'fo.filter_id', '=', 'f.id')
            ->leftJoin($tableProductToFilterOption.' AS pf', 'pf.filter_option_id', '=', 'fo.id');
        $query->whereIn('pf.product_id', $productIdArray);
        $data = $query->get()->toArray();
        $data = self::convertFilter($data);
        return $data;
    }

    /**
     * Get min max price
     */
    public static function getPriceMinMax($data)
    {
        $rs['min'] = 0;
        $rs['max'] = 0;
        if (!empty($data)) {
            $priceArray = [];
            foreach ($data as $row) {
                $priceArray[] = $row->price;
            }
            $rs['min'] = min($priceArray);
            $rs['max'] = max($priceArray);
        }
        return $rs;
    }

    /**
     * Count page
     */
    public static function countPage($totalPages, $currentPage, $limitPage)
    {
        $remainPage = $limitPage - floor($limitPage/2);
        $plusPage = $limitPage - $remainPage;
        if ($totalPages <= $limitPage) {
            // less than $limitPage total pages so show all
            $startPage = 1;
            $endPage = $totalPages;
        } else {
            // more than $limitPage total pages so calculate start and end pages
            if ($currentPage <= $remainPage) {
                $startPage = 1;
                $endPage = $limitPage;
            } elseif ($currentPage + $plusPage >= $totalPages) {
                $startPage = $totalPages - ($limitPage - 1);
                $endPage = $totalPages;
            } else {
                $startPage = $currentPage - floor($limitPage / 2);
                $endPage = $currentPage + $plusPage;
            }
        }
        $pages = [];
        for ($i = $startPage; $i<$endPage + 1; $i++) {
            array_push($pages, $i);
        }
        return ['pages'=>$pages, 'currentPage' => $currentPage, 'totalPages' => $totalPages];
    }

    /**
     * pagination array
     */
    public static function arrayPagination($data, $limit)
    {
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // Create a new Laravel collection from the array data
        $itemCollection = collect($data);
        // Define how many items we want to be visible in each page
        $perPage = $limit;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        return $currentPageItems;
    }

    /**
     * Get weight and weight_id for product
     * input object
     * output object
     */
    public static function getWeightForProduct($data)
    {
        $productIdArray = [];
        if (!empty($data)) {
            foreach ($data as $row) {
                $productIdArray[] = $row->id;
            }
            $attribute = ProductToAttribute::select('weight', 'weight_id', 'product_id')
                ->whereIn('product_id', $productIdArray)
                ->get();
            $attributeConvert = [];
            if (!empty($attribute)) {
                foreach ($attribute as $row) {
                    $attributeConvert[$row->product_id] = [
                        'weight' => $row->weight,
                        'weight_id' => $row->weight_id
                    ];
                }
            }
            foreach ($data as $row) {
                $row->weight = !empty($attributeConvert[$row->id]['weight']) ?
                    $attributeConvert[$row->id]['weight'] : 0;
                $row->weight_id = !empty($attributeConvert[$row->id]['weight_id']) ?
                    $attributeConvert[$row->id]['weight_id'] : 1;
            }
        }
        return $data;
    }

    /**
     * Get list product cache
     */
    public static function getListProductCache($get, $id)
    {
        isset($get['limit']) ? $limit = $get['limit'] : $limit = 9;
        isset($get['filter']) ? $filterArray = explode('_', $get['filter']) : $filterArray = [];
        isset($get['sort-by']) ? $sortBy = explode('-', $get['sort-by']) : $sortBy = ['price', 'asc'];
        isset($get['key']) ? $key = $get['key'] : $key = '';
        isset($get['page']) ? $page = $get['page'] : $page = 1;
        $productTable = with(new Products())->getTable();
        $productToCategoryTable = with(new ProductToCategory())->getTable();
        $productToFilterOptionTable = with(new ProductToFilterOption())->getTable();
        //$productAttribute = with(new ProductToAttribute())->getTable();
        $query = DB::table($productTable.' AS p')
            ->leftJoin($productToCategoryTable.' AS pc', 'p.id', '=', 'pc.product_id')
            //->leftJoin($productAttribute.' AS pa', 'p.id', '=', 'pa.product_id')
            ->where('p.product_type', '!=', Products::PRODUCT_TYPE_CONFIGURABLE_CHILD)//exclude config child
            ->where('pc.category_id', $id);//filter by category

        //filter product by filter option id
        if (!empty($filterArray)) {
            //just join when filter => avoid duplicate record
            $query->leftJoin($productToFilterOptionTable.' AS pf', 'p.id', '=', 'pf.product_id');
            if (count($filterArray) == 1) {
                $query->where('pf.filter_option_id', $filterArray[0]);
            } else { //> 1 filter option, GROUP BY + HAVING COUNT
                $query->whereIn('pf.filter_option_id', $filterArray)
                    ->groupBy('p.id')
                    ->havingRaw('COUNT(pf.filter_option_id) >= '.count($filterArray));
            }
        }

        if ($key != '') {
            $query->where('p.name', 'like', '%'.$get['key'].'%');
        }

        // filter by price range
        if (isset($get['price-range'])) {
            $priceRange = explode('-', $get['price-range']);
            $query->whereBetween('p.price', $priceRange);
        }

        if (!empty($sortBy)) {
            $query->orderBy('p.'.$sortBy[0], $sortBy[1]);
        } else {
            $query->orderBy('p.price', 'asc');//default sort is 'asc'
        }
        //get all product id for filter and price range
        //not use select(*) => Syntax error or access violation: 1055 when group by
        $arrayField = [
            'p.id', 'p.price', 'p.name', 'p.qty', 'p.qty_order', 'p.product_type', 'p.featured_image', 'p.slug',
            'p.tax_class_id'
        ];
        $data = $query->select($arrayField)
            ->distinct()->get()->toArray();
        $data = self::getWeightForProduct($data);
        $minMaxPrice = self::getPriceMinMax($data);
        if (!isset($get['price-range'])) {
            $priceRange = array_values($minMaxPrice);
        };
        $allRecord = count($data);
        $totalPage = ceil($allRecord/$limit);
        $pages = self::countPage($totalPage, $page, 5);
        $filter = self::getFilterForCategory($data);
        $data = self::arrayPagination($data, $limit);
        $data = Price::addFinalPriceForProductObject($data);
        $countProductCurrentPage = count($data);
        $breadCrumb = Category::getBreadCrumb($id);
        $rs = [
            'filter' => $filter,
            'data' => $data,
            'minMaxPrice' => $minMaxPrice,
            'priceRange' => $priceRange,
            'pages' => $pages,
            'sortByString' => implode('-', $sortBy),
            'filterChecked' => $filterArray,
            'count' => $countProductCurrentPage,
            'key' => $key,
            'breadCrumb' => $breadCrumb
        ];
        return $rs;
    }

    /**
     * Get list product - category
     */
    public static function getListProduct($id)
    {
        $get = get();
        $cacheKey = 'category_'.$id.'_';
        if (!empty($get)) {
            foreach ($get as $key => $value) {
                $cacheKey .= $key.'_'.$value.'_';
            }
        }
        $rs = IdeasShop::returnCacheData($cacheKey, function () use ($get, $id) {
            return self::getListProductCache($get, $id);
        });
        $rs['data'] = IdeasShop::checkProductCanBuy($rs['data']);
        return $rs;
    }


    /**
     * Get related product
     */
    public static function getRelatedProduct($id)
    {
        $category = ProductToCategory::where('product_id', $id)
            ->first();
        $rs = [];
        if (!empty($category)) {
            $categoryId = $category->category_id;
            $query = Products::where('id', '!=', $id);
            $query->whereHas('productToCategory', function ($query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            });
            $rs = $query->limit(4)->get();
            $rs = IdeasShop::checkProductCanBuy($rs);
            $rs = Price::addFinalPriceForProductObject($rs);
            $rs = $rs->toArray();
        }
        return $rs;
    }


    /**
     * Get filter for product detail
     */
    public static function getFilterForProductDetail($filterOptionArray, $filterOptionRelate)
    {
        $filterTable = with(new Filter())->getTable();
        $filterOptionTable = with(new FilterOption())->getTable();
        $data = DB::table($filterTable.' AS f')->select('f.name AS filter_name', 'fo.*')
            ->leftJoin($filterOptionTable.' AS fo', 'fo.filter_id', '=', 'f.id')
            ->whereIn('fo.id', $filterOptionArray)
            ->get()
            ->toArray();

        $rs = [];
        foreach ($data as $row) {
            $row->filter_related = array_key_exists($row->id, $filterOptionRelate) ? $filterOptionRelate[$row->id] : '';
            $rs[$row->filter_id]['name'] = $row->filter_name;
            $rs[$row->filter_id]['options'][] = $row;
        }
        return $rs;
    }

    /**
     * Convert filter option of product child
     */
    public static function convertOptionOfProductChild($data)
    {
        $filterOption = [];
        foreach ($data as $row) {
            $strFilterOptionId = $row['configurable']['str_filter_option_id'];
            $filterOption[] = explode(',', $strFilterOptionId);
        }
        $numOption = count($filterOption[0]);
        $convert = [];
        for ($i=0; $i<$numOption; $i++) {
            foreach ($filterOption as $row) {
                $convert[$i][] = $row[$i];
            }
        }
        //for example: 4 options -> for 3 times
        $rs = [];
        for ($i=0; $i<$numOption - 1; $i++) {
            $optionIndex = 0;
            foreach ($convert[$i] as $row) {
                $rs[$row][] = $convert[$i+1][$optionIndex];
                $optionIndex++;
            }
        }
        $rsConvert = [];
        foreach ($rs as $key => $value) {
            $rsConvert[$key] = implode(',', $value);
        }
        return $rsConvert;
    }

    /**
     * Get configurable product data
     */
    public static function getConfigurableData($id)
    {
        $query = Products::with('configurable:*');
        $query->whereHas('configurable', function ($query) use ($id) {
            $query->where('product_id', $id);
        });
        $childProduct = $query->get()->toArray();
        $filterOptionRelate = self::convertOptionOfProductChild($childProduct);
        $filterOptionIdArray = [];
        foreach ($childProduct as $row) {
            $filterOption = explode(',', $row['configurable']['str_filter_option_id']);
            foreach ($filterOption as $o) {
                $filterOptionIdArray[] = $o;
            }
        }
        $filterOptionIdArrayUnique = array_unique($filterOptionIdArray);
        $filter = self::getFilterForProductDetail(array_values($filterOptionIdArrayUnique), $filterOptionRelate);
        $childConvert = [];
        foreach ($childProduct as $row) {
            $childConvert[$row['configurable']['str_filter_option_id']] = $row;
        }
        $childConvert = Price::addFinalPriceForChildConfig($childConvert);
        $rs = [
            'child' => $childProduct,
            'filter' => $filter,
            'childConvert' => $childConvert
        ];
        return $rs;
    }

    /**
     * Get review product
     */
    public static function getProductReview($id)
    {
        $data = ProductReview::where('product_id', $id)
            ->where('status', IdeasShop::ENABLE)
            ->get()->toArray();
        return $data;
    }

    /**
     * Get product detail
     */
    public static function getProductDetailCache($id)
    {
        $data = Products::with(['attribute:*'])
            ->where('id', $id)
            ->first();
        $galleryArray = [];
        $configurable = [];
        if (!empty($data)) {
            $data = $data->toArray();
            $gallery = $data['gallery'];
            $galleryArray = [];
            if ($gallery != '') {
                $galleryArray = explode(';', $gallery);
            }
            $productType = $data['product_type'];
            $configurable = [];
            if ($productType == Products::PRODUCT_TYPE_CONFIGURABLE) {
                $configurable = self::getConfigurableData($id);
            }
            $data = Price::addFinalPriceForDetailProductArray($data);
        }
        //set to zero to set current category again  : product detail, homepage, search
        Session::put('current_category', 0);
        $rs = [
            'data' => $data,
            'gallery' => $galleryArray,
            'configurable' => $configurable,
            'related' => self::getRelatedProduct($id),
            'review' => self::getProductReview($id)
        ];
        return $rs;
    }

    /**
     * Get product detail cache
     */
    public static function getProductDetail($id)
    {
        $cacheKey = 'product_'.$id;
        $rs = IdeasShop::returnCacheData($cacheKey, function () use ($id) {
            return self::getProductDetailCache($id);
        });
        //get breadcrumb beside cache because depends on category id
        $currentCategory = Session::get('current_category');
        if ($currentCategory != 0) {
            $categoryId = $currentCategory;
        } else {
            $category = ProductToCategory::getCategoryForProduct($id);
            $categoryId = !empty($category) ? $category->category_id : 1;
        }
        $rs['breadCrumb'] = Category::getBreadCrumb($categoryId);
        $rs['data'] = IdeasShop::checkSingleProductCanBuyArray($rs['data']);
        return $rs;
    }

    /**
     * Get data for category and product detail
     */
    public static function getProduct($id, $type)
    {
        if ($type == Route::ROUTE_PRODUCT) {
            return self::getProductDetail($id);
        } else {
            //in case of one product belongs to many category
            Session::put('current_category', $id);
            $rs = self::getListProduct($id);
            return $rs;
        }
    }

    /**
     * Ajax Reload gallery of configurable product
     */
    public function onReloadGallery()
    {
        $product = post();
        $featuredImage = $product['featured_image'];
        $gallery = [];
        if (!empty($product['gallery'])) {
            $gallery = explode(';', $product['gallery']);
        }
        $this->page['featuredImage'] = $featuredImage;
        $this->page['gallery'] = $gallery;
    }

}