<?php namespace Ideas\Shop\Components;

use Cms\Classes\ComponentBase;
use Ideas\Shop\Facades\Price;
use Ideas\Shop\Models\IdeasShop;
use Ideas\Shop\Models\Products;
use Illuminate\Support\Facades\Session;

class Theme extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Theme config',
            'description' => 'Get theme config.'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'             => 'Slug of theme config item',
                'type'              => 'string',
            ]
        ];
    }

    public function onRun()
    {
        $this->page['galleries'] = $this->loadGalleries();
        $this->page['welcome'] = $this->loadWelcome();
        $this->page['featured'] = $this->loadFeaturedProduct();
        $this->page['testimonials'] = $this->loadTestimonials();
        $this->page['ship_slide'] = $this->loadShipSlide();
        $this->page['arrivals'] = $this->loadLastedArrivals();
        Session::put('current_category', 0);//set to zero to set current category again
    }

    /**
     * load gallery
     */
    protected function loadGalleries()
    {
        return \Ideas\Shop\Models\Theme::getThemeImagesBySlug('homepage_gallery');
    }

    /**
     * Load welcome image
     */
    protected function loadWelcome()
    {
        return \Ideas\Shop\Models\Theme::getThemeImagesBySlug('welcome_to_our_shop');
    }

    /**
     * Load testimonials
     */
    protected function loadTestimonials()
    {
        return \Ideas\Shop\Models\Theme::getThemeImagesBySlug('testimonials');
    }

    /**
     * Load ship slide
     */
    protected function loadShipSlide()
    {
        return \Ideas\Shop\Models\Theme::getThemeImagesBySlug('ship_slide');
    }

    /**
     * Load featured product
     */
    protected function loadFeaturedProduct()
    {
        $query = Products::whereHas('attribute', function ($query) {
            $query->is_featured_product = IdeasShop::ENABLE;
        });
        $data = $query->limit(4)->get();
        $data = IdeasShop::checkProductCanBuy($data);
        $data = Price::addFinalPriceForProductObject($data);
        return $data;
    }

    /**
     * Load lasted arrivals
     */
    protected function loadLastedArrivals()
    {
        $query = Products::whereHas('attribute', function ($query) {
            $query->is_new = IdeasShop::ENABLE;
        });
        $data = $query->limit(2)->get();
        return $data;
    }
}