<?php namespace Ideas\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;
use Event;

class City extends Model
{
    public $table = 'ideas_city';
    public $timestamps = false;//disable 'created_at' and 'updated_at'
    use Validation;

    public $rules = [
        'name' => 'required',
    ];

    public $hasOne = [
        'geo_zone_relation' => [
            'Ideas\Shop\Models\Geo',
            'key' => 'id',//primary key
            'otherKey' => 'geo_zone_id'//foreign key
        ]
    ];

    public function getGeoZoneIdOptions()
    {
        $rs = Geo::select('id', 'name')
            ->get()
            ->toArray();
        $geoZoneArray = [];
        foreach ($rs as $row) {
            $geoZoneArray[$row['id']] = $row['name'];
        }
        return $geoZoneArray;
    }

    public static function getCity()
    {
        $data = self::select('id', 'name')
            ->get()
            ->toArray();
        return $data;
    }

    public static function getCityNameById($id)
    {
        $data = self::select('name')
            ->where('id', $id)
            ->first();
        $rs = [];
        if (!empty($data)) {
            $rs = $data->toArray();
        }
        return $rs['name'];
    }

    /**
     * after save
     */
    public function afterSave()
    {
        $dataSaved = $this->attributes;
        $post = post();
        Event::fire('ideas.shop.save_city', [$dataSaved, $post]);
    }

}