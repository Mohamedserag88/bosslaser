<?php

namespace Ideas\Shop\Lib;

use Illuminate\Support\Facades\File;
use Stripe\Charge;
use Stripe\Error\ApiConnection;
use Stripe\Stripe;

class LibLoader
{
    public static function initStripe()
    {
        File::requireOnce(plugins_path('ideas/shop/lib/vendor/autoload.php'));
        $stripe = new Stripe();
        return $stripe;
    }


}