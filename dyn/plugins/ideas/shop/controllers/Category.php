<?php

namespace Ideas\Shop\Controllers;

class Category extends IdeasShopController
{
    public $requiredPermissions = ['ideas.shop.access_category'];
    public $controllerName = 'category';

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ReorderController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

}