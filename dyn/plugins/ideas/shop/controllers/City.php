<?php

namespace Ideas\Shop\Controllers;

class City extends IdeasShopController
{
    public $requiredPermissions = ['ideas.shop.access_city'];
    public $controllerName = 'city';
}
