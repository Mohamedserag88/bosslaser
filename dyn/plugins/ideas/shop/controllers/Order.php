<?php

namespace Ideas\Shop\Controllers;

use BackendMenu;
use Backend;
use Ideas\Shop\Models\City;
use Ideas\Shop\Models\IdeasShop;
use Ideas\Shop\Models\OrderProduct;
use Ideas\Shop\Models\OrderReturn;
use Ideas\Shop\Models\OrderStatus;
use Ideas\Shop\Models\ProductDownloadableLink;
use Ideas\Shop\Models\Reason;
use Ideas\Shop\Models\Ship;
use View;
use Flash;
use Ideas\Shop\Models\OrderStatusChange;

class Order extends IdeasShopController
{
    public $requiredPermissions = ['ideas.shop.access_order'];
    public $controllerName = 'order';

    public $implement = [
        'Backend.Behaviors.ListController'
    ];
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ideas.Shop', 'shop', 'order');
    }

    public function update($recordId = null)
    {
        $this->pageTitle = 'ideas.shop::lang.action.update';
        $this->vars['controller'] = $this->controllerName;
        $this->vars['id'] = $recordId;
        $this->vars['invoiceUrl'] = Backend::url('ideas/shop/order/invoice/'.$recordId);
        $data = \Ideas\Shop\Models\Order::getOrderDetail($recordId);
        $data['city_billing_name'] = City::getCityNameById($data['billing_city_id']);
        $data['city_shipping_name'] = City::getCityNameById($data['shipping_city_id']);
        $data['shipping_rule_name'] =  Ship::getShipRuleNameById($data['shipping_rule_id']);
        $this->vars['data'] = $data;
        $this->vars['product'] = OrderProduct::getOrderProduct($recordId);
        $orderStatusChange = OrderStatusChange::getOrderStatusChange($recordId);
        $orderStatusConvert = [];
        foreach ($orderStatusChange as $row) {
            $row['order_status_name'] = OrderStatus::getOrderStatusNameById($row['order_status_id']);
            $orderStatusConvert[] = $row;
        }
        $this->vars['orderStatusConvert'] = $orderStatusConvert;
        $this->vars['orderStatus'] = OrderStatus::getOrderStatus();
        $this->vars['reason'] = Reason::getAllReason();
        $this->vars['paid'] = IdeasShop::PAYMENT_STATUS_PAID;
        $this->vars['notPaid'] = IdeasShop::PAYMENT_STATUS_NOT_PAID;
    }

    public function onOrderStatusHistory()
    {
        $post = post();
        OrderStatusChange::createOrderStatusChange($post);
        $url = Backend::url('ideas/shop/order/update/'.$post['id']);
        Flash::success("You've just added an order status change");
        return redirect($url);
    }

    /**
     * Send download link downloadable product
     */
    public function onSendDownloadLink()
    {
        $post = post();
        $rs = ProductDownloadableLink::sendDownloadLink($post);
        return response()->json($rs);
    }

    /**
     * Check product in order is return
     */
    public function onCheckProductIsReturned()
    {
        $post = post();
        $rs = OrderReturn::checkProductInOrderIsReturn($post);
        return response()->json($rs);
    }

    /**
     * Create order return
     */
    public function onCreateOrderReturn()
    {
        $post = post();
        $rs = OrderReturn::createOrderReturn($post);
        return response()->json($rs);
    }

    /**
     * Change payment status
     */
    public function onChangePaymentStatus()
    {
        $post = post();
        $rs = \Ideas\Shop\Models\Order::changePaymentStatus($post);
        return response()->json($rs);
    }

    /**
     * Invoice
     */
    public function invoice($recordId)
    {
        $this->layout = false;//disable load layout
        $data = \Ideas\Shop\Models\Order::getOrderDetail($recordId);
        $data['city_billing_name'] = City::getCityNameById($data['billing_city_id']);
        $data['city_shipping_name'] = City::getCityNameById($data['shipping_city_id']);
        $data['shipping_rule_name'] =  Ship::getShipRuleNameById($data['shipping_rule_id']);
        $data['payment_method_name'] = \Ideas\Shop\Models\Order::paymentMethodName($data['payment_method_id']);
        $this->vars['data'] = $data;
        $this->vars['product'] = OrderProduct::getOrderProduct($recordId);
        $this->vars['baseUrl'] = url('/');
    }
}