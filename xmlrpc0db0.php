<?xml version="1.0" encoding="UTF-8"?><rsd version="1.0" xmlns="http://archipelago.phrasewise.com/rsd">
  <service>
    <engineName>WordPress</engineName>
    <engineLink>https://wordpress.org/</engineLink>
    <homePageLink>http://bosslasers.fanscreens.net</homePageLink>
    <apis>
      <api name="WordPress" blogID="1" preferred="true" apiLink="http://bosslasers.fanscreens.net/xmlrpc.php" />
      <api name="Movable Type" blogID="1" preferred="false" apiLink="http://bosslasers.fanscreens.net/xmlrpc.php" />
      <api name="MetaWeblog" blogID="1" preferred="false" apiLink="http://bosslasers.fanscreens.net/xmlrpc.php" />
      <api name="Blogger" blogID="1" preferred="false" apiLink="http://bosslasers.fanscreens.net/xmlrpc.php" />
      	<api name="WP-API" blogID="1" preferred="false" apiLink="http://bosslasers.fanscreens.net/wp-json/" />
	    </apis>
  </service>
</rsd>
