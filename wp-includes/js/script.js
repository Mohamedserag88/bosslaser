// $.fn.extend({
//   hasClasses: function (selectors) {
//     var self = this;
//     for (var i in selectors) {
//         if ($(self).hasClass(selectors[i]))
//             return true;
//     }
//     return false;
//   }
// });

var filter_data = [];
(function($){
  $('#show-canvase').on('click', function(e){
    e.preventDefault();
    if($('body.canvas-off').length){
      $('body').removeClass('canvas-off');
    }else {
      $('body').addClass('canvas-off');
    }
  });


  $('.filter').on('click', function(){
    // e.preventDefault();
    $(this).find('checkbox').prop('checked', true);
    if($(this).hasClass('active')){
      $(this).removeClass('active');
    }else {
      $(this).addClass('active');
    }
    update_filter_products();
  });
})(jQuery);

function update_filter_products(){
  (function($){
    var i = 0;
    $filter = $('.filter.active');
    if($filter.length){
      filter_data = [];
      $filter.each(function(index, element){
        filter_data[i] = $(element).data('filter-query');
        i++;
      });
      var data = filter_data.join(',');
      $('.product').each(function(index, element1){
        var found = 0;
        filter_data.forEach(function(el){
          if($(element1).hasClass(el)){
            found = 1;
          }
        });

        if(found == 1){
          $(element1).removeClass('hidden');
        }else {
          $(element1).addClass('hidden');
        }
      });
      console.log(filter_data);
    }else{
      filter_data = [];
      $('.product').removeClass('hidden');
    }
  })(jQuery);
}
